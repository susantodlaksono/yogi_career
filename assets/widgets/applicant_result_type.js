$(function () {

	'use restrict';

	$.fn.get_applicant = function(params) {
      var p = $.extend({
  	      offset: 0,
         currentPage: 1,
         search : $(container + ' #search').val(),
         sdate : $(container + ' #sdate').val(),
         edate : $(container + ' #edate').val(),
         test_type_id : $(container + ' #test_type_id').val()
      }, params);
      ajaxManager.addReq({
         url: site_url + '/applicant_result_type/widget/get_applicant_result',
         type: 'GET',
         dataType: 'JSON',
         data: {
             offset: p.offset,
             search: p.search,
             sdate: p.sdate,
             edate: p.edate,
             test_type_id: p.test_type_id,
         },
         beforeSend: function () {
            show_loading();
         },
         error: function (jqXHR, status, errorThrown) {
            error_handle(jqXHR, status, errorThrown);
         },
         success: function(r) {
        		t = '';
            if(r.total > 0){
               $.each(r.result, function (k,v){ 
                  t += '<tr>';
                     t += '<td><b>'+v.name+'</b><br>'+v.email+'</td>';
                     t += '<td>'+v.vacancy_name+'</td>';
                     t += '<td>'+v.age+'</td>';
                     t += '<td>'+v.contact_number+'</td>';
                     t += '<td>'+v.education_degree+'</td>';
                     t += '<td>'+(v.gender == 'L' ? 'Laki-Laki' : 'Perempuan')+'</td>';
                     t += '<td>';
                     t += '';
                     t += '</td>';
                  t += '</tr>';
               });
            }else{
               t += '<tr><td colspan="7"><h5 class="text-center font-arial text-muted">No Result</h5></td></tr>';
            }
            $(container + ' #section-total').html(r.total + ' Data Found');
            $(container + ' #section-data').html(t);
            $(container + ' #section-pagination').paging({
               items: r.total,
               currentPage: p.currentPage
            });
        	   hide_loading();
         }
      });
   }

    $(this).get_applicant();

    $.fn.paging = function (opt) {
        var s = $.extend({
            items: 0,
            itemsOnPage: 10,
            currentPage: 1
        }, opt);

        $(container + ' #section-pagination').pagination({
            items: s.items,
            itemsOnPage: s.itemsOnPage,
            prevText: '&laquo;',
            nextText: '&raquo;',
            hrefTextPrefix: '#',
            currentPage: s.currentPage,
            onPageClick: function (n, e) {
                e.preventDefault();
                var offset = (n - 1) * s.itemsOnPage;
                $(this).get_applicant({
                    offset: offset,
                    currentPage: n
                });
            }
        });
    };

   $('#range-date').daterangepicker({
        ranges: {
            'Last 7 Days': [moment().subtract('days', 7), moment()],
            'Last 30 Days': [moment().subtract('days', 30), moment()],
            'Last 3 Month': [moment().subtract('days', 90), moment()]
        },
        format: 'DD-MM-YYYY',
        startDate: moment().subtract('days', 29),
        endDate: moment(),
        isShowing: true
   },
   function (start, end) {
    	$('#range-date').html('<i class="fa fa-calendar"></i> '+start.format('DD MMM YYYY')+' sd '+end.format('DD MMM YYYY')+'');
    	$(container + ' #sdate').val(start.format('YYYY-MM-DD'));
      $(container + ' #edate').val(end.format('YYYY-MM-DD'));
      $(this).get_applicant({
         sdate : start.format('YYYY-MM-DD'),
        	edate : end.format('YYYY-MM-DD')
      });
   }
   );

});