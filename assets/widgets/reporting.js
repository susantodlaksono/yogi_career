$(function () {

	'use restrict';

   hide_loading();

   $('#range-date').daterangepicker({
      ranges: {
          'Last 7 Days': [moment().subtract('days', 7), moment()],
          'Last 30 Days': [moment().subtract('days', 30), moment()],
          'Last 3 Month': [moment().subtract('days', 90), moment()]
      },
      locale: {
         format: 'DD MMM YYYY'
      },
      startDate: moment(),
      endDate: moment(),
      isShowing: true
   },
      function (start, end) {
        $('#form-reporting').find('input[name="sdate"]').val(start.format('YYYY-MM-DD'));
        $('#form-reporting').find('input[name="edate"]').val(end.format('YYYY-MM-DD'));
      }
   );

   $(container + ' .vacancy').select2();

});