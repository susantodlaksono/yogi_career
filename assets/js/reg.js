$(function () {

   key_family = 0;
   key_education = 0;
   key_language = 0;
   key_education_nonformal = 0;
   key_work_history = 0;
   key_social = 0;
   key_accident = 0;

   $('.add-more-family').on('click', function (e) {
      key_family += 1;
      t = '';
      t += '<div class="form-group-attached">'
         t += '<div class="row clearfix" style="border-top:2px solid #e6e6e6">';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Hubungan Keluarga</label>';
                  t += '<select class="cs-select cs-skin-slide cs-transparent form-control" data-init-plugin="cs-select" name="family['+key_family+'][family_relation]" required="">';
                     t += '<option value="1" selected="">Ayah</option>';
                     t += '<option value="2">Ibu</option>';
                     t += '<option value="3">Saudara 1</option>';
                     t += '<option value="4">Saudara 2</option>';
                     t += '<option value="5">Saudara 3</option>';
                     t += '<option value="6">Saudara 4</option>';
                  t += '</select>';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Nama</label>';
                  t += '<input type="text" class="form-control input-sm" name="family['+key_family+'][family_name]" required="">';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix" style="padding-top:5px;">';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Jenis Kelamin</label>';
                  t += '<select class="cs-select cs-skin-slide cs-transparent form-control" data-init-plugin="cs-select" name="family['+key_family+'][family_gender]" required="">';
                     t += '<option value="L" selected="">Laki-laki</option>';
                     t += '<option value="P">Perempuan</option>';
                  t += '</select>';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Umur</label>';
                  t += '<input type="number" class="form-control input-sm" name="family['+key_family+'][family_age]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Pendidikan Terakhir</label>';
                  t += '<select class="cs-select cs-skin-slide cs-transparent form-control" data-init-plugin="cs-select" name="family['+key_family+'][family_degree]">';
                     t += '<option value=""></option>';
                     t += '<option value="1">SMK</option>';
                     t += '<option value="2">SMA</option>';
                     t += '<option value="3">SLTA/SEDERAJAT</option>';
                     t += '<option value="4">D1</option>';
                     t += '<option value="5">D2</option>';
                     t += '<option value="6">D3</option>';
                     t += '<option value="7">D4</option>';
                     t += '<option value="8">S1</option>';
                     t += '<option value="9">S2</option>';
                     t += '<option value="10">S3</option>';
                  t += '</select>';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix" style="padding-top:5px;">';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Jabatan</label>';
                  t += '<input type="text" class="form-control input-sm" name="family['+key_family+'][family_last_job_position]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Perusahaan</label>';
                  t += '<input type="text" class="form-control input-sm" name="family['+key_family+'][family_last_job_company]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Keterangan</label>';
                  t += '<input type="text" class="form-control input-sm" name="family['+key_family+'][family_last_description]">';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix" style="margin-bottom:25px;">';
            t += '<div class="col-md-12 text-center">';
               t += '<button type="button" class="btn btn-white btn-remove btn-sm"><i class="fa fa-trash"></i></button>';
            t += '</div>';
         t += '</div>';
      t += '</div>';

      $('#form-registration').find('.sect-family').prepend(t);

      $('#form-registration .btn-remove').on('click', function (e) {
         $(this).parents('.form-group-attached').remove();
         e.preventDefault();
      });
   });

   $('.add-more-education').on('click', function (e) {
      key_education += 1;
      t = '';
      t += '<div class="form-group-attached">'
         t += '<div class="row clearfix" style="border-top:2px solid #e6e6e6">';
            t += '<div class="col-sm-5">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Tingkat</label>';
                  t += '<select class="cs-select cs-skin-slide cs-transparent form-control" data-init-plugin="cs-select" name="education['+key_education+'][education_degree]" required="">';
                     t += '<option value="1" selected="">SMK</option>';
                     t += '<option value="2">SMA</option>';
                     t += '<option value="3">SLTA/SEDERAJAT</option>';
                     t += '<option value="4">D1</option>';
                     t += '<option value="5">D2</option>';
                     t += '<option value="6">D3</option>';
                     t += '<option value="7">D4</option>';
                     t += '<option value="8">S1</option>';
                     t += '<option value="9">S2</option>';
                     t += '<option value="10">S3</option>';
                  t += '</select>';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-5">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Nama Sekolah</label>';
                  t += '<input type="text" class="form-control input-sm" name="education['+key_education+'][education_school_name]" required="">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-1">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Dari</label>';
                  t += '<input type="number" class="form-control input-sm" name="education['+key_education+'][education_start]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-1">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Hingga</label>';
                  t += '<input type="number" class="form-control input-sm" name="education['+key_education+'][education_sampai]">';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix" style="">';
            t += '<div class="col-sm-3">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Kota</label>';
                  t += '<input type="text" class="form-control input-sm" name="education['+key_education+'][education_city]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-3">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Bidang / Jurusan</label>';
                  t += '<input type="text" class="form-control input-sm" name="education['+key_education+'][education_major]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-3">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Lulus / Tidak</label>';
                  t += '<select class="cs-select cs-skin-slide cs-transparent form-control" data-init-plugin="cs-select" name="education['+key_education+'][status]">';
                     t += '<option value="1" selected="">Lulus</option>';
                     t += '<option value="2">Tidak Lulus</option>';
                  t += '</select>';
               t += '</div>';
            t += '</div>';
             t += '<div class="col-sm-3">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Nilai Rata-rata</label>';
                  t += '<input type="number" class="form-control input-sm" name="education['+key_education+'][average_result]">';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix" style="margin-bottom:25px;">';
            t += '<div class="col-md-12 text-center">';
               t += '<button type="button" class="btn btn-white btn-remove btn-sm"><i class="fa fa-trash"></i></button>';
            t += '</div>';
         t += '</div>';
      t += '</div>';

      $('#form-registration').find('.sect-education').prepend(t);

      $('#form-registration .btn-remove').on('click', function (e) {
         $(this).parents('.form-group-attached').remove();
         e.preventDefault();
      });
   });

   $('.add-more-education-nonformal').on('click', function (e) {
      key_education_nonformal += 1;
      t = '';
      t += '<div class="form-group-attached">'
         t += '<div class="row clearfix" style="border-top:2px solid #e6e6e6">';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Jenis (Kursus/Loka-karya/Seminar/Pelatihan,dll)</label>';
                  t += '<input type="text" class="form-control input-sm" name="education_nonformal['+key_education_nonformal+'][type]" required="">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Bidang/Judul/Topik</label>';
                  t += '<input type="text" class="form-control input-sm" name="education_nonformal['+key_education_nonformal+'][title_education]" required="">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Penyelenggara</label>';
                  t += '<input type="text" class="form-control input-sm" name="education_nonformal['+key_education_nonformal+'][organizer]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Kota</label>';
                  t += '<input type="text" class="form-control input-sm" name="education_nonformal['+key_education_nonformal+'][city]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Lama Pendidikan</label>';
                  t += '<input type="text" class="form-control input-sm" name="education_nonformal['+key_education_nonformal+'][duration]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Tahun Ikut Serta</label>';
                  t += '<input type="text" class="form-control input-sm" name="education_nonformal['+key_education_nonformal+'][year_register]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Dibiayai oleh</label>';
                  t += '<input type="text" class="form-control input-sm" name="education_nonformal['+key_education_nonformal+'][financed_by]">';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix" style="margin-bottom:25px;">';
            t += '<div class="col-md-12 text-center">';
               t += '<button type="button" class="btn btn-white btn-remove btn-sm"><i class="fa fa-trash"></i></button>';
            t += '</div>';
         t += '</div>';
      t += '</div>';

      $('#form-registration').find('.sect-education-nonformal').prepend(t);

      $('#form-registration .btn-remove').on('click', function (e) {
         $(this).parents('.form-group-attached').remove();
         e.preventDefault();
      });
   });

   $('.add-more-language').on('click', function (e) {
      key_language += 1;
      t = '';
      t += '<div class="form-group-attached">'
         t += '<div class="row clearfix" style="border-top:2px solid #e6e6e6">';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Jenis Bahasa</label>';
                  t += '<input type="text" class="form-control input-sm" name="language['+key_language+'][language]" required="">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-2">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Mendengar</label>';
                  t += '<input type="number" class="form-control input-sm" name="language['+key_language+'][listening]" placeholder="(Skala 10 - 100)">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-2">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Berbicara</label>';
                  t += '<input type="number" class="form-control input-sm" name="language['+key_language+'][speaking]" placeholder="(Skala 10 - 100)">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-2">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Membaca</label>';
                  t += '<input type="number" class="form-control input-sm" name="language['+key_language+'][reading]" placeholder="(Skala 10 - 100)">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-2">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Menulis</label>';
                  t += '<input type="number" class="form-control input-sm" name="language['+key_language+'][writing]" placeholder="(Skala 10 - 100)">';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix" style="margin-bottom:25px;">';
            t += '<div class="col-md-12 text-center">';
               t += '<button type="button" class="btn btn-white btn-remove btn-sm"><i class="fa fa-trash"></i></button>';
            t += '</div>';
         t += '</div>';
      t += '</div>';

      $('#form-registration').find('.sect-language').prepend(t);

      $('#form-registration .btn-remove').on('click', function (e) {
         $(this).parents('.form-group-attached').remove();
         e.preventDefault();
      });
   });

   $('.add-more-work-history').on('click', function (e) {
      key_work_history += 1;
      t = '';
      t += '<div class="form-group-attached">'
         t += '<div class="row clearfix" style="border-top:2px solid #e6e6e6">';
            t += '<div class="col-sm-12">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Nama/Alamat/Telpon Perusahaan/Bidang usaha</label>';
                  t += '<textarea class="form-control input-sm" name="work_history['+key_work_history+'][company_name]" required="" style="height:35px;"></textarea>';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Masuk</label>';
                  t += '<input type="text" class="form-control input-sm" name="work_history['+key_work_history+'][work_in]" placeholder="Bulan & Tahun...">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Jabatan Sebagai</label>';
                  t += '<input type="text" class="form-control input-sm" name="work_history['+key_work_history+'][work_in_position]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Keluar</label>';
                  t += '<input type="text" class="form-control input-sm" name="work_history['+key_work_history+'][work_out]" placeholder="Bulan & Tahun...">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Jabatan Sebagai</label>';
                  t += '<input type="text" class="form-control input-sm" name="work_history['+key_work_history+'][work_out_position]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-3">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Div/Dept/Bagian/Area</label>';
                  t += '<input type="text" class="form-control input-sm" name="work_history['+key_work_history+'][division]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-3">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Nama atasan terakhir</label>';
                  t += '<input type="text" class="form-control input-sm" name="work_history['+key_work_history+'][company_head]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-3">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Gaji terakhir</label>';
                  t += '<input type="number" class="form-control input-sm" name="work_history['+key_work_history+'][last_salary]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-3">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Status Tetap/Kontrak</label>';
                  t += '<input type="text" class="form-control input-sm" name="work_history['+key_work_history+'][status_work]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Alasan Keluar</label>';
                  t += '<textarea class="form-control input-sm" name="work_history['+key_work_history+'][out_reason]" required="" style="height:25px;"></textarea>';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-6">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Uraian tugas pada jabatan terakhir</label>';
                  t += '<textarea class="form-control input-sm" name="work_history['+key_work_history+'][work_description]" required="" style="height:25px;"></textarea>';
               t += '</div>';
            t += '</div>';
         t += '<div class="row clearfix">';
            t += '<div class="col-md-12 text-center" style="margin-top:5px;margin-bottom:5px;">';
               t += '<button type="button" class="btn btn-white btn-remove btn-sm"><i class="fa fa-trash"></i></button>';
            t += '</div>';
         t += '</div>';
      t += '</div>';

      $('#form-registration').find('.sect-work-history').prepend(t);

      $('#form-registration .btn-remove').on('click', function (e) {
         $(this).parents('.form-group-attached').remove();
         e.preventDefault();
      });
   });

   $('.add-more-social').on('click', function (e) {
      key_social += 1;
      t = '';
      t += '<div class="form-group-attached">'
         t += '<div class="row clearfix" style="border-top:2px solid #e6e6e6">';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Nama Organisasi</label>';
                  t += '<input type="text" class="form-control input-sm" name="social_activity['+key_social+'][organization_name]" required="">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Jenis Kegiatan Organisasi</label>';
                  t += '<input type="text" class="form-control input-sm" name="social_activity['+key_social+'][organization_type]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-2">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Tahun Masuk</label>';
                  t += '<input type="text" class="form-control input-sm" name="social_activity['+key_social+'][year_in]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-2">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Tahun Keluar</label>';
                  t += '<input type="text" class="form-control input-sm" name="social_activity['+key_social+'][year_out]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-12">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Jabatan Terakhir</label>';
                  t += '<input type="text" class="form-control input-sm" name="social_activity['+key_social+'][last_position]">';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix">';
            t += '<div class="col-md-12 text-center" style="margin-top:5px;margin-bottom:5px;">';
               t += '<button type="button" class="btn btn-white btn-remove btn-sm"><i class="fa fa-trash"></i></button>';
            t += '</div>';
         t += '</div>';
      t += '</div>';

      $('#form-registration').find('.sect-social').prepend(t);

      $('#form-registration .btn-remove').on('click', function (e) {
         $(this).parents('.form-group-attached').remove();
         e.preventDefault();
      });
   });

   $('.add-more-accident').on('click', function (e) {
      key_accident += 1;
      t = '';
      t += '<div class="form-group-attached">'
         t += '<div class="row clearfix" style="border-top:2px solid #e6e6e6">';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default required">';
                  t += '<label>Jenis</label>';
                  t += '<input type="text" class="form-control input-sm" name="accident_history['+key_accident+'][accident_type]" required="">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-4">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Opname</label>';
                  t += '<input type="text" class="form-control input-sm" name="accident_history['+key_accident+'][opname_accident]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-2">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Lama</label>';
                  t += '<input type="text" class="form-control input-sm" name="accident_history['+key_accident+'][accident_time]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-2">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Tahun</label>';
                  t += '<input type="text" class="form-control input-sm" name="accident_history['+key_accident+'][year_accident]">';
               t += '</div>';
            t += '</div>';
            t += '<div class="col-sm-12">';
               t += '<div class="form-group form-group-default">';
                  t += '<label>Gangguan yg. ada sampai sekarang</label>';
                  t += '<input type="text" class="form-control input-sm" name="accident_history['+key_accident+'][interference_accident]">';
               t += '</div>';
            t += '</div>';
         t += '</div>';
         t += '<div class="row clearfix">';
            t += '<div class="col-md-12 text-center" style="margin-top:5px;margin-bottom:5px;">';
               t += '<button type="button" class="btn btn-white btn-remove btn-sm"><i class="fa fa-trash"></i></button>';
            t += '</div>';
         t += '</div>';
      t += '</div>';

      $('#form-registration').find('.sect-accident').prepend(t);

      $('#form-registration .btn-remove').on('click', function (e) {
         $(this).parents('.form-group-attached').remove();
         e.preventDefault();
      });
   });

});