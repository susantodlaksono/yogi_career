<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// require (APPPATH.'/libraries/REST_Controller.php');
require APPPATH . 'libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Api extends REST_Controller {
   
   public function __construct(){
      parent::__construct();
      // $this->load->helper('url');
      // $this->load->library('session');
   }

   public function assessment_get(){
      $params = $this->get();
      if(isset($params['limit']) && isset($params['offset'])){
         $applicant = $this->applicant_result($params);
         if($applicant){
            foreach ($applicant as $k => $v) {
               $data[$k] = $v;
               $data[$k]['result']['sect_one'] = $this->sect_one($v['id'], 3);
            }
            $this->response($data, 200);
         }else{
            $this->response( [
               'status' => false,
               'message' => 'No such data found'
            ], 404 );
         }
      }else{
         $this->response( [
            'status' => false,
            'message' => 'No such data found'
         ], 404 );
      }
   }

   public function vacancy_get(){
      $params = $this->get();
      $response = $this->vacancy_result($params);
      $this->response($response, 200);
      // echo 'tets';
   }

   private function vacancy_result($params){
      $this->db->select('id, name');
      if(isset($params['id']) && $params['id'] != ''){
         $this->db->where('id', $params['id']);
      }
      $this->db->where('status', 1);
      return $this->db->get('vacancy_division')->result_array();
   }

   private function applicant_result($params){
      $this->db->select('a.id, a.name, a.birth_date, a.age, a.gender, a.education_degree, a.school_majors, a.university, a.contact_number, a.email, a.created_date as register');
      $this->db->select('b.name as vacancy');
      $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id', 'left');
      if(isset($params['reg']) && $params['reg'] != ''){
         $this->db->where('DATE(a.created_date)', $params['reg']);
      }
      if(isset($params['vacancy']) && $params['vacancy'] != ''){
         $this->db->where('vacancy_division_id', $params['vacancy']);
      }
      return $this->db->get('applicant as a', $params['limit'], $params['offset'])->result_array();
   }

   private function sect_one($id, $test_type){
      $this->db->select('id');
		$this->db->where('applicant_id', $id);
		$this->db->where('test_type_id', $test_type);
		$result = $this->db->get('test_transaction')->row_array();	
		if($result){
			return $this->result_assest($result['id']);
		}else{
			return NULL;
		}
   }

   public function result_assest($trans_id){
		$this->db->where('test_transaction_id', $trans_id);
		$this->db->where('result', 100);
		return $this->db->count_all_results('test_answers');
	}
}