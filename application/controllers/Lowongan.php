<?php

class Lowongan extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('model_lowongan');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$data = array(
            'title' => 'Lowongan',
            'datalowongan' => $this->db->get('vacancy_division')->result_array(),
            'content' => 'themes/pages/admin/page/lowongan/lowongan'
    	);
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function detail($id){
        $data = array(
            'title' => 'Detail Lowongan',
            'group_test' => $this->model_lowongan->group_test($id),
            'test_type_list' => $this->db->select('id, name')->where('status', 1)->get('test_type')->result_array(),
            'lowongan' => $this->db->get_where('vacancy_division', array('id' => $id))->row_array(),
            'content' => 'themes/pages/admin/page/lowongan/detail_lowongan'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function tambah_test(){
        $post = $this->input->post();
        $check = $this->db->where('vacancy_division_id', $post['id'])->where('test_type_id', $post['test_id'])->get('test_groups');
        if($check->num_rows() == 0){
            $this->db->insert('test_groups', array('vacancy_division_id' => $post['id'], 'test_type_id' => $post['test_id'], 'status' => 1));
            $this->session->set_userdata('notif', 'Test berhasil di daftarkan ');
            redirect('lowongan/detail/'.$post['id'].'', 'refresh');
        }else{
            $this->session->set_userdata('notif', 'Test ini sudah terdaftar');
            redirect('lowongan/detail/'.$post['id'].'', 'refresh');
        }
    }

    public function update(){
        $post = $this->input->post();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('name', 'Nama Lowongan', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        $this->form_validation->set_message('required', '{field} wajib diisi');
        if($this->form_validation->run()){
            $entity = array(
                'name' => $post['name'],
                'status' => $post['status']
            );
            $insert = $this->db->update('vacancy_division', $entity, array('id' => $post['id']));
            if($insert){
                $this->session->set_userdata('notif', 'Lowongan berhasil di perbaharui');
                redirect('lowongan/detail/'.$post['id'].'', 'refresh');
            }else{
                $this->session->set_userdata('notif', 'Lowongan gagal di perbaharui');
                redirect('lowongan/detail/'.$post['id'].'', 'refresh');
            }
        }else{
            $this->session->set_userdata('notif', validation_errors());
            redirect('lowongan/detail/'.$post['id'].'', 'refresh');
        }
    }
}