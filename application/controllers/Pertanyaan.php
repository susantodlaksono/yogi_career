<?php

class Pertanyaan extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('model_pertanyaan');
        $this->load->library('upload');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$data = array(
            'title' => 'Pertanyaan',
            'test' => $this->model_pertanyaan->get_data(),
            'content' => 'themes/pages/admin/page/pertanyaan/pertanyaan'
    	);
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function tambah(){
        $data = array(
            'title' => 'Tambah Pertanyaan',
            'test' => $this->db->where('status', 1)->get('test_type')->result_array(),
            'content' => 'themes/pages/admin/page/pertanyaan/tambah_pertanyaan'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function edit($id, $testid){
        $pertanyaan = $this->model_pertanyaan->detail_pertanyaan($id);
        $data = array(
            'title' => 'Edit Pertanyaan',
            'pertanyaan' => $pertanyaan,
            'test_type_id' => $testid,
            'test' => $this->db->where('status', 1)->get('test_type')->result_array(),
            'content' => 'themes/pages/admin/page/pertanyaan/edit_pertanyaan'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function insert(){
        $post = $this->input->post();
        $insert_jawaban_berhasil = 0;

        if($post['description'] == ''){
            if(!empty($_FILES['question_image']['name']) || !empty($_FILES['question_file']['name'])){
                if(!isset($post['type_answers'])){
                    $this->session->set_userdata('notif', 'Jenis jawaban pertanyaan wajib diisi');
                    redirect('pertanyaan/tambah', 'refresh');    
                }
            }else{
                $this->session->set_userdata('notif', 'Gambar/File/Deskripsi pertanyaan wajib diisi');
                redirect('pertanyaan/tambah', 'refresh');
            }
        }else{
            if(!isset($post['type_answers'])){
                $this->session->set_userdata('notif', 'Jenis jawaban pertanyaan wajib diisi');
                redirect('pertanyaan/tambah', 'refresh');    
            }
        }
        if(isset($post['type_answers']) && $post['type_answers'] == 1){
            if(!isset($post['correct'])){
                $this->session->set_userdata('notif', 'Anda harus memilih satu jawaban benar');
                redirect('pertanyaan/tambah', 'refresh');   
            }
        }
        if(isset($post['type_answers']) && $post['type_answers'] == 4){
            if(!isset($post['correct'])){
                $this->session->set_userdata('notif', 'Anda harus memilih minimal 1 jawaban benar');
                redirect('pertanyaan/tambah', 'refresh');   
            }
        }

        $this->db->trans_start();

        $this->model_pertanyaan->insert_pertanyaan($post);
        $qid = $this->db->insert_id();

        if (!empty($_FILES['question_image']['name'])){
            $config['upload_path']  = './files/pertanyaan_gambar/';
            $config['allowed_types'] = '|png|jpg|jpeg|gif|';
            $config['file_name'] = uniqid(date("his").$qid);

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('question_image')) {
                $this->db->delete('question', array('id' => $qid));
                $this->session->set_userdata('notif', $this->upload->display_errors());
                redirect('pertanyaan/tambah', 'refresh'); 
            }else{
                $datafile = $this->upload->data();
                $this->model_pertanyaan->insert_gambar_pertanyaan($datafile['file_name'], $_FILES['question_image']['name'], $qid);
            }
        
        }
        
        if (!empty($_FILES['question_file']['name'])){
            $configs['upload_path']  = './files/pertanyaan_file/';
            $configs['allowed_types'] = '*';
            $configs['file_name'] = uniqid(date("his").$qid);

            $this->upload->initialize($configs);
            if (!$this->upload->do_upload('question_file')) {
                $this->db->delete('question', array('id' => $qid));
                $this->session->set_userdata('notif', $this->upload->display_errors());
                redirect('pertanyaan/tambah', 'refresh'); 
            }else{
                $datafile = $this->upload->data();
                $this->model_pertanyaan->insert_file_pertanyaan($datafile['file_name'], $_FILES['question_file']['name'], $qid);
            }
        }
        
        if($post['type_answers'] == 1){
            foreach ($post['ans_mchoice'] as $key => $value) {
                $insert_jawaban = $this->model_pertanyaan->insert_jawaban_pertanyaan($qid, $key, $value);
                $insert_jawaban ? $insert_jawaban_berhasil++ : FALSE;   
            }
            if($insert_jawaban_berhasil != count($post['ans_mchoice'])){
                $this->db->trans_rollback();
                $this->session->set_userdata('notif', 'Transaksi jawaban gagal, seluruh transaksi dibatalkan');
                redirect('pertanyaan/tambah', 'refresh');
            }

        }

        if($post['type_answers'] == 4){
            foreach ($post['ans_manswers'] as $key => $value) {
                $insert_jawaban = $this->model_pertanyaan->insert_jawaban_pertanyaan($qid, $key, $value);
                $insert_jawaban ? $insert_jawaban_berhasil++ : FALSE;   
            }
            if($insert_jawaban_berhasil != count($post['ans_manswers'])){
                $this->db->trans_rollback();
                $this->session->set_userdata('notif', 'Transaksi jawaban gagal, seluruh transaksi dibatalkan');
                redirect('pertanyaan/tambah', 'refresh');    
            }

        }
        
        $this->db->trans_complete();

        if($this->db->trans_status()){
            $this->db->trans_commit();
            $this->session->set_userdata('notif', 'Pertanyaan berhasil ditambahkan');
            redirect('pertanyaan/tambah', 'refresh');
        }else{
            $this->db->trans_rollback();
            $this->session->set_userdata('notif', 'Pertanyaan gagal ditambahkan');
            redirect('pertanyaan/tambah', 'refresh');
        }
    }

    public function update(){
        $post = $this->input->post();
        $insert_jawaban_berhasil = 0;

        if($post['description'] == ''){
            if(!empty($_FILES['question_image']['name']) || !empty($_FILES['question_file']['name'])){
                if(!isset($post['type_answers'])){
                    $this->session->set_userdata('notif', 'Jenis jawaban pertanyaan wajib diisi');
                    redirect('pertanyaan/edit/'.$post['id'].'/'.$post['test_type_id'].'', 'refresh');    
                }
            }else{
                $this->session->set_userdata('notif', 'Gambar/File/Deskripsi pertanyaan wajib diisi');
                redirect('pertanyaan/edit/'.$post['id'].'/'.$post['test_type_id'].'', 'refresh');    
            }
        }
        if(isset($post['question_type_id']) && $post['question_type_id'] == 1){
            if(!isset($post['correct'])){
                $this->session->set_userdata('notif', 'Anda harus memilih satu jawaban benar');
                redirect('pertanyaan/edit/'.$post['id'].'/'.$post['test_type_id'].'', 'refresh');
            }
        }

        $this->db->trans_start();

        $this->model_pertanyaan->update_pertanyaan($post);

        if (!empty($_FILES['question_image']['name'])){
            $config['upload_path']  = './files/pertanyaan_gambar/';
            $config['allowed_types'] = '|png|jpg|jpeg|gif|';
            $config['file_name'] = uniqid(date("his").$post['id']);

            $this->upload->initialize($config);
            if (!$this->upload->do_upload('question_image')) {
                $this->session->set_userdata('notif', $this->upload->display_errors());
                redirect('pertanyaan/edit/'.$post['id'].'/'.$post['test_type_id'].'', 'refresh');
            }else{
                $datafile = $this->upload->data();
                if(isset($post['image_before']) && $post['image_before'] != ''){
                    $update_file = $this->model_pertanyaan->update_gambar_pertanyaan($datafile['file_name'], $_FILES['question_image']['name'], $post['image_id']);
                    if($update_file){
                        if (file_exists('./files/pertanyaan_gambar/'.$post['image_before'].'')) {
                            unlink('./files/pertanyaan_gambar/'.$post['image_before'].'');
                        }
                    }
                }else{
                    $this->model_pertanyaan->insert_gambar_pertanyaan($datafile['file_name'], $_FILES['question_image']['name'], $post['id']);
                }
            }
        
        }
        
        if (!empty($_FILES['question_file']['name'])){
            $configs['upload_path']  = './files/pertanyaan_file/';
            $configs['allowed_types'] = '*';
            $configs['file_name'] = uniqid(date("his").$post['id']);

            $this->upload->initialize($configs);
            if (!$this->upload->do_upload('question_file')) {
                $this->session->set_userdata('notif', $this->upload->display_errors());
                redirect('pertanyaan/edit/'.$post['id'].'/'.$post['test_type_id'].'', 'refresh'); 
            }else{
                $datafile = $this->upload->data();
                if(isset($post['file_before']) && $post['file_before'] != ''){
                    $update_file = $this->model_pertanyaan->update_file_pertanyaan($datafile['file_name'], $_FILES['question_file']['name'], $post['file_id']);
                    if($update_file){
                        if (file_exists('./files/pertanyaan_file/'.$post['file_before'].'')) {
                            unlink('./files/pertanyaan_file/'.$post['file_before'].'');
                        }
                    }
                }else{
                    $this->model_pertanyaan->insert_file_pertanyaan($datafile['file_name'], $_FILES['question_file']['name'], $post['id']);
                }
            }
        }

        if($post['question_type_id'] == 1){
            foreach ($post['ans_mchoice'] as $key => $value) {
                $mc['question_answers'] = $value['result'];
                $mc['correct_answers'] = $value['correct'] ? $value['correct'] : NULL;
                if(isset($value['updated'])){
                    $wheremc['id'] = $value['updated'];
                    $this->db->update('question_answers', $mc, $wheremc);
                }else{
                    $mc['question_id'] = $post['id'];
                    $this->db->insert('question_answers', $mc);
                }
            }
        }

        if($post['question_type_id'] == 4){
            foreach ($post['ans_manswers'] as $key => $value) {
                $mc['question_answers'] = $value['result'];
                $mc['correct_answers'] = $value['correct'] ? $value['correct'] : NULL;
                if(isset($value['updated'])){
                    $wheremc['id'] = $value['updated'];
                    $this->db->update('question_answers', $mc, $wheremc);
                }else{
                    $mc['question_id'] = $post['id'];
                    $this->db->insert('question_answers', $mc);
                }
            }
        }

        $this->db->trans_complete();

        if($this->db->trans_status()){
            $this->db->trans_commit();
            $this->session->set_userdata('notif', 'Pertanyaan berhasil diperbaharui');
            redirect('pertanyaan/detail/'.$post['test_type_id'].'', 'refresh');
        }else{
            $this->db->trans_rollback();
            $this->session->set_userdata('notif', 'Pertanyaan gagal diperbaharui');            
            redirect('pertanyaan/detail/'.$post['test_type_id'].'', 'refresh');
        }
    }

    public function detail($id){
        $tipe_pertanyaan = $this->model_pertanyaan->tipe_pertanyaan($id);
        $pertanyaan = $this->model_pertanyaan->get_pertanyaan($id);
        $data = array(
            'title' => 'Detail Pertanyaan',
            'test_type_id' => $id,
            'tipe_pertanyaan' => $tipe_pertanyaan,
            'pertanyaan' => $pertanyaan,
            'test' => $result = $this->db->get_where('test_type', array('id' => $id))->row_array(),
            'content' => 'themes/pages/admin/page/pertanyaan/detail_pertanyaan'
        );
        $this->load->view('themes/pages/admin/index', $data);
    }

    public function delete_jawaban(){
        $post = $this->input->get();
        $response['success'] = $this->db->delete('question_answers', array('id' => $post['id']));
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function status($id, $status){
        $tmp['status'] = $status;
        $where['id'] = $id;
        $result = $this->db->update('test_type', $tmp, $where);
        if($result){
            $this->session->set_userdata('notif', 'Status test berhasil di perbaharui');
        }else{
            $this->session->set_userdata('notif', 'Status test gagal di perbaharui');
        }
        redirect('manage_test', 'refresh');
    }

    public function hapus($id, $test_type_id){
        $gambar = $this->db->where('question_id', $id)->get('question_image')->row_array();
        $file = $this->db->where('question_id', $id)->get('question_file')->row_array();
        if($gambar){
            if (file_exists('./files/pertanyaan_gambar/'.$gambar['image'].'')){
                unlink('./files/pertanyaan_gambar/'.$gambar['image'].'');
            }
        }
        if($file){
            if (file_exists('./files/pertanyaan_file/'.$file['file'].'')){
                unlink('./files/pertanyaan_file/'.$file['file'].'');
            }
        }
        $result = $this->db->delete('question', array('id' => $id));
        if($result){
            $this->session->set_userdata('notif', 'pertanyaan berhasil di hapus');
        }else{
            $this->session->set_userdata('notif', 'pertanyaan gagal di hapus');
        }
        redirect('pertanyaan/detail/'.$test_type_id.'', 'refresh');
    }
}