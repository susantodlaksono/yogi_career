<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Simulation extends MY_Controller {
    
 	public function __construct() {
     	parent::__construct();
     	if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
      	redirect('security');
     	}
 	}

 	public function psikotest(){
 		$sdate = '2018-01-01';
 		$edate = date('Y-m-d');
 		$applicant = $this->get_applicant($sdate, $edate, 1);
 		if($applicant->num_rows() > 0){
 			echo '<table border="1">';
	 			echo '<tr>';
	 				echo '<td>Name</td>';
	 				echo '<td width="100">Birth Day</td>';
	 				echo '<td>Age</td>';
	 				echo '<td>Gender</td>';
	 				echo '<td>Education Degree</td>';
	 				echo '<td>School Majors</td>';
	 				echo '<td>University</td>';
	 				echo '<td>Contact Number</td>';
	 				echo '<td>Email</td>';
	 				echo '<td>Level</td>';
	 				echo '<td>Vacancy</td>';
	 				echo '<td>Result</td>';
	 			echo '</tr>';
	 			foreach ($applicant->result_array() as $v) {
	 				echo '<tr>';
	 					echo '<td>'.$v['name'].'</td>';
	 					echo '<td>'.date('d M Y', strtotime($v['birth_date'])).'</td>';
	 					echo '<td>'.$v['age'].'</td>';
	 					echo '<td>'.$v['gender'].'</td>';
	 					echo '<td>'.$v['education_degree'].'</td>';
	 					echo '<td>'.$v['school_majors'].'</td>';
	 					echo '<td>'.$v['university'].'</td>';
	 					echo '<td>'.$v['contact_number'].'</td>';
	 					echo '<td>'.$v['email'].'</td>';
	 					echo '<td>'.($v['level'] == 1 ? 'Fresh Graduate' : 'Expert').'</td>';
	 					echo '<td>'.$v['vacancy_name'].'</td>';
	 					$psikotest = $this->_psikotes_counting(1, $v['trans_id']);
	 					echo '<td>'.$psikotest.'</td>';
	 				echo '</tr>';
	 			}
 			echo '</table>';
 		}else{
 			echo 'No Result';
 		}
 	}

 	public function papi_kostick(){
 		$this->load->library('papi_kostick');
 		$sdate = '2018-01-01';
 		$edate = date('Y-m-d');
 		$applicant = $this->get_applicant($sdate, $edate, 1);
 		if($applicant->num_rows() > 0){
 			echo '<table border="1">';
	 			echo '<tr>';
	 				echo '<td>Name</td>';
	 				echo '<td width="100">Birth Day</td>';
	 				echo '<td>Age</td>';
	 				echo '<td>Gender</td>';
	 				echo '<td>Education Degree</td>';
	 				echo '<td>School Majors</td>';
	 				echo '<td>University</td>';
	 				echo '<td>Contact Number</td>';
	 				echo '<td>Email</td>';
	 				echo '<td>Level</td>';
	 				echo '<td>Vacancy</td>';
	 				echo '<td>Result</td>';
	 			echo '</tr>';
	 			foreach ($applicant->result_array() as $v) {
	 				echo '<tr>';
	 					echo '<td>'.$v['name'].'</td>';
	 					echo '<td>'.date('d M Y', strtotime($v['birth_date'])).'</td>';
	 					echo '<td>'.$v['age'].'</td>';
	 					echo '<td>'.$v['gender'].'</td>';
	 					echo '<td>'.$v['education_degree'].'</td>';
	 					echo '<td>'.$v['school_majors'].'</td>';
	 					echo '<td>'.$v['university'].'</td>';
	 					echo '<td>'.$v['contact_number'].'</td>';
	 					echo '<td>'.$v['email'].'</td>';
	 					echo '<td>'.($v['level'] == 1 ? 'Fresh Graduate' : 'Expert').'</td>';
	 					echo '<td>'.$v['vacancy_name'].'</td>';
	 					$papi = $this->papi_kostick->counting($v['trans_id']);
	 					echo '<td>';
 						foreach ($papi as $key => $value) {
 							echo ''.$key.' : '.$value.'<br>';
 						}
	 					echo '</td>';
	 				echo '</tr>';
	 			}
 			echo '</table>';
 		}else{
 			echo 'No Result';
 		}
 	}

 	public function get_applicant($sdate, $edate, $test_type_id){
 		$this->db->select('a.id as trans_id, b.*, c.name as vacancy_name');
 		$this->db->join('applicant as b', 'a.applicant_id = b.id');
 		$this->db->join('vacancy_division as c', 'b.vacancy_division_id = c.id');
 		$this->db->where('date(a.time_start) between "'.$sdate.'" and "'.$edate.'"');
 		$this->db->where('a.test_type_id', $test_type_id);
 		return $this->db->get('test_transaction as a');
 	}

 	private function _psikotes_counting($test_type_id, $test_transaction_id){
     	$question_id = $this->_question_list(7, $test_type_id); 
     	$_i = $this->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'I')->get('test_answers')->num_rows();
     	$_e = $this->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'E')->get('test_answers')->num_rows();
     	$_s = $this->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'S')->get('test_answers')->num_rows();
     	$_n = $this->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'N')->get('test_answers')->num_rows();
     	$_t = $this->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'T')->get('test_answers')->num_rows();
     	$_f = $this->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'F')->get('test_answers')->num_rows();
     	$_j = $this->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'J')->get('test_answers')->num_rows();
     	$_p = $this->db->where_in('question_id', $question_id)->where('test_transaction_id', $test_transaction_id)->where('answers', 'P')->get('test_answers')->num_rows();
        
     	$_ie = $_i > $_e ? 'I' : 'E';
     	$_sn = $_s > $_n ? 'S' : 'N';
     	$_tf = $_t > $_f ? 'T' : 'F';
     	$_jp = $_j > $_p ? 'J' : 'P';

     	return $_ie.$_sn.$_tf.$_jp;
 	}

 	private function _question_list($question_type_id, $test_type_id){
     	$this->db->select('id');
     	$this->db->where('test_type_id', $test_type_id);
     	$this->db->where('question_type_id', $question_type_id);
     	$rs = $this->db->get('question');
     	if($rs->num_rows() > 0){
         foreach ($rs->result_array() as $key => $value) {
          	$result[] = $value['id'];
         }
         return $result;
     	}else{
         return FALSE;
     	}
 	}

 	public function read_pdf(){
 		header('Content-Type: application/pdf');
        $data = file_get_contents('./files/cv/0131570000005bed12dd0c539.pdf');
        echo $data;
 	}

 	public function migration($database){
 		$this->_db = $this->load->database($database, TRUE);

 		$applicant = $this->_db->get('applicant', 2, 0)->result_array();

 		foreach ($applicant as $k => $v) {
			//Ambil data users
 			$users = $this->getUsers($v['user_id'], $database);
 			if($users){

 				// 1. Ambil data
				$applicant_file = $this->getApplicantFile($v['id'], $database);
				$test_transaction = $this->getTestTransaction($v['id'], $database);

 				//Insert new user dan ambil id usernya
				unset($users['id']);
 				$user_id = $this->insertUser($users);
 				
 				unset($v['id']);
 				$v['user_id'] = $user_id;
				$applicant_id = $this->insertApplicant($v);

				//Insert data
				if($applicant_file){
	 				$this->insertApplicantFile($applicant_file, $applicant_id);
 				}
 				if($test_transaction){
 					foreach ($test_transaction as $vv) {

 						$test_answers = $this->getTestAnswers($vv['id'], $database);

 						unset($vv['id']);
 						$vv['applicant_id'] = $applicant_id;
 						$transaction_id = $this->insertTestTransaction($vv);	
 					}
 				}
	 			
	 			echo $applicant_id."\r\n";
 			}
 		}
 	}	

 	public function getApplicantFile($applicant_id, $database){	
 		$this->_db = $this->load->database($database, TRUE);
 		$this->_db->where('applicant_id', $applicant_id); 	
 		return $this->_db->get('applicant_file')->result_array();
 	}

 	public function getTestTransaction($applicant_id, $database){	
 		$this->_db = $this->load->database($database, TRUE);
 		$this->_db->where('applicant_id', $applicant_id); 	
 		return $this->_db->get('test_transaction')->result_array();
 	}

 	public function insertApplicantFile($data, $applicant_id){	
 		foreach ($data as $v) {
 			$v['applicant_id'] = $applicant_id;
 			$this->db->insert('applicant_file', $v);
 		}
 	}

 	public function insertTestTransaction($data, $applicant_id, $database){	
 		$this->db->insert('test_transaction', $v);
 		return $this->db->insert_id();
 	}

 	public function getApplicant($database){	
 		$this->_db = $this->load->database($database, TRUE);
 		$this->_db->select('id');
 		return $this->_db->get('applicant');
 	}

 	public function insertUser($v){	
 		$this->db->insert('users', $v);
 		return $this->db->insert_id();
 	}

 	public function insertApplicant($v){	
 		$this->db->insert('applicant', $v);
 		return $this->db->insert_id();
 	}

 	public function getUsers($user_id, $database){	
 		$this->_db = $this->load->database($database, TRUE);
 		$this->_db->where('id', $user_id);
 		return $this->_db->get('users')->row_array();
 	}

 	public function clear(){
 		$this->db->where_not_in('id', array(8,514));
 		return $this->db->delete('users');
 		// $this->_db->delete('users', array());
 	}

	public function all_results_test(){
		$this->load->library('result_assessment');
		$applicant = $this->db->select('a.*, b.name as vacancy_name')->join('vacancy_division as b', 'a.vacancy_division_id = b.id', 'left')->order_by('a.created_date', 'desc')->get('applicant as a')->result_array();
		echo '<table border=1>';
		echo '<tr>';
		echo '<td>Register</td>';
		echo '<td>Nama</td>';
		echo '<td>Email</td>';
		echo '<td>Telp</td>';
		echo '<td>Pendidikan</td>';
		echo '<td>Posisi</td>';
		echo '<td>Tes 1</td>';
		echo '<td>Tes 2 Bag 1</td>';
		echo '<td>Test 2 Bag 2</td>';
		echo '<td>Test 2 Bag 3</td>';
		echo '<td>Test 2 Bag 4</td>';
		echo '<td>Psikotest</td>';
		echo '<td>Papi Kostick</td>';
		echo '</tr>';
		foreach ($applicant as $v) {
			$result = $this->get_results($v['id']);
			$papi = $this->get_papi($v['id']);
			$psikotest = $this->psikotest_result($v['id']);
			echo '<tr>';
			echo '<td>'.date('d M Y', strtotime($v['created_date'])).'</td>';
			echo '<td>'.$v['name'].'</td>';
			echo '<td>'.$v['email'].'</td>';
			echo '<td>`'.$v['contact_number'].'</td>';
			echo '<td>'.$v['education_degree'].'</td>';
			echo '<td>'.$v['vacancy_name'].'</td>';
			echo '<td>'.($result ? $result[4] : '').'</td>';
			echo '<td>'.($result ? $result[0] : '').'</td>';
			echo '<td>'.($result ? $result[1] : '').'</td>';
			echo '<td>'.($result ? $result[2] : '').'</td>';
			echo '<td>'.($result ? $result[3] : '').'</td>';
			echo '<td>'.($psikotest ? $psikotest : '').'</td>';
			if($papi){
				$ecd = json_decode($papi);
				echo '<td>';
				foreach ($ecd as $kk => $vv) {
					echo $kk.':'.$vv.' ';
				}
				echo '</td>';
			}else{
				echo '<td></td>';	
			}
			echo '</tr>';
		}
		echo '</table>';
	}

	public function get_papi($id){
		$this->db->select('id');
		$this->db->where('applicant_id', $id);
		$this->db->where('test_type_id', 4);
		$result = $this->db->get('test_transaction')->row_array();	
		if($result){
			return $this->result_papi($result['id']);
		}else{
			return FALSE;
		}
	}

	public function psikotest_result($id){
		$this->db->select('id');
		$this->db->where('applicant_id', $id);
		$this->db->where('test_type_id', 1);
		$result = $this->db->get('test_transaction')->row_array();	
		if($result){
			return $this->result_papi($result['id']);
		}else{
			return FALSE;
		}
	}

	public function get_results($id){
		$test = array(16,17,18,19,3);
		$i = 0;
		foreach ($test as $v) {
			$rs = $this->mapping_trans($id, $v);
			$data[$i] = $rs ? $rs : '';
			$i++;
		}
		return $data;
	}

	public function mapping_trans($id, $test_type){
		$this->db->select('id');
		$this->db->where('applicant_id', $id);
		$this->db->where('test_type_id', $test_type);
		$result = $this->db->get('test_transaction')->row_array();	
		if($result){
			return $this->result_assest($result['id']);
		}else{
			return FALSE;
		}
	}
	public function result_assest($trans_id){
		$this->db->where('test_transaction_id', $trans_id);
		$this->db->where('result', 100);
		return $this->db->count_all_results('test_answers');
	}

	public function result_papi($trans_id){
		$this->db->select('assessment');
		$this->db->where('test_transaction_id', $trans_id);
		$rs = $this->db->get('test_assessment')->row_array();
		if($rs){
			return $rs['assessment'];
		}else{
			return FALSE;
		}
	}

	public function test(){
		$this->load->library('result_assessment');
		$applicant = $this->db->select('a.*, b.name as vacancy_name')->join('vacancy_division as b', 'a.vacancy_division_id = b.id', 'left')->get('applicant as a', 10, 0)->result_array();
		$i = 0;
		foreach ($applicant as $k => $v) {
			$data[$k] = $v;
			$data[$k]['test'] = $this->result_assessment->summary_simulation($v['user_id'], $v['vacancy_division_id']);
			$i++;
		}
		// $data = $this->result_assessment->summary_simulation(1547, 5);
		echo json_encode($data);
	}

}		