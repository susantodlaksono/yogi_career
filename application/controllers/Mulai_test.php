<?php

class Mulai_test extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('tests');
        $this->load->model('assessment');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
    	$get = $this->input->get();
    	$data = array(
            'title' => 'Test',
            'id' => $get['id'],
            'detail_tests' => $this->tests->detail_tests($get['id'], $this->_user->id),
            'content' => 'themes/pages/pelamar/page/test',
        );
        $this->load->view('themes/pages/pelamar/index', $data);
    }

    public function register_tests(){
        $p = $this->input->post();
        $rules = $this->tests->rules_tests($p['reg_id'], $this->_user->id);
        if($rules){
            $register_tests = $this->tests->register_tests($p['reg_id'], $p['time_test'], $this->_user->id);
            if($register_tests){
                $response['success'] = TRUE;
            }else{
                $response['success'] = FALSE;
                $response['msg'] = 'Register failed';
            }
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'You dont have permissions'; 
        }
        $this->json_result($response);
    }

    public function finish_tests(){
        $p = $this->input->get();
        $this->load->model('assessment');
        if($p['checking_mode'] == 'not_true'){
            $get_mode_test = $this->db->select('mode_test')->where('id', $p['test_id'])->get('test_type')->row_array();
            $check_question_type_id = $this->db->where('test_type_id', $p['test_id'])->where_in('question_type_id', array(1,7,8))->get('question');
            if($check_question_type_id->num_rows() > 0){
                $check = $this->db->select('id')->where('test_type_id', $p['test_id'])->get('question');
                foreach ($check->result_array() as $key => $value) {
                    $question_id[] = $value['id'];
                }
                $total_question = $check->num_rows();
                $check_total_answers = $this->db->where('test_transaction_id', $p['test_transaction_id'])->where_in('question_id', $question_id)->get('test_answers');
                if($get_mode_test['mode_test'] == 1){
                    if($total_question == $check_total_answers->num_rows()){
                        $finish_tests = $this->tests->finish_tests($p['test_id'], $p['test_transaction_id']);
                        $counting = $this->assessment->counting_result($p['test_id'], $p['test_transaction_id']);
                        if($finish_tests && $counting){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                            $response['msg'] = 'Register for finishing tests failed';
                        }
                    }else{
                        $response['success'] = FALSE;
                        $response['msg'] = 'You must complete all the question';
                    }          
                }else{
                    $finish_tests = $this->tests->finish_tests($p['test_id'], $p['test_transaction_id']);
                    $counting = $this->assessment->counting_result($p['test_id'], $p['test_transaction_id']);
                    if($finish_tests && $counting){
                        $response['success'] = TRUE;
                    }else{
                        $response['success'] = FALSE;
                        $response['msg'] = 'Register for finishing tests failed';
                    }
                }    
            }else{
                $finish_tests = $this->tests->finish_tests($p['test_id'], $p['test_transaction_id']);
                if($finish_tests){
                    $response['success'] = TRUE;
                }else{
                    $response['success'] = FALSE;
                    $response['msg'] = 'Register for finishing tests failed';
                }
            }
        }else{
            $finish_tests = $this->tests->finish_tests($p['test_id'], $p['test_transaction_id']);
            if($finish_tests){
                $response['success'] = TRUE;
            }else{
                
                $response['testing'] = 'testing';
                $response['msg'] = 'Register for finishing tests failed';
            }
        }
        $this->json_result($response);
    }

    public function get_questions(){
        $p = $this->input->get();
        if($p['question_id'] || is_numeric($p['question_id'])){
            $question = $this->tests->get_question($p['question_id']);
            if($question){
                $response['question'] = $question;
                
                $previous_id = $this->db->select('max(id) as previous_id')->where('id < ', $question['id'])->where('test_type_id', $question['test_type_id'])->get('question')->row_array('previous_id');
                $next_id = $this->db->select('min(id) as next_id')->where('id > ', $question['id'])->where('test_type_id', $question['test_type_id'])->get('question')->row_array('previous_id');
                $max_soal = $this->db->select('max(id) as soal_terakhir')->where('test_type_id', $question['test_type_id'])->get('question')->row_array();
                $min_soal = $this->db->select('min(id) as soal_pertama')->where('test_type_id', $question['test_type_id'])->get('question')->row_array();

                $response['previous_id'] = $previous_id['previous_id'];
                $response['next_id'] = $next_id['next_id'];
                $response['max_soal'] = $max_soal['soal_terakhir'];
                $response['min_soal'] = $min_soal['soal_pertama'];

                $answers_question = $this->db->select('answers,answers_temp_file')->where('test_transaction_id', $p['test_transaction_id'])->where('question_id', $p['question_id'])->get('test_answers')->row_array();
                // $response['answers_question'] = $answers_question['answers'] ? $answers_question['answers'] : FALSE;
                
                if($question['question_type_id'] == 5 || $question['question_type_id'] == 6 || $question['question_type_id'] == 4){
                    $response['answers_question'] = $answers_question['answers'] ? json_decode($answers_question['answers'], TRUE) : FALSE;
                }else if($question['question_type_id'] == 2){
                    $response['answers_question'] = $answers_question['answers'] ? $answers_question['answers'] : FALSE;
                    $response['answers_question_temp_file'] = $answers_question['answers_temp_file'] ? $answers_question['answers_temp_file'] : FALSE;
                }else{
                    $response['answers_question'] = $answers_question['answers'] ? $answers_question['answers'] : FALSE;
                }

                $response['success'] = TRUE;
            }else{
                $response['success'] = FALSE;
                $response['msg'] = 'Question Not Found';
            }
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'Soal Tidak Di temukan';
        }
        $this->json_result($response);
    }



    public function save_answers(){
        ini_set('memory_limit', '1G');
        $p = $this->input->post();
        if(isset($p['previous_id'])){
            $correct_answers_checking = $p['question_type_id'] == 2 ? $_FILES['correct_answers']['name'] : $p['correct_answers'];
            if(isset($correct_answers_checking)){
                if($p['type'] == 'save'){
                    if($p['question_type_id'] == 2){
                        if (!empty($_FILES['correct_answers']['name'])){
                            $config['upload_path']  = './files/pelamar_upload/';
                            $config['allowed_types'] = 'zip|pptx|docx|doc|png|jpeg|jpg|rar';
                            $config['max_size'] = 10000000000;
                            $rep_name = str_replace("_", "-", $_FILES['correct_answers']['name']);
                            $config['file_name'] = uniqid(date("his").$this->_applicant_id.$p['q_id']);

                            $this->load->library('upload', $config);
                            $checking_file = $this->tests->checking_file($p['q_id'], $p['test_transaction_id']);
                            if($checking_file){
                                if(file_exists('./files/pelamar_upload/'.$checking_file)){
                                    unlink('./files/pelamar_upload/'.$checking_file);
                                }
                            }
                            if (!$this->upload->do_upload('correct_answers')) {
                                $response['success'] = FALSE;
                                $response['msg'] = $this->upload->display_errors();   
                                $this->json_result($response);
                                exit();                 
                            }else{
                                $datafile = $this->upload->data();
                                $correct_answers =  $datafile['file_name'];
                                $answers_file_temp_name =  $_FILES['correct_answers']['name'];
                                $response['answers_file'] = $datafile['file_name'];
                                $response['answers_file_temp_name'] = $_FILES['correct_answers']['name'];
                            }
                        }else{
                            $response['success'] = FALSE;
                            $response['msg'] = 'Upload your file';   
                            $this->json_result($response);
                            exit();                 
                        }
                    }else{
                        $correct_answers = $p['correct_answers'];
                        $answers_file_temp_name = NULL;
                    }

                    $result = $this->db->where('test_transaction_id', $p['test_transaction_id'])->where('question_id', $p['q_id'])->get('test_answers');
                    if($p['question_type_id'] == 1){
                        $answers_assessment = $p['correct_answers'] == $p['correct_answers_question'] ? 1 : NULL;
                    }else{
                        $answers_assessment = NULL;
                    }
                    if($result->num_rows() == 0){
                        $ins = array(
                            'question_id' => $p['q_id'],
                            'test_transaction_id' => $p['test_transaction_id'],
                            'answers' => $correct_answers,
                            'answers_temp_file' => $answers_file_temp_name,
                            'result' => $answers_assessment
                        );
                        $save = $this->db->insert('test_answers', $ins);
                        if($save){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                        }
                    }else{
                        $upd = array(
                            'answers' => $correct_answers,
                            'answers_temp_file' => $answers_file_temp_name,
                            'result' => $answers_assessment
                        );
                        $update = $this->db->update('test_answers', $upd, array('test_transaction_id' => $p['test_transaction_id'], 'question_id' => $p['q_id']));
                        if($update){
                            $response['success'] = TRUE;
                        }else{
                            $response['success'] = FALSE;
                        }
                    }
                }else{
                    $response['success'] = TRUE;
                }
                $response['answered'] = TRUE;
            }else{
                $response['success'] = TRUE;
                $response['answered'] = FALSE;
            }
        }else{
            $response['success'] = TRUE;
        }
        $response['previous_id'] = $p['previous_id'];
        $response['next_id'] = $p['next_id'];
        $response['mode'] = $p['mode'];
        $response['question_id'] = $p['q_id']; 

        $this->json_result($response);
    }

    public function next_step(){
        $p = $this->input->post();
        $check_answers = $this->db->where('question_id', $p['q_id'])->where('test_transaction_id', $p['test_transaction_id'])->count_all_results('test_answers');
        if($check_answers > 0){
            $response['answered'] = TRUE;
        }else{
            $response['answered'] = FALSE;
        }
        $response['success'] = TRUE;
        $response['previous_id'] = $p['previous_id'];
        $response['next_id'] = $p['next_id'];
        $response['mode'] = $p['mode'];
        $response['question_id'] = $p['q_id'];
        $this->json_result($response);
    }

    public function save_answers_multiple(){
        $params = $this->input->get();
        $response['answered'] = FALSE;

        $result = $this->db->where('test_transaction_id', $params['test_transaction_id'])->where('question_id', $params['question_id'])->get('test_answers');
        if($result->num_rows() == 0){
            $ins = array(
                'question_id' => $params['question_id'],
                'test_transaction_id' => $params['test_transaction_id'],
                'answers' => json_encode($params['answers_multiple']),
                'result' => $this->tests->assessment_multiple_choice($params['question_id'], $params['answers_multiple'])

            );
            $response['total'] = count($params['answers_multiple']);
            $response['success'] = $this->db->insert('test_answers', $ins);
        }else{
            if(isset($params['answers_multiple'])){
                $total = count($params['answers_multiple']);
                if($total <= 2){
                    $update = array(
                        'answers' => json_encode($params['answers_multiple']),
                        'result' => $this->tests->assessment_multiple_choice($params['question_id'], $params['answers_multiple'])

                    );
                    $where = array(
                        'question_id' => $params['question_id'],
                        'test_transaction_id' => $params['test_transaction_id']
                    );
                    $response['success'] = $this->db->update('test_answers', $update, $where);
                    $response['answered'] = $total == 2 ? TRUE : FALSE;
                }else{
                    $response['success'] = FALSE;
                    $response['answered'] = TRUE;
                }
            }else{
                $where = array(
                    'question_id' => $params['question_id'],
                    'test_transaction_id' => $params['test_transaction_id']
                );
                $response['success'] = $this->db->delete('test_answers', $where);
            }
        }
        $response['question_id'] = $params['question_id'];
        $this->json_result($response);
    }

    public function applicant_download($file){
        $this->load->helper('download');
        $temp_name = $this->db->select('answers_temp_file')->where('answers', $file)->get('test_answers')->row_array();
        $data = file_get_contents('./files/pelamar_upload/'.$file.'');
        force_download($temp_name['answers_temp_file'], $data);
    }
}