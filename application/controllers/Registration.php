<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registration extends MY_Controller {
	
	public function index(){
		$data = array(
	    	'date' => date('d M Y'),
		);
		$this->load->view('themes/pages/registration',$data);
	}
}