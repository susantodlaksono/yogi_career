<?php

class Security extends MX_Controller {

    protected $theme_path = 'themes/pages/';

    public function __construct() {
        parent::__construct();
        $this->load->add_package_path(APPPATH . 'third_party/ion_auth/');
        $this->load->library('ion_auth');
        $this->load->library('validation_form');    
    }

    private function json_result($result = array()) {
        header('Content-Type: application/json');
        echo json_encode($result);
        exit();
    }

    public function index(){
        if ($this->ion_auth->logged_in()) {
            redirect('main');
        }
    	$data = array(
            'title' => 'Beranda'
    	);
    	$this->load->view($this->theme_path . 'login', $data);
    }

    public function login() {
        if ($this->input->post()) {
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $remember = true;
            $redirect = $this->input->post('redirect') ? $this->input->post('redirect') : 'main';

            $login = $this->ion_auth->login($username, $password, $remember);
            if ($login) {
                redirect(urldecode($redirect));
            } else {
                $this->_message = array(
                    'message' => 'Kesalahan Username atau Password',
                    'label' => 'danger'
                );
                $this->session->set_flashdata('message', $this->_message);
                redirect('security?redirect=' . urlencode($redirect));
            }
        } else {
            redirect('security');
        }
    }

    public function logout() {
        $this->session->unset_userdata('cart');
        $this->ion_auth->logout();
        redirect('security');
    }

    public function register(){
        $this->load->library('form_validation');
        $post = $this->input->post();
        if(isset($_FILES['cv_file']['name']) && isset($_FILES['photo_file']['name'])){
            $this->form_validation->set_rules('register_fullname', 'Nama Lengkap', 'required');
            $this->form_validation->set_rules('register_gender', 'Jenis Kelamin', 'required');
            $this->form_validation->set_rules('register_degree', 'Tingkat', 'required');
            $this->form_validation->set_rules('register_majors', 'Jurusan', 'required');
            $this->form_validation->set_rules('register_university', 'Universitas', 'required');
            $this->form_validation->set_rules('register_contact', 'No Handphone', 'required|numeric|is_unique[applicant.contact_number]');
            $this->form_validation->set_rules('register_email', 'Email', 'required|is_unique[applicant.email]');
            $this->form_validation->set_rules('register_vacancy', 'Lowongan', 'required');
            $this->form_validation->set_rules('register_birth_date', 'Tgl Lahir', 'required');
            $this->form_validation->set_rules('register_level', 'Level Keahlian', 'required');
            $this->form_validation->set_message('required', '{field} wajib diisi');
            $this->form_validation->set_message('is_unique', '{field} telah terdaftar');
            if($this->form_validation->run()){
                $cv_upload = $this->upload_cv();
                $pasphoto_upload = $this->upload_pasphoto();
                if($cv_upload['success']){
                    if($pasphoto_upload['success']){
                        $p = $this->input->post();
                        $password_show = uniqid($p['register_contact']);
                        $user = array(
                            'active' => 0,
                            'first_name' => NULL,
                            'last_name' => NULL,
                            'phone' => NULL,
                            'status' => NULL,
                            'avatar' => NULL,
                            'password_show' => $password_show
                        );

                        $user['id'] = $this->ion_auth->register($p['register_email'], $password_show, $p['register_email'], $user, array(2));
                        if ($user['id']) {
                            $biday = new DateTime($p['register_birth_date']);
                            $today = new DateTime();
                            $diff = $today->diff($biday);
                            $applicant['user_id'] = $user['id'];
                            $applicant['name'] = $p['register_fullname'];
                            $applicant['gender'] = $p['register_gender'];
                            $applicant['vacancy_division_id'] = $p['register_vacancy'];
                            $applicant['education_degree'] = $p['register_degree'];
                            $applicant['school_majors'] = $p['register_majors'];
                            $applicant['university'] = $p['register_university'];
                            $applicant['contact_number'] = $p['register_contact'];
                            $applicant['email'] = $p['register_email'];
                            $applicant['birth_date'] = $p['register_birth_date'];
                            $applicant['level'] = $p['register_level'];
                            $applicant['sc_twitter'] = $p['sc_twitter'];
                            $applicant['sc_facebook'] = $p['sc_facebook'];
                            $applicant['sc_instagram'] = $p['sc_instagram'];
                            $applicant['sc_portofolio'] = $p['sc_portofolio'];
                            $applicant['sc_linkedin'] = $p['sc_linkedin'];
                            $applicant['age'] = $diff->y;
                            $applicant['created_date'] = date('Y-m-d H:i:s');
                            $insert_applicant = $this->db->insert('applicant', $applicant);

                            $response['success'] = $insert_applicant ? TRUE : FALSE;
                            $response['msg'] = $insert_applicant ? '<b>Pendaftaran Berhasil</b> Tunggu administrator untuk mengaktifkan akses login Anda' : 'Registration Failed';

                            if($insert_applicant){
                                $applicant_id = $this->db->insert_id();
                                $objcv = array(
                                    'applicant_id' => $applicant_id,
                                    'applicant_file' => $cv_upload['data']['file_name'],
                                    'applicant_temp_file' => $_FILES['cv_file']['name'],
                                    'applicant_type_file' => 1
                                );
                                $this->db->insert('applicant_file', $objcv);
                                $objphoto = array(
                                    'applicant_id' => $applicant_id,
                                    'applicant_file' => $pasphoto_upload['data']['file_name'],
                                    'applicant_temp_file' => $_FILES['photo_file']['name'],
                                    'applicant_type_file' => 2
                                );
                                $this->db->insert('applicant_file', $objphoto);
                            }
                        }
                    }else{
                        $response['success'] = FALSE;
                        $response['msg'] = $pasphoto_upload['msg'];    
                    }
                }else{
                    $response['success'] = FALSE;
                    $response['msg'] = $cv_upload['msg'];
                }
            }else{
                $response['success'] = FALSE;
                $response['msg'] = validation_errors();
            }
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'CV & Pas Photo wajib diisi';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function upload_cv(){
        $conffile['upload_path'] = './files/pelamar_cv/';
        $conffile['allowed_types'] = 'doc|docx|pdf';
        $conffile['max_size'] = 0;
        $conffile['file_name'] = uniqid(date("hisu"));
        $this->load->library('upload', $conffile);
        $this->upload->initialize($conffile);
        if ($this->upload->do_upload('cv_file')) {
            $data = $this->upload->data();
            return array(
                'success' => TRUE,
                'data' => $data
            );
        }else{
            return array(
                'success' => FALSE,
                'msg' => $this->upload->display_errors()
            );
        }
    }

    public function upload_pasphoto(){
        $confphoto['upload_path'] = './files/pelamar_pasphoto/';
        $confphoto['allowed_types'] = 'jpg|jpeg|png|gif';
        $confphoto['max_size'] = 0;
        $confphoto['file_name'] = uniqid(date("hisu"));
        $this->load->library('upload', $confphoto);
        $this->upload->initialize($confphoto);
        if ($this->upload->do_upload('photo_file')) {
            $data = $this->upload->data();
            return array(
                'success' => TRUE,
                'data' => $data
            );
        }else{
            return array(
                'success' => FALSE,
                'msg' => $this->upload->display_errors()
            );
        }
    }

   public function _register(){        
      $params = $this->input->post();
      if($this->validation_form->register($params)){
         if($_FILES) {
            $config['upload_path'] = './files/cv/';
            $config['allowed_types'] = 'doc|docx|pdf';
            $config['max_size'] = 0;
            $config['file_name'] = uniqid(date("hisu"));

            $this->load->library('upload', $config);
            if ($this->upload->do_upload('cv_file')) {
               $p = $this->input->post();
               $password_show = uniqid($p['register_contact']);
               $user = array(
                  'active' => 0,
                  'first_name' => NULL,
                  'last_name' => NULL,
                  'phone' => NULL,
                  'status' => NULL,
                  'avatar' => NULL,
                  'password_show' => $password_show
               );

               $user['id'] = $this->ion_auth->register($p['register_email'], $password_show, $p['register_email'], $user, array(2));
               if ($user['id']) {
                  $biday = new DateTime($p['register_birth_date']);
                  $today = new DateTime();
                  $diff = $today->diff($biday);

                  $applicant['user_id'] = $user['id'];
                  $applicant['name'] = $p['register_fullname'];
                  $applicant['gender'] = $p['register_gender'];
                  $applicant['vacancy_division_id'] = $p['register_vacancy'];
                  $applicant['education_degree'] = $p['register_degree'];
                  $applicant['school_majors'] = $p['register_majors'];
                  $applicant['university'] = $p['register_university'];
                  $applicant['contact_number'] = $p['register_contact'];
                  $applicant['email'] = $p['register_email'];
                  $applicant['birth_date'] = $p['register_birth_date'];
                  $applicant['level'] = $p['register_level'];
                  $applicant['sc_twitter'] = $p['sc_twitter'];
                  $applicant['sc_facebook'] = $p['sc_facebook'];
                  $applicant['sc_instagram'] = $p['sc_instagram'];
                  $applicant['sc_portofolio'] = $p['sc_portofolio'];
                  $applicant['sc_linkedin'] = $p['sc_linkedin'];
                  $applicant['age'] = $diff->y;
                  $applicant['created_date'] = date('Y-m-d H:i:s');
                  $insert_applicant = $this->db->insert('applicant', $applicant);

                  $response['success'] = $insert_applicant ? TRUE : FALSE;
                  $response['msg'] = $insert_applicant ? '<b>Registration Successfully</b> Wait for the administrator to enable your login access' : 'Registration Failed';

                  if($insert_applicant){
                     $applicant_id = $this->db->insert_id();
                     $this->load->model('applicant'); 
                     $data = $this->upload->data();
                     $this->applicant->upload_applicant_file($applicant_id, $data['file_name'], $_FILES['cv_file']['name'], 1);
                  }
               }
            }else {
               $response['success'] = FALSE;
               $response['msg'] = $this->upload->display_errors();  
            }
         }else{
            $response['success'] = FALSE;
            $response['msg'] = 'File CV Required';
         }
            
      }else{
         $response['success'] = FALSE;
         $response['msg'] = validation_errors();
      }
      $response['csrf'] = $this->security->get_csrf_hash();
      $this->json_result($response);
   }

   // public function register(){        
   //    if($this->validation_form->register($this->input->post())){
   //       $p = $this->input->post();
   //       $password_show = uniqid($p['register_contact']);
   //       $user = array(
   //          'active' => 0,
   //          'first_name' => NULL,
   //          'last_name' => NULL,
   //          'phone' => NULL,
   //          'status' => NULL,
   //          'avatar' => NULL,
   //          'password_show' => $password_show
   //       );

   //       $user['id'] = $this->ion_auth->register($p['register_email'], $password_show, $p['register_email'], $user, array(2));
   //       if ($user['id']) {
   //          $biday = new DateTime($p['register_birth_date']);
   //          $today = new DateTime();
   //          $diff = $today->diff($biday);

   //          $applicant['user_id'] = $user['id'];
   //          $applicant['name'] = $p['register_fullname'];
   //          $applicant['gender'] = $p['register_gender'];
   //          $applicant['vacancy_division_id'] = $p['register_vacancy'];
   //          $applicant['education_degree'] = $p['register_degree'];
   //          $applicant['school_majors'] = $p['register_majors'];
   //          $applicant['university'] = $p['register_university'];
   //          $applicant['contact_number'] = $p['register_contact'];
   //          $applicant['email'] = $p['register_email'];
   //          $applicant['birth_date'] = $p['register_birth_date'];
   //          $applicant['level'] = $p['register_level'];
   //          $applicant['age'] = $diff->y;
   //          $applicant['created_date'] = date('Y-m-d H:i:s');
   //          $insert_applicant = $this->db->insert('applicant', $applicant);

   //          $response['success'] = $insert_applicant ? TRUE : FALSE;
   //          $response['msg'] = $insert_applicant ? '<b>Registration Successfully</b> Wait for the administrator to enable your login access' : 'Registration Failed';
   //       }  
   //    }else{
   //       $response['success'] = FALSE;
   //       $response['msg'] = validation_errors();
   //    }
   //    $response['csrf'] = $this->security->get_csrf_hash();
   //    $this->json_result($response);
   // }

    public function check_username(){      
        $p = $this->input->post();
        $user = $this->db->select('active, password_show')->where('email', $p['email_check'])->get('users');
        if($user->num_rows() > 0){
            $response['userdata'] = $user->row_array();
        }else{
            $response['userdata'] = FALSE;
        }
        $response['csrf'] = $this->security->get_csrf_hash();
        $this->json_result($response);
    }
}