<?php

class Main extends MY_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('dashboard');
        if (!$this->ion_auth->logged_in() && php_sapi_name() != 'cli') {
            redirect('security');
        }
    }

    public function index(){
        $role_id = $this->db->select('group_id')->where('user_id', $this->_user->id)->get('users_groups')->row_array();
        if($this->_role_id['group_id'] == 1){
        	$data = array(
                'title' => 'Dashboard',
                'content' => 'themes/pages/admin/page/dashboard',
                'vacancy' => $this->dashboard->summary_lowongan(),
                'tingkat' => $this->dashboard->summary_tingkat(),
                'kelamin' => $this->dashboard->summary_kelamin(),
                'pelamar' => $this->db->count_all_results('applicant'),
                'active' => $this->db->where('active', 1)->count_all_results('users'),
                'inactive' => $this->db->where('active', 0)->count_all_results('users'),
        	);
            $this->load->view('themes/pages/admin/index', $data);
        }else if($this->_role_id['group_id'] == 2){

            $pelamar = $this->profilPelamar($this->_user->id);
            $vacancy = $this->getVacancy($this->_user->id);
            $test_list = $this->getTestList($vacancy['id'], $vacancy['vacancy_division_id']);

            $data = array(
                'title' => 'Dashboard',
                'pelamar' => $pelamar,
                'vacancy' => $vacancy,
                'test_list' => $test_list,
                'content' => 'themes/pages/pelamar/page/dashboard',
            );
            $this->load->view('themes/pages/pelamar/index', $data);
        }
    }

    public function registration_test(){
        $p = $this->input->post();
        $rules_vacancy = $this->dashboard->rules_vacancy($p['id'], $this->_user->id);
        $rules_transaction = $this->dashboard->rules_transaction($p['id'], $this->_user->id);
        if($rules_vacancy && $rules_transaction){
            $register_tests = $this->dashboard->registration_test($p['id'], $this->_user->id, date('Y-m-d H:i:s'));
            $response['time_now_js'] = $p['time_now_js'];
            $response['success'] = $register_tests ? TRUE : FALSE;
            $response['msg'] = $register_tests ? '' : 'Transaction Failed';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'You dont have permission';
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function profilPelamar($user_id){
        $this->db->select('a.*, b.name as vacancy_division_name');
        $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id');
        $this->db->where('a.user_id', $user_id);
        return $this->db->get('applicant as a')->row_array();
    }

    public function getVacancy($user_id){
        $this->db->select('vacancy_division_id, id');
        $this->db->where('user_id', $user_id);
        return $this->db->get('applicant')->row_array();
    }

    public function getTestList($vacancy_id, $vacancy_division_id){
        $this->db->select('a.rules, b.*,c.*');
        $this->db->join('test_type as b', 'a.test_type_id = b.id', 'left');
        $this->db->join('(
                        select status as status_register, test_type_id 
                        from test_transaction where applicant_id = '.$vacancy_id.'
                     ) as c', 'b.id = c.test_type_id', 'left');

        $this->db->where('a.vacancy_division_id', $vacancy_division_id);
        $this->db->where('a.status', 1);
        $this->db->where('a.random_test IS NULL');
        $this->db->order_by('a.sort', 'asc');
        return $this->db->get('test_groups as a');
    }

}