<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mg extends MY_Controller {
    
	public function __construct() {
		parent::__construct();
	}

	public function move_users(){
		$this->_db = $this->load->database('orig', TRUE);
		$count = 0;
		$count_groups = 0;
		$users = $this->db->where_not_in('id', array(8,514))->get('users')->result_array();
		// echo json_encode($users);
		// exit();
		foreach ($users as $v) {
			$tmp = array(
				'before_id' => $v['id'],
				'ip_address' => $v['ip_address'],
				'username' => $v['username'],
				'password' => $v['password'],
				'password_show' => $v['password_show'],
				'salt' => $v['salt'],
				'email' => $v['email'],
				'activation_code' => $v['activation_code'],
				'forgotten_password_code' => $v['forgotten_password_code'],
				'forgotten_password_time' => $v['forgotten_password_time'],
				'remember_code' => $v['remember_code'],
				'created_on' => $v['created_on'],
				'last_login' => $v['last_login'],
				'active' => $v['active'],
				'first_name' => $v['first_name'],
				'last_name' => $v['last_name'],
				'company' => $v['company'],
				'phone' => $v['phone'],
				'avatar' => $v['avatar']
			);
			$insert = $this->_db->insert('users', $tmp);
			$insert ? $count++ : FALSE;
			$insert_id = $this->_db->insert_id();

			$tmp_groups = array(
				'user_id' => $insert_id,
				'group_id' => 2
			);
			$insertgroups = $this->_db->insert('users_groups', $tmp_groups);
			$insertgroups ? $count_groups++ : FALSE;
		}
		echo $count.' Data users and '.$count_groups.' Data groups';
	}

	public function move_applicant(){
		$count = 0;
		$this->_db = $this->load->database('orig', TRUE);
		$applicant = $this->db->get('applicant')->result_array();
		foreach ($applicant as $v) {
			$users = $this->_db->where('before_id', $v['user_id'])->get('users')->row_array();
			$tmp = array(
				'before_id' => $v['id'],
				'user_id' => $users['id'],
				'name' => $v['name'],
				'birth_date' => $v['birth_date'],
				'age' => $v['age'],
				'vacancy_division_id' => $v['vacancy_division_id'],
				'gender' => $v['gender'],
				'education_degree' => $v['education_degree'],
				'school_majors' => $v['school_majors'],
				'university' => $v['university'],
				'contact_number' => $v['contact_number'],
				'email' => $v['email'],
				'created_date' => $v['created_date'],
				'status' => $v['status'],
				'level' => $v['level'],
			);
			$insert = $this->_db->insert('applicant', $tmp);
			$insert ? $count++ : FALSE;
		}
		echo $count.' Data applicant';
	}

	public function move_applicant_file(){
		$count = 0;
		$this->_db = $this->load->database('orig', TRUE);
		$result = $this->db->get('applicant_file')->result_array();
		foreach ($result as $v) {
			$applicant = $this->_db->where('before_id', $v['applicant_id'])->get('applican')->row_array();
			$tmp = array(
				'applicant_id' => $applicant['id'],
				'applicant_file' => $v['applicant_file'],
				'applicant_temp_file' => $v['applicant_temp_file'],
				'applicant_type_file' => $v['applicant_type_file'],
			);
			$insert = $this->_db->insert('applicant_file', $tmp);
			$insert ? $count++ : FALSE;
		}
		echo $count.' Data applicant file';
	}
}