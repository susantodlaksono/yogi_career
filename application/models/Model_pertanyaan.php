<?php

class Model_pertanyaan extends CI_Model {

	public function get_data(){
		$this->db->where('status', 1);
		$result = $this->db->get('test_type')->result_array();
		if($result){
			foreach ($result as $k => $v) {
				$data[$k] = $v;
				$data[$k]['total_pertanyaan'] = $this->hitung_total_pertanyaan($v['id']);
			}
			return $data;
		}else{
			return FALSE;
		}
	}

	public function hitung_total_pertanyaan($test_id){
		$this->db->where('test_type_id', $test_id);
		return $this->db->count_all_results('question');
	}

	public function insert_pertanyaan($post){
        $insert_question = array(
            'question_text' => $post['description'] != '' ? utf8_encode($post['description']) : NULL,
            'test_type_id' => $post['type_test'],
            'question_type_id' => $post['type_answers']
        );
        $this->db->insert('question', $insert_question);
    }

    public function update_pertanyaan($post){
        $insert_question = array(
            'question_text' => $post['description'] != '' ? utf8_encode($post['description']) : NULL,
            'test_type_id' => $post['type_test']
        );
        $this->db->update('question', $insert_question, array('id' => $post['id']));
    }

    public function update_file_pertanyaan($file_name, $temp_name, $file_id){
        $obj = array(
            'file' => $file_name,
            'temp_name' => $temp_name
        );
        return $this->db->update('question_file', $obj, array('id' => $file_id));
    }

    public function update_gambar_pertanyaan($file_name, $temp_name, $file_id){
        $obj = array(
            'image' => $file_name,
            'temp_name' => $temp_name
        );
        return $this->db->update('question_image', $obj, array('id' => $file_id));
    }

    public function insert_gambar_pertanyaan($file_name, $temp_name, $question_id){
        $obj = array(
            'image' => $file_name,
            'temp_name' => $temp_name,
            'question_id' => $question_id,
        );
        return $this->db->insert('question_image', $obj);
    }

     public function insert_file_pertanyaan($file_name, $temp_name, $question_id){
        $obj = array(
            'file' => $file_name,
            'temp_name' => $temp_name,
            'question_id' => $question_id,
        );
        return $this->db->insert('question_file', $obj);
    }

    public function insert_jawaban_pertanyaan($question_id, $answers_sort, $answers){
        $insert_answers = array(
            'question_id' => $question_id,
            'question_answers' => $answers['result'],
            'correct_answers' => $answers['correct'] ? $answers['correct'] : NULL
        );
        return $this->db->insert('question_answers', $insert_answers);
    }

    public function insert_jawaban_multiple_benar($question_id, $answers_correct){
        $correct_answers_id = $this->id_jawaban_benar_multiple($question_id, $answers_correct);
        $q = array(
            'correct_answers_multiple' => json_encode($correct_answers_id),
        );
        return $this->db->update('question', $q, array('id' => $question_id));
    }

    public function insert_jawaban_benar($question_id, $answers_sort, $type = '_by_sort'){
        $correct_answers_id = $this->id_jawaban_benar($question_id, $answers_sort, $type);
        $q = array(
            'correct_answers_id' => $correct_answers_id['id'],
        );
        return $this->db->update('question', $q, array('id' => $question_id));
    }

    public function id_jawaban_benar_multiple($question_id, $answers_correct){
        $this->db->select('id');
        $this->db->where('question_id', $question_id);
        $this->db->where_in('answers_sort', $answers_correct);
        $rs  = $this->db->get('question_answers')->result_array();
        if($rs){
            foreach ($rs as $v) {
                $data[] = $v['id'];
            }
            return $data;
        }
    }

    public function id_jawaban_benar($question_id, $answers_sort, $type){
        $this->db->select('id');
        $this->db->where('question_id', $question_id);
        if($type == '_by_sort'){
            $this->db->where('answers_sort', $answers_sort);
        }else{
            $this->db->where('id', $answers_sort);
        }
        return $this->db->get('question_answers')->row_array();
    }

    public function tipe_pertanyaan($testid){
    	$this->db->select('question_type_id');
    	$this->db->where('test_type_id', $testid);
    	$this->db->group_by('question_type_id');
    	$this->db->order_by('question_type_id', 'asc');
    	return $this->db->get('question')->result_array();
    }

    public function get_pertanyaan($testtypeid){
    	$this->db->select('a.*, d.question_answers');
    	$this->db->select('b.file, b.temp_name as temp_name_file');
    	$this->db->select('c.image, c.temp_name as temp_name_image');
    	$this->db->join('question_file as b', 'a.id = b.question_id', 'left');
    	$this->db->join('question_image as c', 'a.id = c.question_id', 'left');
    	$this->db->join('question_answers as d', 'a.correct_answers_id = d.id', 'left');
    	$this->db->where('a.test_type_id', $testtypeid);
    	$result = $this->db->get('question as a')->result_array();
    	if($result){
    		foreach ($result as $key => $value) {
    			$data[$key] = $value;
    			if($value['question_type_id'] == 1){
    				$data[$key]['jawaban_benar'] = $this->jawaban_benar_single($value['id']);
    			}else if($value['question_type_id'] == 4){
					$data[$key]['jawaban_benar'] = $this->jawaban_benar_multi($value['id']);
    			}else{
    				$data[$key]['jawaban_benar'] = NULL;
    			}
    		}
    		return $data;
    	}else{
    		return FALSE;
    	}
    }

    public function detail_pertanyaan($id){
    	$this->db->select('a.*');
    	$this->db->select('b.id as file_id, b.file, b.temp_name as temp_name_file');
    	$this->db->select('c.id as image_id, c.image, c.temp_name as temp_name_image');
    	$this->db->join('question_file as b', 'a.id = b.question_id', 'left');
    	$this->db->join('question_image as c', 'a.id = c.question_id', 'left');
    	$this->db->where('a.id', $id);
    	return $this->db->get('question as a')->row_array();
    }

    public function get_pertanyaan_multiple_jawaban($testtypeid){
    	$this->db->select('a.*');
    	$this->db->select('b.file, b.temp_name as temp_name_file');
    	$this->db->select('c.image, c.temp_name as temp_name_image');
    	$this->db->join('question_file as b', 'a.id = b.question_id', 'left');
    	$this->db->join('question_image as c', 'a.id = c.question_id', 'left');
    	$this->db->where('a.test_type_id', $testtypeid);
    	$this->db->where('a.question_type_id', 4);
    	$result = $this->db->get('question as a')->result_array();
    	if($result){
    		foreach ($result as $key => $value) {
    			$data[$key] = $value;
    			$data[$key]['jawaban_benar'] = $this->jawaban_benar_multi($value['id']);
    		}
    		return $data;
    	}else{
    		return FALSE;
    	}
    }

    public function jawaban_benar_multi($qid){
    	$this->db->where('question_id', $qid);
    	$this->db->where('correct_answers', 1);
    	$result = $this->db->get('question_answers')->result_array();
    	if($result){
    		foreach ($result as $key => $value) {
    			$data[] = $value['question_answers'];
    		}
    		$implode = implode(',', $data);
    		return $implode;
    	}else{
    		return FALSE;
    	}
    }

    public function jawaban_benar_single($qid){
    	$this->db->where('question_id', $qid);
    	$this->db->where('correct_answers', 1);
    	$result = $this->db->get('question_answers')->row_array();
    	if($result){
    		return $result['question_answers'];
    	}else{
    		return FALSE;
    	}
    }
}