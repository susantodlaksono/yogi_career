<?php

class Model_pelamar extends CI_Model {

	public function getdata(){
		$this->db->select('a.*, b.*, a.id as applicant_id');
		$this->db->select('c.name as vacancy_name');
		$this->db->join('users as b', 'a.user_id = b.id', 'left');
		$this->db->join('vacancy_division as c', 'a.vacancy_division_id = c.id', 'left');
		$result = $this->db->get('applicant as a')->result_array();
		foreach ($result as $k => $v) {
			$data[$k] = $v;
			$data[$k]['cv'] = $this->get_employee_doc($v['applicant_id'], 1);
			$data[$k]['pas_photo'] = $this->get_employee_doc($v['applicant_id'], 2);
		}
		return $data;
	}

	public function get_employee_doc($id, $type){
      	$this->db->where('applicant_id', $id);
      	$this->db->where('applicant_type_file', $type);
      	return $this->db->get('applicant_file')->row_array();
   }
}