<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 *
 * @author SUSANTO DWI LAKSONO
 */

class Email_template{

	public function __construct() {
 	 	$this->ci = & get_instance();
 	}

 	public function approval_account($applicant_id){
 		$applicant = $this->ci->db->where('id', $applicant_id)->get('applicant')->row_array();
 		$password = $this->ci->db->select('password_show')->where('id', $applicant['user_id'])->get('users')->row_array();

 		$t = '';
 		$t .= '<h3 style="font-family:Calibri;">Dear '.$applicant['name'].'</h3>';
 		$t .= '<p style="font-family:Calibri;font-size:15px;">';
 			$t .= 'Thank you for registering as an employee candidate at PT ebdesk Teknologi. <br>';
 			$t .= 'Your account has been <span style="padding:2px; background-color: #2cd82f; color:#FFF; font-family:Calibri">verified</span> by the administrator and you can log in directly using an account :</p>';
 		$t .= '</p>';
 		$t .= '<p style="font-size:15px;font-family:Calibri;">';
 			$t .= '<b>Email : <i>'.$applicant['email'].'</i></b><br>';
 			$t .= '<b>Password : <i>'.$password['password_show'].'</i></b>';
 		$t .= '</p>';
 		$t .= '<center><a href="http://career.ebdesk.com/career" target="_blank" style="text-decoration:none; padding:10px; background-color: #f11818; color:#FFF; font-family:Calibri">GO TO CAREER</a></center>';
 		$t .= '<br>';
 		$t .= '<hr>';
 		$t .= '<p style="font-family:Calibri;text-align:center; color:#777"><small><em>This email is generated and sent automatically using EB CAREER . Copyright © eBdesk Teknologi 2018 </em></small></p>';
 		// $t .= '<p style="font-size:15px;font-family:Calibri;"></p>';
 		return $t;
	}

}