<style type="text/css">
    .btn-danger{
        background-color: #f11818;
    }
    .text-danger{
        color: #f11818 !important;
    }
    .bg-danger{
        background-color: #f11818;
    }    
</style>

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
    <div class="row" style="margin-top: 10px;">
        <div class="col-md-5">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-transparent" style="margin-bottom: 0;">
                        <div class="panel-heading">
                            <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-user"></i> Profil Anda</div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-condensed">
                                <tbody>
                                    <tr>
                                        <td width="170" class="bold">Nama</td>
                                        <td ><?php echo $applicant['name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">Email</td>
                                        <td><?php echo $applicant['email'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">No. Handphone</td>
                                        <td><?php echo $applicant['contact_number'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="190" class="bold">Tingkat Pendidikan</td>
                                        <!-- <td><?php echo $applicant['education_degree'] ?></td> -->
                                        <?php
                                        if($education){
                                            switch ($education['degree']) {
                                                case '1': echo '<td>SD</td>'; break;
                                                case '2': echo '<td>SMP</td>'; break;
                                                case '3': echo '<td>SMA/SMK</td>'; break;
                                                case '4': echo '<td>Akad./univ</td>'; break;
                                                case '5': echo '<td>Pasca Sarjana</td>'; break;
                                            }
                                        }else{
                                            echo '<td></td>';
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">Universitas/Sekolah</td>
                                        <!-- <td><?php echo $applicant['university'] ?></td> -->
                                        <?php
                                        if($education){
                                            echo '<td>'.($education['school_name'] ? $education['school_name'] : '').'</td>';
                                        }else{
                                            echo '<td></td>';
                                        }
                                        ?>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">Bidang</td>
                                        <!-- <td><?php echo $applicant['school_majors'] ?></td> -->
                                        <?php
                                        if($education){
                                            echo '<td>'.($education['major'] ? $education['major'] : '').'</td>';
                                        }else{
                                            echo '<td></td>';
                                        }
                                        ?>
                                    </tr>
                                    <!-- <tr>
                                        <td width="150" class="bold">Birth Date</td>
                                        <td><?php echo $applicant['birth_date'] ? date('d M Y', strtotime($applicant['birth_date'])) : '' ?></td>
                                    </tr> -->
                                    <tr>
                                        <td width="150" class="bold">Biodata</td>
                                        <td>
                                            <a href="http://career.regis/print/<?php echo $applicant['num_register'] ?>" target="_blank"><i class="fa fa-file"></i> Preview</a><br>
                                            <a href="http://career.regis" target="_blank"><i class="fa fa-edit"></i> perbaharui</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold">Lowongan</td>
                                        <td style=""><?php echo $applicant['vacancy_division_name'] ?></td>
                                    </tr>
                                    <tr>
                                        <td width="150" class="bold" style="">Level</td>
                                        <td style=""><?php echo $applicant['level'] == 1 ? 'Fresh Graduate' : 'Expert' ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <!-- <div class="panel panel-transparent" style="margin-bottom: 0">
                        <div class="panel-heading">
                            <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-file"></i> Upload Registration Form</div>
                        </div>
                        <div class="panel-body">
                           <div class="row" style="display: none;">
                             <?php
                                 $cv = $this->db->where('applicant_id', $applicant['id'])->where('applicant_type_file', 2)->get('applicant_file');
                                 if($cv->num_rows() > 0){
                                     $cv_name = $cv->row_array();
                                     $mode = $cv_name['applicant_file'];
                                 }else{
                                     $mode = '';
                                 }
                             ?>
                             <form id="form-upload-cv" enctype="multipart/form-data">
                                 <div class="col-md-9">
                                     <input type="hidden" name="file_before" value="<?php echo $mode ?>">
                                     <input type="file" name="cv_file" class="form-control input-sm">
                                     <p class="text-muted"><i>Only .docx .doc .pdf</i></p>
                                 </div>
                                 <div class="col-md-3 no-padding sm-m-t-10 sm-text-center">
                                     <button type="submit" class="btn btn-danger"><i class="fa fa-upload"></i> Upload</button>
                                 </div>
                             </form> 
                           </div>
                           <div class="row" style="display: none;">
                              <div class="col-md-12 result-cv">
                                 <?php if($mode != '') { ?>
                                    <h6 class="text-center bold" style="font-size:13px;">Your CV :</h6> <h6 class="text-center font-arial" style="font-size:13px;"><a href="<?php echo site_url('dashboard/widget/download/'.$cv_name['applicant_file'].'') ?>"><?php echo $cv_name['applicant_temp_file'] ?></a></h6>
                                 <?php } ?>
                              </div>
                           </div>
                           <div class="row">
                             <?php
                                 $cv = $this->db->where('applicant_id', $applicant['id'])->where('applicant_type_file', 2)->get('applicant_file');
                                 if($cv->num_rows() > 0){
                                     $cv_name = $cv->row_array();
                                     $mode_reg = $cv_name['applicant_file'];
                                 }else{
                                     $mode_reg = '';
                                 }
                             ?>
                             <form id="form-upload-registration" enctype="multipart/form-data">
                                 <div class="col-md-9">
                                     <input type="hidden" name="file_before" value="<?php echo $mode_reg ?>">
                                     <input type="file" name="reg_file" class="form-control input-sm">
                                     <p class="text-muted"><i>Only .docx .doc .pdf</i></p>
                                 </div>
                                 <div class="col-md-3 no-padding sm-m-t-10 sm-text-center">
                                     <button type="submit" class="btn btn-danger"><i class="fa fa-upload"></i> Upload</button>
                                 </div>
                             </form> 
                           </div>
                           <div class="row">
                              <div class="col-md-12 result-registration">
                                 <?php if($mode_reg != '') { ?>
                                    <h6 class="text-center bold" style="font-size:13px;">Your Registration Form :</h6> <h6 class="text-center font-arial" style="font-size:13px;"><a href="<?php echo site_url('dashboard/widget/download_reg/'.$cv_name['applicant_file'].'') ?>"><?php echo $cv_name['applicant_temp_file'] ?></a></h6>
                                 <?php } ?>
                              </div>
                           </div>
                        </div>
                    </div> -->
                    <div class="panel panel-transparent" style="margin-bottom: 0">
                        <div class="panel-heading">
                            <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-file"></i> Portofolio</div>
                        </div>
                        <div class="panel-body">
                           <div class="row" style="display: none;">
                             <?php
                                 $cv = $this->db->where('applicant_id', $applicant['id'])->where('applicant_type_file', 2)->get('applicant_file');
                                 if($cv->num_rows() > 0){
                                     $cv_name = $cv->row_array();
                                     $mode = $cv_name['applicant_file'];
                                 }else{
                                     $mode = '';
                                 }
                             ?>
                             <form id="form-upload-cv" enctype="multipart/form-data">
                                 <div class="col-md-9">
                                     <input type="hidden" name="file_before" value="<?php echo $mode ?>">
                                     <input type="file" name="cv_file" class="form-control input-sm">
                                     <p class="text-muted"><i>Only .docx .doc .pdf</i></p>
                                 </div>
                                 <div class="col-md-3 no-padding sm-m-t-10 sm-text-center">
                                     <button type="submit" class="btn btn-danger"><i class="fa fa-upload"></i> Upload</button>
                                 </div>
                             </form> 
                           </div>
                           <div class="row" style="display: none;">
                              <div class="col-md-12 result-cv">
                                 <?php if($mode != '') { ?>
                                    <h6 class="text-center bold" style="font-size:13px;">Your CV :</h6> <h6 class="text-center font-arial" style="font-size:13px;"><a href="<?php echo site_url('dashboard/widget/download/'.$cv_name['applicant_file'].'') ?>"><?php echo $cv_name['applicant_temp_file'] ?></a></h6>
                                 <?php } ?>
                              </div>
                           </div>
                           <div class="row">
                             <?php
                                 $cv = $this->db->where('applicant_id', $applicant['id'])->where('applicant_type_file', 2)->get('applicant_file');
                                 if($cv->num_rows() > 0){
                                     $cv_name = $cv->row_array();
                                     $mode_reg = $cv_name['applicant_file'];
                                 }else{
                                     $mode_reg = '';
                                 }
                             ?>
                             <form id="form-upload-portofolio" enctype="multipart/form-data">
                                 <div class="col-md-9">
                                     <input type="hidden" name="portofolio_file_before" value="<?php echo $mode_reg ?>">
                                     <input type="file" name="portofolio_file" class="form-control input-sm">
                                     <p class="text-muted"><i>Berupa .docx .doc .pdf</i></p>
                                 </div>
                                 <div class="col-md-3 no-padding sm-m-t-10 sm-text-center">
                                     <button type="submit" class="btn btn-danger"><i class="fa fa-upload"></i> Upload</button>
                                 </div>
                             </form> 
                             <form id="form-portofolio">
                                 <div class="col-md-9">
                                     <input type="text" name="portofolio_link" class="form-control input-sm">
                                     <p class="text-muted"><i>Berupa tautan disini</i></p>
                                 </div>
                                 <div class="col-md-3 no-padding sm-m-t-10 sm-text-center">
                                     <button type="submit" class="btn btn-danger"><i class="fa fa-upload"></i> Simpan</button>
                                 </div>
                             </form> 
                           </div>
                           <div class="row">
                              <div class="col-md-12 result-registration">
                                 <?php if($mode_reg != '') { ?>
                                    <h6 class="text-center bold" style="font-size:13px;">Your Registration Form :</h6> <h6 class="text-center font-arial" style="font-size:13px;"><a href="<?php echo site_url('dashboard/widget/download_reg/'.$cv_name['applicant_file'].'') ?>"><?php echo $cv_name['applicant_temp_file'] ?></a></h6>
                                 <?php } ?>
                              </div>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-7">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-transparent" style="margin-bottom: 0">
                        <div class="panel-heading">
                            <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-edit"></i> Daftar Test</div>
                        </div>
                        <div class="panel-body">
                           <div class="row">
                              <div class="col-md-12">
                                 <button class="btn btn-success btn-block" data-target="#modal-instructions" data-toggle="modal"><i class="fa fa-info"></i> Instruksi Tes</button>
                              </div>
                           </div><br>
                           <?php
                            // $test_list = $this->getTestList($vacancy['id'], $vacancy['vacancy_division_id'], $random_question_test_id);
                            // $vacancy = $this->db->select('vacancy_division_id, id')->where('user_id', $user->id)->get('applicant')->row_array();
                            // $test_list = $this->db->select('a.rules, b.*,c.*')
                            //             ->join('test_type as b', 'a.test_type_id = b.id', 'left')
                            //             ->join('(select status as status_register, test_type_id from test_transaction where applicant_id = '.$vacancy['id'].') as c', 'b.id = c.test_type_id', 'left')
                            //             ->where('a.vacancy_division_id', $vacancy['vacancy_division_id'])
                            //             ->where('a.status', 1)
                            //             ->order_by('a.sort', 'asc')
                            //             ->get('test_groups as a');

                            if($test_list->num_rows() > 0){
                                foreach ($test_list->result_array() as $key => $value) {
                                    $total_question = $this->db->where('test_type_id', $value['id'])->count_all_results('question');
                                    if($value['rules'] != 'opened'){
                                        $check = $this->db->where('applicant_id', $vacancy['id'])
                                                              ->where('test_type_id', $value['rules'])
                                                              ->where('status', 2)
                                                              ->get('test_transaction')->num_rows();
                                    }else{
                                        $check = 1;
                                    }
                                    

                                    echo '<div class="card share full-width">';
                                        if($value['status_register']){
                                            if($value['status_register'] == 1){
                                                if($check > 0){
                                                    echo '<button class="btn btn-danger btn-sm pull-right apply-test" style="margin-top:13px;margin-right:10px;" data-id="'.$value['id'].'">Continue</button>';
                                                }
                                            }else{
                                                if($check > 0){
                                                    echo '<label class="label label-success pull-right" style="font-size:20px;margin-top:15px;margin-right:10px;"><i class="fa fa-check"></i></label>';
                                                }
                                            }
                                        }else{
                                            if($check > 0){
                                                echo '<button class="btn btn-danger btn-sm pull-right apply-test" style="margin-top:13px;margin-right:10px;" data-id="'.$value['id'].'">Apply</button>';
                                            }
                                        }
                                        echo '<div class="card-header clearfix" style="cursor: default;">';
                                            // echo '<div class="user-pic">';
                                            //     if($value['type'] == 'Basic'){
                                            //         echo '<img width="150" height="150" src="'.base_url().'/assets/img/basic.png">';
                                            //     }else{
                                            //         echo '<img width="150" height="150" src="'.base_url().'/assets/img/teknis.png">';
                                            //     }
                                            // echo '</div>';
                                            echo '<h5>'.$value['name'].'</h5>';
                                            echo '<h6>'.$total_question.' pertanyaan dalam '.$value['time'].' menit</h6>';
                                        echo '</div>';
                                        echo '<div class="card-description">';
                                            echo ''.$value['guide_tests'].'';
                                        echo '</div>';
                                    echo '</div>';
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
</div>

<div class="modal fade fill-in" id="modal-instructions" tabindex="-1" role="dialog" aria-hidden="true">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close"></i></button>
  <div class="modal-dialog" style="">
      <div class="modal-content">
            <div class="modal-header">
            <h5 class="text-left"><span class="bold">Instruksi Tes</span></h5>
            </div>
            <div class="modal-body" style="padding:2px;">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <h5 class="font-arial" style="font-size: 16px;">
                        <ul style="">
                        <li style="padding-bottom: 10px;">Anda harus memastikan internet cukup baik dan tidak membuka banyak file yang berat yang dapat menghambat berjalannya tes</li>
                        <li style="padding-bottom: 10px;">Anda dipersilahkan untuk menyiapkan kertas kosong dan alat tulis karena di salah satu tes memerlukannya untuk mencorat-coret</li>
                        <li style="padding-bottom: 10px;">setiap tes memiliki waktu pengerjaan dan petunjuknya masing-masing</li>
                        </ul>
                     </h5>
                  </div>
               </div>
         </div>
      </div>
  </div>
</div>


<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';


</script>

<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>