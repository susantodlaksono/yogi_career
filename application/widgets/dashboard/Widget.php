	<?php

class Widget extends Widgets {

   public function __construct() {
      parent::__construct();
      date_default_timezone_set('Asia/Jakarta');
      $this->load->library('validation_form'); 
      $this->load->model('tests'); 
      $this->load->model('applicant'); 
   }

   public function index() {
      $applicant = $this->getApplicant($this->_user->id);
      $random_question_test_id = $this->getRandomQuestion($applicant['id'], $applicant['vacancy_division_id']);
      $vacancy = $this->getVacancy($this->_user->id);
      $education = $this->getEducation($applicant['id']);
      $data = array(
         'title' => 'Dashboard',
         'user' => $this->_user,
         'applicant' => $applicant,
         'education' => $education,
         'random_question_test_id' => $random_question_test_id, 
         'vacancy' => $vacancy, 
         'test_list' => $this->getTestList($vacancy['id'], $vacancy['vacancy_division_id'], $random_question_test_id)
      );
      $this->render_widget($data);
   }

   public function getVacancy($user_id){
      $this->db->select('vacancy_division_id, id');
      $this->db->where('user_id', $user_id);
      return $this->db->get('applicant')->row_array();
   }

   public function getRandomQuestion($applicant_id, $vacancy_division_id){
      $test_groups_random = $this->testGroupsRandom($vacancy_division_id);
      if($test_groups_random){
         $test_transaction = $this->testTransaction($applicant_id, $test_groups_random);
         if($test_transaction){
            $result['with_test_type_id'] = $test_transaction;
            $result['except_test_type_id'] = $this->exceptTestType($test_transaction, $vacancy_division_id);
         }else{
            $with = $this->withTestType($applicant_id, $vacancy_division_id);
            $result['with_test_type_id'] = $with;
            $result['except_test_type_id'] = $this->exceptTestType($with, $vacancy_division_id);
         }
         return $result;
      }else{
         return FALSE;
      }
   }

   public function withTestType($applicant_id, $vacancy_division_id){
      $this->db->select('b.random_code, b.test_type_id');
      $this->db->join('(SELECT * FROM test_groups_random ORDER BY RAND()) as b', 'a.random_code = b.random_code', 'left');
      $this->db->where('a.vacancy_division_id', $vacancy_division_id);
      $this->db->group_by('b.random_code');
      $this->db->order_by('a.random_code');
      $rs = $this->db->get('test_groups_random as a')->result_array();
      if($rs){
         foreach ($rs as $v) {
            $result[] = $v['test_type_id'];
         }
         return $result;
      }else{
         return FALSE;
      }
   }

   public function exceptTestType($test_type_id, $vacancy_division_id){
      $this->db->where('vacancy_division_id', $vacancy_division_id);
      $this->db->where_not_in('test_type_id', $test_type_id);
      $rs = $this->db->get('test_groups_random')->result_array();
      if($rs){
         foreach ($rs as $v) {
            $result[] = $v['test_type_id'];
         }
         return $result;
      }else{
         return FALSE;
      }
   }

   public function testGroupsRandom($vacancy_division_id){
      $this->db->where('vacancy_division_id', $vacancy_division_id);
      $rs = $this->db->get('test_groups_random')->result_array();
      if($rs){
         foreach ($rs as $v) {
            $result[] = $v['test_type_id'];
         }
         return $result;
      }else{
         return FALSE;
      }
   }

   public function testTransaction($applicant_id, $test_type_id = array()){
      $this->db->where('applicant_id', $applicant_id);
      $this->db->where_in('test_type_id', $test_type_id);
      $rs = $this->db->get('test_transaction')->row_array();
      if($rs){
         $result[] = $rs['test_type_id'];
         return $result;
      }else{
         return FALSE;
      }
   }


   public function getApplicant($user_id){
      $this->db->select('a.*, b.name as vacancy_division_name');
      $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id');
      $this->db->where('a.user_id', $user_id);
      return $this->db->get('applicant as a')->row_array();
   }

   public function getTestList($vacancy_id, $vacancy_division_id, $random_question_test_id){
      $this->db->select('a.rules, b.*,c.*');
      $this->db->join('test_type as b', 'a.test_type_id = b.id', 'left');
      $this->db->join('(
                        select status as status_register, test_type_id 
                        from test_transaction where applicant_id = '.$vacancy_id.'
                     ) as c', 'b.id = c.test_type_id', 'left');

      $this->db->where('a.vacancy_division_id', $vacancy_division_id);
      $this->db->where('a.status', 1);
      $this->db->where('a.random_test IS NULL');
      if($random_question_test_id){
         $this->db->or_where_in('a.test_type_id', $random_question_test_id['with_test_type_id']);
         $this->db->where_not_in('a.test_type_id', $random_question_test_id['except_test_type_id']);
      }
      $this->db->order_by('a.sort', 'asc');
      return $this->db->get('test_groups as a');
   }

    public function registration_test(){
        $p = $this->input->post();
        $rules_vacancy = $this->tests->rules_vacancy($p['id'], $this->_user->id);
        $rules_transaction = $this->tests->rules_transaction($p['id'], $this->_user->id);
        if($rules_vacancy && $rules_transaction){
            $register_tests = $this->tests->registration_test($p['id'], $this->_user->id, date('Y-m-d H:i:s'));
            $response['time_now_js'] = $p['time_now_js'];
            $response['success'] = $register_tests ? TRUE : FALSE;
            $response['msg'] = $register_tests ? '' : 'Transaction Failed';
        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'You dont have permission';
        }
        $this->json_result($response);
    }



    public function upload_cv(){        
        $applicant = $this->db->select('a.id, a.name, b.name as vacancy_name')->join('vacancy_division as b', 'a.vacancy_division_id = b.id')->where('a.user_id', $this->_user->id)->get('applicant as a')->row_array();
        if($_FILES) {
            $config['upload_path'] = './files/cv/';
            $config['allowed_types'] = 'doc|docx|pdf';
            $config['max_size'] = 0;
            $config['file_name'] = uniqid(date("his").$applicant['id']);

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('cv_file')) {
                $response['success'] = FALSE;
                $response['msg'] = $this->upload->display_errors();                    
            }
            else {
                $data = $this->upload->data();
                if($this->input->post('file_before') != '') {
                    unlink('./files/cv/'.$this->input->post('file_before'));
                    $this->applicant->update_cv($applicant['id'], $data['file_name'], $_FILES['cv_file']['name']);
                }else{
                    $this->applicant->upload_cv($applicant['id'], $data['file_name'], $_FILES['cv_file']['name']);
                }
                $response['success'] = TRUE;
                $response['msg'] = 'CV has been uploaded';
                $response['file_name'] = $data['file_name'];
                $response['temp_name'] = $_FILES['cv_file']['name'];
            }

        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'File CV Required';
        }
        $this->json_result($response);
    }

    public function upload_registration(){        
        $applicant = $this->db->select('a.id')->where('a.user_id', $this->_user->id)->get('applicant as a')->row_array();
        if($_FILES) {
            $config['upload_path'] = './files/regform/';
            $config['allowed_types'] = 'doc|docx|pdf';
            $config['max_size'] = 0;
            $config['file_name'] = uniqid(date("hisu"));

            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('reg_file')) {
                $response['success'] = FALSE;
                $response['msg'] = $this->upload->display_errors();                    
            }
            else {
                $data = $this->upload->data();
                if($this->input->post('file_before') != '') {
                    unlink('./files/regform/'.$this->input->post('file_before'));
                    $this->applicant->update_applicant_file($applicant['id'], $data['file_name'], $_FILES['reg_file']['name'], 2);
                }else{
                    $this->applicant->upload_applicant_file($applicant['id'], $data['file_name'], $_FILES['reg_file']['name'], 2);
                }
                $response['success'] = TRUE;
                $response['msg'] = 'Registration form has been uploaded';
                $response['file_name'] = $data['file_name'];
                $response['temp_name'] = $_FILES['reg_file']['name'];
            }

        }else{
            $response['success'] = FALSE;
            $response['msg'] = 'File Registration Required';
        }
        $this->json_result($response);
    }


   public function getting_files($applicant_id, $type){
      $this->db->where('applicant_id', $applicant_id);
      $this->db->where('applicant_type_file', $type);
      return $this->db->get('applicant_file')->row_array();
   }

   public function download($type, $applicant_id){
      $this->load->helper('download');
      $getting_files = $this->getting_files($applicant_id, $type);
      if($type == 1){
         $mime_type = mime_content_type('./files/cv/'.$getting_files['applicant_file'].'');
         if($mime_type == 'application/pdf'){
            header("Content-Type: application/pdf");
            echo file_get_contents('./files/cv/'.$getting_files['applicant_file'].'');
         }else{
            $data = file_get_contents('./files/cv/'.$getting_files['applicant_file'].'');
            force_download($getting_files['applicant_temp_file'], $data);
         }
      }else{
         $mime_type = mime_content_type('./files/regform/'.$getting_files['applicant_file'].'');
         if($mime_type == 'application/pdf'){
            header("Content-Type: application/pdf");
            echo file_get_contents('./files/regform/'.$getting_files['applicant_file'].'');
         }else{
            $data = file_get_contents('./files/regform/'.$getting_files['applicant_file'].'');
            force_download($getting_files['applicant_temp_file'], $data);
         }
      }
      
      // $temp_name = $this->db->select('applicant_temp_file')->where('applicant_file', $file)->where('applicant_type_file', $mode)->get('applicant_file')->row_array();
      // if($mode == 1){
      //    $type = mime_content_type('./files/cv/'.$file.'');
      //    if($type == 'application/pdf'){
      //       // header("Content-Type: application/pdf");
      //       // echo file_get_contents('./files/cv/'.$file.'');
      //       header("Content-type:application/pdf");
      //       header("Content-Disposition:attachment;filename='".file_get_contents('./files/cv/'.$file.'')."'");
      //       readfile(file_get_contents('./files/cv/'.$file.''));
      //    }else{
      //       // force_download($temp_name['applicant_temp_file'], $data);
      //    }
      // }
      // if($mode == 2){
      //    $data = file_get_contents('./files/regform/'.$file.'');
      //    $type = mime_content_type('./files/regform/'.$file.'');
      //    if($type == 'application/pdf'){
      //       header('Content-Type: application/pdf');
      //       echo $data;
      //    }else{
      //       force_download($temp_name['applicant_temp_file'], $data);
      //    }
      // }
      // echo $type;
      // header('Content-Type: application/pdf');
      // $data = file_get_contents('./files/cv/0131570000005bed12dd0c539.pdf');
      // echo $data;
   }

   public function download_reg($file){
      $this->load->helper('download');
      $temp_name = $this->db->select('applicant_temp_file')->where('applicant_file', $file)->where('applicant_type_file', 2)->get('applicant_file')->row_array();
      $data = file_get_contents('./files/regform/'.$file.'');
      force_download($temp_name['applicant_temp_file'], $data);
   }

   public function getEducation($appid){
      $this->db->where('applicant_id', $appid);
      $this->db->where('last_education', 1);
      return $this->db->get('temp_employee_education')->row_array();
   }
}