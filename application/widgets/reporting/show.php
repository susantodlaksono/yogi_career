<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" rel="stylesheet" type="text/css">
<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  
   <div class="row">
      <div class="col-md-12">
         <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
      </div>
   </div>
   <div class="row">
      <div class="col-md-5">
    	   <div class="panel panel-default" style="margin-bottom: 0">
            <div class="panel-body">
               <form id="form-reporting" method="post" action="<?php echo site_url() ?>reporting/widget/generate">
                  <div class="form-group">
                     <label>Vacancy</label>
                     <select class="form-control vacancy" name="vacancy[]" multiple="">
                        <?php
                           foreach ($vacancy as $v) {
                              echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                           }
                        ?>
                     </select>
                  </div>
                  <div class="form-group">
                     <label>Register In</label>
                     <input type="hidden" name="sdate" value="<?php echo date('Y-m-d'); ?>">
							<input type="hidden" name="edate" value="<?php echo date('Y-m-d'); ?>">
                     <input type="text" id="range-date" class="form-control">
                  </div>
                  <button type="submit" class="btn btn-danger btn-block"><i class="fa fa-save"></i> Generate</button>
               </form>
            </div>
         </div>
   </div>
</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>