<?php

class Widget extends Widgets {

   public function __construct() {
      parent::__construct();
      if (!$this->ion_auth->logged_in()) {
         die;
      }
   }

   public function index() {
      $get = $this->input->get();
      $vacancy = $this->db->where('status', 1)->get('vacancy_division')->result_array();
      $data = array(
         'title' => 'Reporting',
         'vacancy' => $vacancy
      );
      $this->render_widget($data);
   }
   
   public function generate(){
      $params = $this->input->post();
      $applicant = $this->get_applicant($params);
      $data['filename'] = 'Applicant Result '.date('d M Y', strtotime($params['sdate'])).' to '.date('d M Y', strtotime($params['edate'])).'';
      if($applicant){
         foreach ($applicant as $k => $v) {
            $tmp[$k] = $v;
            $tmp[$k]['basic_result'] = $this->basic_result($v['id']);
            $tmp[$k]['papi']  = $this->get_papi($v['id']);
			   $tmp[$k]['psikotest']  = $this->psikotest_result($v['id']);
         }
         $data['result'] = $tmp;
      }else{
         $data['result'] = FALSE;
      }
      $this->load->view('reporting/_applicant_result', $data, TRUE);
   }

   public function get_applicant($params){
      $this->db->select('a.*, b.name as vacancy_name');
      $this->db->join('vacancy_division as b', 'a.vacancy_division_id = b.id');
      if(isset($params['vacancy'])){
         $this->db->where_in('a.vacancy_division_id', $params['vacancy']);
      }
      $this->db->where('DATE(a.created_date) BETWEEN "'.$params['sdate'].'" AND "'.$params['edate'].'"');
      return $this->db->get('applicant as a')->result_array();
   }

   public function basic_result($id){
		$test = array(16,17,18,19,3);
		$i = 0;
		foreach ($test as $v) {
			$rs = $this->mapping_trans($id, $v);
			$data[$i] = $rs ? $rs : '';
			$i++;
		}
		return $data;
	}

	public function mapping_trans($id, $test_type){
		$this->db->select('id');
		$this->db->where('applicant_id', $id);
		$this->db->where('test_type_id', $test_type);
		$result = $this->db->get('test_transaction')->row_array();	
		if($result){
			return $this->result_assest($result['id']);
		}else{
			return FALSE;
		}
	}
	public function result_assest($trans_id){
		$this->db->where('test_transaction_id', $trans_id);
		$this->db->where('result', 100);
		return $this->db->count_all_results('test_answers');
	}

   public function get_papi($id){
		$this->db->select('id');
		$this->db->where('applicant_id', $id);
		$this->db->where('test_type_id', 4);
		$result = $this->db->get('test_transaction')->row_array();	
		if($result){
			return $this->result_papi($result['id']);
		}else{
			return FALSE;
		}
	}

   public function psikotest_result($id){
		$this->db->select('id');
		$this->db->where('applicant_id', $id);
		$this->db->where('test_type_id', 1);
		$result = $this->db->get('test_transaction')->row_array();	
		if($result){
			return $this->result_papi($result['id']);
		}else{
			return FALSE;
		}
	}
   
   public function result_papi($trans_id){
		$this->db->select('assessment');
		$this->db->where('test_transaction_id', $trans_id);
		$rs = $this->db->get('test_assessment')->row_array();
		if($rs){
			return $rs['assessment'];
		}else{
			return FALSE;
		}
	}
}