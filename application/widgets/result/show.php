<style type="text/css">
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: white;
		border-color: #dadada;
		color:black;
	}
</style>
<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" rel="stylesheet" type="text/css">

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  

</div>

<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>