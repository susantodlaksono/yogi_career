<?php

class Widget extends Widgets {

    public function __construct() {
        parent::__construct();
        
        $this->load->model('applicant');
        if (!$this->ion_auth->logged_in()) {
            return '{"msg":"success"}';
        }
    }

    public function index() {
        $get = $this->input->get();
        $data = array(
            'title' => 'Applicant',
        );
        $this->render_widget($data);
    }

   public function get_applicant(){
      $params = $this->input->get(); 
      $result = $this->applicant->get_applicant('get', $params);
      if($result){
         foreach ($result as $v) {
            $list[] = array(
               'id' => $v['id'],
               'name' => $v['name'],
               'password_show' => $v['password_show'],
               'email' => $v['email'],
               'education_degree' => $v['education_degree'],
               'birth_date' => $v['birth_date'] ? $v['birth_date'] : NULL,
               'age' => $v['age'] ? $v['age'] : NULL,
               'vacancy_name' => $v['vacancy_name'] ? $v['vacancy_name'] : NULL,
               'created' => $v['created'] ? $v['created'] : NULL,
               'active_login' => $v['active_login'] ? $v['active_login'] : NULL,
               'user_id' => $v['user_id'] ? $v['user_id'] : NULL,
               'num_register' => $v['num_register'] ? $v['num_register'] : NULL,
               'cv' => $this->get_employee_doc($v['id'], 2),
               'pas_photo' => $this->get_employee_doc($v['id'], 1),
               'portofolio' => $this->get_employee_doc($v['id'], 3),
               'notes' => $v['notes'] ? $v['notes'] : NULL
            );
         }
         $response['result'] = $list;
      }

      $response['total'] = $this->applicant->get_applicant('count', $params);
      $response['success'] = TRUE;
      $this->json_result($response);
   }

   public function get_employee_doc($id, $type){
      $this->db->where('applicant_id', $id);
      $this->db->where('doc_type', $type);
      return $this->db->get('temp_employee_doc')->row_array();
   }

   public function get_files($id, $applicant_type_file){
      $this->db->where('applicant_id', $id);
      $this->db->where('applicant_type_file', $applicant_type_file);
      $rs = $this->db->get('applicant_file')->row_array();
      return $rs['applicant_file'];
   }

   public function save_notes(){
      $post = $this->input->post();
      $nts = $post['notes'] != '' ? $post['notes'] : NULL;
      $rs = $this->db->update('applicant', array('notes' => $nts), array('user_id' => $post['id']));
      if($rs){
         $response['success'] = TRUE;
         $response['msg'] = 'Notes Added';  
      }else{
         $response['success'] = FALSE;
         $response['msg'] = 'Transaction Failed';
      }       
      $this->json_result($response);
   }

   public function change_status(){
      error_reporting(0);
      $post = $this->input->post();
      $response['success'] = FALSE;
      $response['msg'] = 'Function Failed';

      $this->load->library('mailing');
      $this->load->library('email_template');

      if(isset($post['mode']) && isset($post['id'])){
         $config = $this->db->select('config_set')->where('config_name', '_send_email_approval_account')->get('config_app')->row_array();
         if($config['config_set'] == 'active'){
            if($post['mode'] == 'active'){
               $applicant = $this->db->select('a.id, a.name, a.email, b.password_show')->join('users as b', 'a.user_id = b.id')->where('a.user_id', $post['id'])->get('applicant as a')->row_array();
               $params['alias'] = 'EB CAREER';
               $params['mail_to'][] = $applicant['email'];
               $params['subject'] = '[EB CAREER] Approval Account';
               $params['body'] = $this->email_template->approval_account($applicant['id']);
               // $mail_status = $this->mailing->send($params);
               $mail_status = TRUE;
            }else{
               $mail_status = TRUE;   
            }
         }else{
            $mail_status = TRUE;
         }
         if($mail_status){
            if($post['mode'] == 'active'){
               $update = $this->db->update('users', array('active' => 1), array('id' => $post['id']));
            }else{
               $update = $this->db->update('users', array('active' => 0), array('id' => $post['id']));
            }
            if($update){
               $response['success'] = TRUE;
               $response['mail_status'] = $mail_status;
            }
         }else{
            $response['msg'] = 'Failed Sending Email to Applicant';
         }
      }
      $this->json_result($response);
   }

    public function change_selected(){
        $post = $this->input->post();
        if($post['mode'] == 'tests'){
            $this->db->where_in('applicant_id', $post['data']);
            $this->db->where_in('test_type_id', $post['value']);
            $this->db->delete('test_transaction');
        }else{
            $user_id = $this->db->select('user_id')->where_in('id', $post['data'])->get('applicant');
            if($user_id->num_rows() > 0){
                foreach ($user_id->result_array() as $key => $value) {
                    $rows = array(
                        'active' => $post['value']
                    );
                    $this->db->where('id', $value['user_id']);
                    $this->db->update('users', $rows);
                }
            }
            
        }
        $response['success'] = TRUE;
        $this->json_result($response);
    }

}