<style type="text/css">
	.pagination>.active>a, .pagination>.active>a:focus, .pagination>.active>a:hover, .pagination>.active>span, .pagination>.active>span:focus, .pagination>.active>span:hover{
		background-color: white;
		border-color: #dadada;
		color:black;
	}
	./*table thead tr th{
		padding-top: 0px;
	}*/
	/*.table tbody tr td{
		padding:0px;
	}*/
	./*table.table-condensed thead tr th{
		padding-left: 0px;
		padding-right: 0px;
	}*/
</style>

<link href="<?php echo base_url('assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css'); ?>" rel="stylesheet" type="text/css">

<div id="<?php echo $widget_name ?>_<?php echo $uniqid ?>">  

	<div class="row">
        <div class="col-md-12">
            <h4 class="font-arial bold" style="margin-left: 15px;font-weight: bold;"><?php echo $title ?></h4>   
        </div>
    </div>
	
	<div class="col-md-12">
    	<div class="panel panel-default" style="margin-bottom: 0">
	    	<div class="panel-header" style="padding:10px;background-color: #fbfbfb;">
				<div class="row">
					<div class="col-md-5">
						<div class="input-group">
							<input type="hidden" id="sdate" value="<?php echo date('Y-m-d'); ?>">
							<input type="hidden" id="edate" value="<?php echo date('Y-m-d'); ?>">
						 	<button type="button" id="range-date" class="btn btn-default">
							 	<i class="fa fa-calendar"></i> <?php echo date('d M Y') ?>
						 	</button>
					 	</div>
					</div>
					<div class="col-md-4">
						<div class="input-group">
							<span class="input-group-addon">Vacancy</span>
							<select class="form-control" id="vacancy">
								<option value="all">All</option>
								<?php
								$vacancy = $this->db->where('status', 1)->get('vacancy_division');
								if($vacancy->num_rows() > 0){
									foreach ($vacancy->result_array() as $key => $value) {
										echo '<option value="'.$value['id'].'">'.$value['name'].'</option>';
									}
								}
								?>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-search"></i></span>
							<input type="text" id="search" class="form-control" placeholder="Search...">
						</div>
					</div>
				</div>
	    	</div>
	       	<div class="panel-body">
				 <table class="table table-condensed table-striped">
					<thead>
						<th>Name</th>
						<th>Vacancy</th>
						<th>Contact</th>
						<th width="250">email</th>
						<th>Register</th>
						<th>Status</th>
						<th class="text-center"></th>
					</thead>
					<tbody id="section-data"></tbody>
				</table>
				<div class="row">
					<div class="col-md-3 pull-left">
						<span id="section-total" class="label label-default bold">0 Data Found</span>
					</div>
					<div class="col-md-9">
						<ul id="section-pagination" class="pagination pagination-sm no-margin pull-right"></ul>
					</div>
				</div>
	       	</div>
   		</div>
	</div>
	
	<div class="modal fade slide-up disable-scroll in" id="modal-view-detail">
    	<div class="modal-dialog  modal-lg">
      		<div class="modal-content-wrapper">
        		<div class="modal-content" style="width:1300px;">
		          	<div class="modal-body modal-body-view-detail" style="margin-top: 15px;">
		          		<div class="row" style="background-color: #f9f9f9">
		          			<div class="col-md-3" style="border-right: 1px solid #e8e8e8">
		          				<h5 class="bold applicant-name" style="margin: 0px;"></h5>
		          				<h6 style="margin: 0px;">
		          					<span class="applicant-email"></span> | 
		          					<span class="applicant-contact-number"></span> | 
		          					<span class="applicant-age"></span> TH
		          				</h6>
	          				</div>
	          				<div class="col-md-3" style="border-right: 1px solid #e8e8e8">
		          				<h5 class="bold applicant-education-degree" style="margin: 0px;"></h5>
		          				<h6 style="margin: 0px;">
		          					From : 
		          					<span class="applicant-school-majors"></span> <i class="fa fa-arrow-right"> </i>
		          					<span class="applicant-university"></span>
		          				</h6>
	          				</div>
	          				<div class="col-md-3" style="border-right: 1px solid #e8e8e8">
		          				<h5 class="bold" style="margin: 0px;">As <span class="applicant-vacancy"></span></h5>
		          				<h6 style="margin: 0px;">
		          					<span class="applicant-level"></span>
		          				</h6>
	          				</div>
	          				<div class="col-md-3">
		          				<h6 style="margin-top:5px;">
		          					Download CV <span class="applicant-cv"></span>
		          				</h6>
		          				<h6>
		          					Download Registration Form <span class="applicant-register"></span>
		          				</h6>
	          				</div>
	          			</div>
	          			<div class="row result-detail" style="margin-top:10px;">
          				</div>
		          		<!-- <div class="row">
							<div class="col-md-5">
								<h4 class="bold text-danger">Applicant Profil</h4>
								<table class="table table-condensed">
	                                <tbody>
	                                    <tr>
	                                        <td width="170" class="bold">Name</td>
	                                        <td class="applicant-name"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">Email</td>
	                                        <td class="applicant-email"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">Birth Date</td>
	                                        <td class="applicant-birth-date"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">Age</td>
	                                        <td class="applicant-age"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">Contact Number</td>
	                                        <td class="applicant-contact-number"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="190" class="bold">Education Degree</td>
	                                        <td class="applicant-education-degree"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">School Majors</td>
	                                        <td class="applicant-school-majors"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">University</td>
	                                        <td class="applicant-university"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">Apply As</td>
	                                        <td class="applicant-vacancy"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">Level</td>
	                                        <td class="applicant-level"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">CV</td>
	                                        <td class="applicant-cv"></td>
	                                    </tr>
	                                    <tr>
	                                        <td width="150" class="bold">Registration Form</td>
	                                        <td class="applicant-register"></td>
	                                    </tr>
	                                </tbody>
	                            </table>
							</div>
							<div class="col-md-7 result-detail" style="height: 600px;overflow: auto;">
							</div>
		          		</div>
		          	</div> -->
		          	<div class="modal-footer">
		          		<div class="btn-group">
		          			<input type="hidden" id="applicant_id" value="">
							<button class="btn btn-default" data-dismiss="modal"><i class="fa fa-arrow-left"></i> Back</button>
							<button class="btn btn-success btn-status" data-status="2">Pass</button>
							<button class="btn btn-danger btn-status" data-status="1">Not Pass</button>
						</div>
		          	</div>
	          	</div>
          	</div>
      	</div>
  	</div>

</div>

<div class="modal fade slide-up disable-scroll in" id="modal-result">
 	<div class="modal-dialog  modal-lg">
		<div class="modal-content-wrapper">
     		<div class="modal-content" style="width:1300px;height: 700px;overflow: auto;">
     			<div class="modal-header">
     				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
     			</div>
          	<div class="modal-body modal-body-view-detail" style="margin-top: 15px;">
          	</div>
       	</div>
    	</div>
 	</div>
</div>



<script>
    var uniqid = '<?php echo $uniqid; ?>';
    var container = '#<?php echo $widget_name; ?>_<?php echo $uniqid; ?>';
</script>

<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>