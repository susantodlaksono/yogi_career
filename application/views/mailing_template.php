<!doctype html>
<html>
    <head>
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <style>
        /* -------------------------------------
            GLOBAL
        ------------------------------------- */
        * {
          font-size: 100%;
          line-height: 1.6em;
          margin: 0;
          padding: 0;
        }

        img {
          max-width: 600px;
          width: 100%;
        }

        body {
          -webkit-font-smoothing: antialiased;
          height: 100%;
          -webkit-text-size-adjust: none;
          width: 100% !important;
        }


        /* -------------------------------------
            BODY
        ------------------------------------- */
        table {
          width: 100%;
        }

        table.body-wrap {
          padding: 5px;
          width: 100%;
        }

        table.body-wrap .container {
          border: 1px solid #f0f0f0;
        }


        /* -------------------------------------
            FOOTER
        ------------------------------------- */
        table.footer-wrap {
          clear: both !important;
          width: 100%;
        }

        .footer-wrap .container p {
          color: #666666;
          font-size: 12px;

        }

        table.footer-wrap a {
          color: #999999;
        }


        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        p{
          font-size: 13px;
          font-weight: normal;
          margin-bottom: 10px;
          letter-spacing:0.4px;
        }

        ul li,
        ol li {
          margin-left: 5px;
          list-style-position: inside;
        }
        .title{
            letter-spacing:0.6px;
            font-size:14.5px;
            text-decoration:none;
        }
        .title:hover {
            color: red;
            text-decoration:none;
        }
        .linked{
            color:#000;font-weight:normal;font-size:11.5px;margin-top:-20px;
        }
        .descrip{
            font-size:12px;font-weight:normal;
        }
        .tone_neg{
            font-size:12px;
            background-color:#f93b3b;
            color:#fff;
        }
    </style>
    </head>

   <body bgcolor="#f6f6f6">

    <!-- body -->
   <table class="body-wrap" bgcolor="#f6f6f6">
      <tr>
        <td></td>
        <td class="container" bgcolor="#FFFFFF">
            <p style="text-align:center;padding:10px;border-bottom:1px solid #e8e2e2;">
               <span style="font-size:25px;text-transform: uppercase;color:#f11818;font-weight:bold;letter-spacing:0.5px;">
                  CAREER
               </span>
               <br>
               <span style="color:#848181;">Terima kasih anda telah mendaftarkan diri anda untuk menjadi bagian dari perusahaan kami</span><br>
               <span style="color:#848181;">Harap Tunggu. Registrasi anda akan segera di verifikasi untuk bisa login di portal hiring kami</span><br>
               <span style="color:#ff1010;">Verifikasi akan di beritahukan ke email anda</span>
            </p>
            <p style="text-align:center; letter-spacing:0.5px">
               <span style="font-size:13px; font-weight:bold">akun login anda</span>
            </p>
            <table style="width:auto; margin:0 auto; border:0px solid #ececec;margin-bottom:20px;">
               <tbody>
                  <tr>
                     <td style="text-align:left; width:20%; border-bottom:1px solid #e4e4e4">Username</th>
                     <td style="text-align:center; width:20%; border-bottom:1px solid #e4e4e4">:</th>
                     <td style="text-align:left;border-bottom:1px solid #e4e4e4">susanto@gmail.com</th>
                  </tr>
                  <tr>
                     <td style="text-align:left; width:20%; border-bottom:1px solid #e4e4e4">Password</th>
                     <td style="text-align:center; width:20%; border-bottom:1px solid #e4e4e4">:</th>
                     <td style="text-align:left; border-bottom:1px solid #e4e4e4">231382918DSJJKDJSJiej</th>
                  </tr>
               </tbody>
            </table>
            <div class="content">
            </div>
        </td>
        <td></td>
      </tr>
   </table>
    <!-- /body -->

    <!-- footer -->
   <table class="footer-wrap">
      <tr>
         <td></td>
         <td class="container">
            <div class="content">
               <table>
                  <tr>
                     <td align="center">
                     <p>This email is generated and sent automatically using Career . Copyright (c) Indonesia Indicator 2018</p>
                     </td>
                  </tr>
               </table>
            </div>
         </td>
        <td></td>
      </tr>
   </table>
   
   </body>
</html>