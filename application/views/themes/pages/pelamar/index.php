<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="<?php echo config_item('app_desc');?>" />
        <meta name="author" content="<?php echo config_item('app_author')?>" />
        <link href="<?php echo base_url('favicon.png'); ?>" rel="shortcut icon"/>
        <title>EB CAREER</title>

        <link class="main-stylesheet" href="<?php echo base_url('assets/pages/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/loading.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/pace/pace-theme-flash.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/bootstrapv3/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url('assets/plugins/switchery/css/switchery.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url('assets/pages/css/pages-icons.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/plugins/simplepagination/simplePagination.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/plugins/daterangepicker/css/daterangepicker.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/plugins/select2/css/select2.min.css') ?>" rel="stylesheet" type="text/css" >
        <link href="<?php echo base_url('assets/plugins/font-awesome-5.0.1/css/fontawesome-all.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/snackbar/css/snackbar.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/snackbar/css/material.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/datatable/dataTables.min.css'); ?>" rel="stylesheet" type="text/css" />        
	    <script src="<?php echo base_url('assets/plugins/jquery/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/jquery.countdown.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrap-sweetalert/sweet-alert.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrap-sweetalert/jquery.sweet-alert.init.js') ?>"></script>
    </head>

    <body class="fixed-header windows menu-behind desktop pace-done horizontal-menu">
    	<div class="page-container">
    		<div class="header" style="background-color:#f11818">
				<div class=" pull-left sm-table hidden-xs hidden-sm">
					<div class="header-inner">
			         <div class="brand inline" style="width: auto;text-align: left;">
			            <img src="<?php echo base_url('assets/img/ebdesk.svg') ?>" width="30" height="30" style="margin-top: 8px;margin-right: 7px;">
			            <h5 class="bold" style="float:right;font-size: 20px;margin-top: 8px;color: #fff;">CAREER</h5>
			         </div>
					</div>
				</div>
				<div class="pull-right">
			  		<div class="header-inner">
			        	<a href="<?php echo site_url('security/logout') ?>" style="color:#fff;font-size: 16px;font-weight: bold;"><i class="fa fa-sign-out"></i> Logout</a>
			   	</div>
			   </div>
			</div>
		   <div class="page-content-wrapper content-builder active full-height">
                <div class="content" style="background-color: white;">
                    <div class="container-fluid container-fixed-lg" id="container-content" style="min-height: 590px;">
                	 	<?php
                        if (!defined('BASEPATH')) 
                            exit('No direct script access allowed');
                            if ($content) {
                                $this->load->view($content);
                            }  
                        ?>         
                    </div>
                </div>
            </div>
        </div>

        <script src="<?php echo base_url('assets/setup.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/widget.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/datatable/jquery.dataTables.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/pace/pace.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/modernizr.custom.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrapv3/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery/jquery-easy.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-unveil/jquery.unveil.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-bez/jquery.bez.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-actual/jquery.actual.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/plugins/classie/classie.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/switchery/js/switchery.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/pages/js/pages.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/scripts.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/timeline.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/simplepagination/jquery.simplePagination.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/momentjs/momentjs.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/daterangepicker/js/daterangepicker.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery.form.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/select2/js/select2.full.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/snackbar/js/snackbar.min.js') ?>"></script>
        

        <script>
		    var site_url = '<?php echo site_url(); ?>';
            var base_url = '<?php echo base_url(); ?>';
		    var loading = '<h5 class="font-arial" style="font-size:15px;"></h5>';
		</script>
		
	</body>

</html>