<style type="text/css">
    .bar-question{
        font-size: 15px;
        color:black;
    }
    .bar-question > p{
        font-size: 15px;
        color:black;
    }
    .btn-danger{
        background-color: #f11818;
    }
    .text-danger{
        color: #f11818 !important;
    }
    .radio.radio-danger input[type=radio]:checked + label:before{
        border-color: #f11818;
    }
</style>

<div class="row" style="margin-top: 10px;">
    <div class="col-md-3" style="border-right: 0px solid #e5e8e9;">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-transparent" style="margin-bottom: 0">
                  <div class="panel-body">
                    <h4 class="text-center" style="font-weight:bold;">Test Name</h4>
                    <h5 class="font-arial text-center text-danger" style="margin-top: -12px;font-weight:bold;">
                        <?php echo $detail_tests['name'] ?>
                    </h5>
                    <hr style="border-color: #f5f3f3">

                    <h4 class="text-center" style="font-weight:bold;">Question</h4>
                    <h5 class="font-arial text-center text-danger" style="margin-top: -12px;font-weight:bold;">
                        <?php echo $detail_tests['total_question'] ?>
                    </h5>
                    <hr style="border-color: #f5f3f3">

                    <h4 class="text-center" style="font-weight:bold;">Start</h4>
                    <h5 class="font-arial text-center text-danger" style="margin-top: -12px;font-weight:bold;">
                        <?php echo date('H:i', strtotime($detail_tests['time_start'])) ?> sd <?php echo date('H:i', strtotime($detail_tests['time_end'])) ?>
                    </h5>
                    <hr style="border-color: #f5f3f3">

                    <h4 class="text-center" style="font-weight:bold;">Time Left</h4>
                    <h5 class="font-arial text-center text-danger" style="margin-top: -12px;font-weight:bold;" id="clock-time"></h5>
                    <hr style="border-color: #f5f3f3">
                    
                    <a href="<?php echo site_url() ?>" class="btn btn-default btn-block btn-back"><i class="fa fa-arrow-left"></i> Back To List</a>
                    <button class="btn btn-default btn-block" data-target="#modal-instructions" data-toggle="modal"><i class="fa fa-info"></i> Test Instructions</button>
                    <button class="btn btn-danger btn-block btn-finish-test"><i class="fa fa-check"></i> Finish Test</button>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="panel panel-transparent panel-list-question" style="margin-bottom: 0;">
            <div class="panel-body">
                <ul class="nav nav-pills nav-stacked" id="tests_no" style="width:150px;">
                    <?php 
                    $answers = $this->db->select('max(a.question_id) as last_question_id')
                                ->join('question as b', 'a.question_id = b.id')
                                ->where('b.test_type_id', $detail_tests['test_id'])
                                ->get('test_answers as a')->row_array();

                    $questions = $this->db->select('id')
                                ->where('test_type_id', $detail_tests['test_id'])
                                ->order_by('id', 'asc')
                                ->get('question');

                    $first_question = $this->db->select('min(id) as first_question')
                                                ->where('test_type_id', $detail_tests['test_id'])
                                                ->get('question')->row_array();
                                                
                    $list = $this->db->select('question_id')
                                    ->where('test_transaction_id', $detail_tests['id'])
                                    ->get('test_answers')->result_array();

                    if(count($list) > 0){
                        foreach ($list as $v) {
                            $listdata[] = $v['question_id'];
                        }
                    }else{
                        $listdata = array();
                    }
                    if($questions->num_rows() > 0){
                        $no = 0;
                        foreach ($questions->result_array() as $key => $value) {
                            $icon  = in_array($value['id'], $listdata) ? '<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>&nbsp;' : '<i class="fa fa-circle text-danger pull-left" style="margin-top:5px;"></i>&nbsp;';
                            $no++;
                            echo '<li>';
                                echo '<a style="border: 1px solid #efe9e9;border-radius: 5px;cursor: pointer;" class="detail-soal" data-id="'.$value['id'].'">'.$icon.' No '.$no.'</a>';
                            echo '</li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <form id="form-save-answers">
            <div class="panel" style="margin-bottom: 0;margin-top: 10px;">
                <input type="hidden" id="question_id" value="<?php echo $first_question['first_question'] ?  $first_question['first_question'] : '' ?>">
                <div class="panel-body question-section">
                </div>
            </div>
        </form>
    </div>
</div>



<script src="<?php echo base_url('assets/js/jquery.form.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') ?>"></script>
<script src="<?php echo base_url('assets/plugins/slimScroll/jquery.slimscroll.min.js') ?>"></script>

<link rel="stylesheet" href="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.min.css') ?>" />
<script src="<?php echo base_url('assets/plugins/fancybox/jquery.fancybox.min.js') ?>"></script>

<script type="text/javascript">
    var reg_id = '<?php echo $id; ?>';
    var now_date = '<?php echo date('Y-m-d H:i:s'); ?>';
    var time_test = '<?php echo $detail_tests['time']; ?>';
    var end_time = '<?php echo $detail_tests['time_end']; ?>';
    var test_transaction_id = '<?php echo $detail_tests['test_transaction_id']; ?>';

    function startTime(){
        var today = new Date();
        var h = today.getHours();
        var m = (today.getMinutes()<10?'0':'') + today.getMinutes()
        var s = today.getSeconds(); 
        return [ h, m, s ].join(':');
    }

    $(function () {
        
        'use restrict';

        var _question_id = $('#question_id').val();

        $.fn.toggleQuestion = function(id) {
            $('.detail-soal[data-id="' + id + '"]').removeAttr('style');
            for (var i = 1; i <= id; i++) {
                $('.detail-soal').css("font-weight", "");
                $('.detail-soal[data-id="' + i + '"]').attr('style', 'border: 1px solid #efe9e9;border-radius: 5px;background-color:#ffffff;cursor: pointer;');
            }
            $('.detail-soal[data-id="' + id + '"]').attr('style', 'border: 1px solid #efe9e9;border-radius: 5px;cursor: pointer;font-weight:bold;');
        };

        $('.panel-list-question').slimScroll({height: '550px', alwaysVisible:true});

        $('#clock-time').countdown(end_time)
            .on('update.countdown', function (event) {
                $(this).html(event.strftime('%H:%M:%S'));
            })
            .on('finish.countdown', function (event) {
                $(this).finish_tests();
                return false;
            });

        $.fn.get_questions = function(params) {
            var p = $.extend({
                question_id : _question_id,
                test_transaction_id : test_transaction_id,
                load : true
            }, params);

            ajaxManager.addReq({
                url: site_url + 'mulai_test/get_questions',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    question_id: p.question_id,
                    test_transaction_id: test_transaction_id

                },
                beforeSend: function () {
                    $(this).toggleQuestion(p.question_id);
                    if(p.load){
                        $('.question-section').html('<h6 class="text-center font-arial"><i class="fa fa-spinner fa-spin"></i> Please Wait...</h6>');
                    }
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    if(r.success){
                        t = '';
                        t += '<input type="hidden" id="test_transaction_id" name="test_transaction_id" value="'+test_transaction_id+'">';
                        t += '<input type="hidden" name="mode" class="mode-saving">';
                        t += '<input type="hidden" id="q_id" name="q_id" value="'+r.question.id+'">';
                        t += '<input type="hidden" name="previous_id" value="'+r.previous_id+'">';
                        t += '<input type="hidden" name="next_id" value="'+r.next_id+'">';
                        t += '<input type="hidden" name="question_type_id" value="'+r.question.question_type_id+'">';
                        
                        if(r.question.question_text){
                            t += '<h5 class="font-arial" style="font-weight: bold;color:#f11818">Question</h5>';
                            if(r.question.question_description){
                                t += '<span class="label label-info">'+r.question.question_description+'</span>';
                            }
                            t += '<h5 class="font-arial bar-question" style="font-size:15px;">'+(r.question.question_text ? r.question.question_text : '')+'</h5>';  
                        }
                        
                        if(r.question.question_file){
                            t += '<h5 class="font-arial text-danger" style="font-weight: bold;">Question File</h5>';
                            $.each(r.question.question_file, function (k,v){
                                t += '<p><a href="'+base_url+'files/question_file/'+v.file+'">'+v.file+'</a></p>';
                            });
                        }
                            
                        if(r.question.question_image){
                            t += '<h5 class="font-arial text-danger" style="font-weight: bold;">Question Image</h5>';
                            $.each(r.question.question_image, function (k,v){
                                t += '<a data-fancybox="" href="'+base_url+'files/question_image/'+v.image+'"><img class="img-thumbnail" style="margin-bottom:5px;max-width: 696px;max-height: 500px;"" src="'+base_url+'files/question_image/'+v.image+'"></a>';
                            });
                        }
                        if(r.question.question_answers){
                            switch(r.question.question_type_id){
                                case '1':
                                    t += '<input type="hidden" name="correct_answers_question" value="'+r.question.correct_answers_question+'">';
                                    t += '<div class="radio radio-danger">';
                                        $.each(r.question.question_answers, function (k,v){
                                            t += '<input type="radio" value="'+v.answers_id+'" name="correct_answers" class="choose_answers" id="opt_'+v.answers_id+'" '+(v.answers_id === r.answers_question ? 'checked' : '')+'>';
                                            t += '<label for="opt_'+v.answers_id+'" style="margin-bottom: 10px;font-size:15px;color:black;">'+v.question_answers+'</label><br>';
                                        });
                                    t += '</div>';  
                                    break;
                                case '4':
                                    var data = [];
                                    $.each(r.answers_question, function (kk,vv){
                                        data.push(vv);
                                    }); 
                                    console.log(data);

                                    $.each(r.question.question_answers, function (k,v){
                                        t += '<div class="checkbox check-danger">';
                                            if(data){
                                                if(jQuery.inArray(v.answers_id, data) !== -1){
                                                    t += '<input type="checkbox" class="answers_multiple" name="answers_multiple[]" value="'+v.answers_id+'" data-id="'+v.answers_id+'" id="checkbox_'+v.answers_id+'" checked>';
                                                }else{
                                                    t += '<input type="checkbox" class="answers_multiple" name="answers_multiple[]" value="'+v.answers_id+'" data-id="'+v.answers_id+'" id="checkbox_'+v.answers_id+'">';
                                                }
                                            }else{
                                               t += '<input type="checkbox" class="answers_multiple" name="answers_multiple[]" value="'+v.answers_id+'" data-id="'+v.answers_id+'" id="checkbox_'+v.answers_id+'">'; 
                                            }
                                            t += '<label for="checkbox_'+v.answers_id+'">'+v.question_answers+'</label>';
                                        t += '</div>';
                                    });
                                    break;

                                case '3':
                                    t += '<textarea style="margin-top:10px;" class="form-control">'+(r.answers_question ? r.answers_question : '')+'</textarea>';
                                    break;
                                case '2':
                                    t += '<input type="file" style="margin-top:10px;" name="correct_answers" class="form-control" id="correct_answers_file">';
                                    if(r.answers_question){
                                        t += '<p id="your-file">Your File : <a href="'+site_url+'mulai_test/applicant_download/'+r.answers_question+'">'+r.answers_question_temp_file+'</a></p>';
                                    }else{
                                        t += '<p id="your-file">Your File : -</p>';
                                    }
                                    break;
                                case '7':
                                    t += '<div class="radio radio-danger">';
                                        $.each(r.question.question_answers, function (k,v){
                                            var re_answers = v.question_answers_alias ? v.question_answers_alias : v.answers_id;
                                            t += '<input type="radio" value="'+re_answers+'" name="correct_answers" class="choose_answers" id="opt_'+v.answers_id+'" '+(re_answers === r.answers_question ? 'checked' : '')+'>';
                                            t += '<label for="opt_'+v.answers_id+'" style="margin-bottom: 10px;font-size:15px;color:black;">'+v.question_answers+'</label><br>';
                                        });
                                    t += '</div>';  
                                    break;
                                case '8':
                                    t += '<div class="radio radio-danger">';
                                        $.each(r.question.question_answers, function (k,v){
                                            var re_answers = v.question_answers_alias ? v.question_answers_alias : v.answers_id;
                                            t += '<input type="radio" value="'+re_answers+'" name="correct_answers" class="choose_answers" id="opt_'+v.answers_id+'" '+(re_answers === r.answers_question ? 'checked' : '')+'>';
                                            t += '<label for="opt_'+v.answers_id+'" style="margin-bottom: 10px;font-size:15px;color:black;">'+v.question_answers+'</label><br>';
                                        });
                                    t += '</div>';  
                                    break;
                            }
                        }else{
                            t += '<h5 class="font-arial text-center text-danger">No Result Answers</h5>';
                        }
                        t += '<hr>';
                        if(r.question.id != r.min_soal){
                            t += '<button style="margin-top:10px;" name="mode" class="btn btn-danger saving-answers pull-left" type="submit" value="previous"><i class="fa fa-chevron-left"></i> Previous</button>&nbsp;';    
                        }
                        if(r.question.id != r.max_soal){
                            t += '<button style="margin-top:10px;" name="mode" class="btn btn-danger saving-answers pull-right" type="submit" value="next">Next <i class="fa fa-chevron-right"></i></button>';
                        }

                        $('.question-section').html(t);
                        $('.tagsinput').tagsinput();
                        if(r.question.question_type_id == 3){
                            tinymce.init({
                                selector: 'textarea',
                                menubar: false,
                                paste_as_text: false,
                                paste_enable_default_filters: false,
                                height: '200px',
                                setup:function(ed) {
                                   ed.on('change', function(e) {
                                        $('#form-save-answers').ajaxSubmit({
                                            url: site_url + 'mulai_test/save_answers/_uncsrf',
                                            type: 'POST',
                                            data:{
                                                type : 'save',
                                                correct_answers : ed.getContent()
                                            },
                                            dataType: 'JSON',
                                            success: function(r) {
                                                $('#csrf').val(r.csrf);
                                                if(r.answered){
                                                    $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                                                    $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>');
                                                }
                                                if(!r.success){
                                                    alert('Failed to save your answers');
                                                }
                                            },
                                            error: function (jqXHR, status, errorThrown) {
                                                error_handle(jqXHR, status, errorThrown);
                                            },
                                        });
                                   });
                                }
                            });
                        }
                    }else{
                      alert(r.msg);
                      window.location.href = location; 
                    }
                }
            });
        }

        $(this).get_questions();
        
        $(this).on('click', '.answers_multiple', function (e){
            var id = $(this).data('id');
            var data = [];
            $('.answers_multiple').each(function () {
                if (this.checked) {
                    data.push($(this).val());
                }
            });
            ajaxManager.addReq({
                url: site_url + 'mulai_test/save_answers_multiple',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    answers_multiple: data,
                    test_transaction_id: $('#test_transaction_id').val(),
                    question_id: $('#q_id').val()

                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    if(r.answered){
                        $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                        $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>');
                    }else{
                        $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                        $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-danger pull-left" style="margin-top:5px;"></i>');
                    }
                    if(!r.success){
                        $('#checkbox_'+id+'').prop('checked', false);
                        alert('Sorry maximum answer can only be less than two');
                    }
                }
            });
        });
        
        $(this).on('click', '.btn-more', function (e){
            var id = $(this).data('id');
            t = '';
            t += '<div class="row statement-holder" style="margin-bottom:10px;">';
                t += '<div class="col-md-5">';
                    t += '<input type="text" name="sh[statement][]" class="form-control" placeholder="Add Statement..." required="">';
                t += '</div>';
                t += '<div class="col-md-5">';
                    t += '<input type="text" name="sh[holder][]" required="" class="form-control tagsinput" placeholder="Add Holder..." data-role="tagsinput">';
                t += '</div>';
                t += '<div class="col-md-2">';
                    t += '<button class="btn btn-complete btn-sm btn-remove" type="button"><i class="fa fa-remove"></i></button>';
                t += '</div>';
            t += '</div>';
            $('#statement-holder').append(t);
            $('.tagsinput').tagsinput('refresh');
            e.preventDefault();
        });

        $(this).on('click', '.btn-remove', function (e){
            $(this).parent().parent().remove();
            e.preventDefault();
        });

        
        $(this).on('change', '#correct_answers_file', function (e){
            if( document.getElementById("correct_answers_file").files.length !== 0 ){
                $('#form-save-answers').attr('enctype', 'multipart/form-data');

                $('#form-save-answers').ajaxSubmit({
                    url: site_url + 'mulai_test/save_answers/_csrf',
                    type: 'POST',
                    data:{
                        type : 'save'
                    },
                    dataType: 'JSON',
                    beforeSend: function(){
                         $('#your-file').html('<h6 class="font-arial"><i class="fa fa-spinner fa-spin"></i> Please Wait...</h6>');
                    },
                    success: function(r) {
                        $('#csrf').val(r.csrf);
                        if(r.answered){
                            $('#your-file').html('Your File : <a href="'+site_url+'mulai_test/applicant_download/'+r.answers_file+'">'+r.answers_file_temp_name+'</a></p>');
                            $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                            $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>');
                        }
                        if(!r.success){
                            $('.container-fluid').pgNotification({
                                style: 'flip',
                                message: r.msg,
                                position: 'top-right',
                                timeout: 2000,
                                type: 'danger'
                            }).show();
                            $('#your-file').html('');
                        }
                        
                    },
                    error: function (jqXHR, status, errorThrown) {
                        error_handle(jqXHR, status, errorThrown);
                    },
                });
            }
        });
        
        $(this).on('click', '.choose_answers', function (e){
            $('#form-save-answers').ajaxSubmit({
                url: site_url + 'mulai_test/save_answers/_csrf',
                type: 'POST',
                data:{
                    type : 'save'
                },
                dataType: 'JSON',
                success: function(r) {
                    $('#csrf').val(r.csrf);
                    if(r.answered){
                        $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                        $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>');
                    }
                    if(!r.success){
                        $('.container-fluid').pgNotification({
                            style: 'flip',
                            message: 'Failed to save your answers',
                            position: 'top-right',
                            timeout: 2000,
                            type: 'danger'
                        }).show();
                    }
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                 },
            });
        });


        $(this).on('submit', '#form-save-answers', function (e) {
            var form = $(this);
            var mode = $(this).data('mode');
            $(this).ajaxSubmit({
                url: site_url + 'mulai_test/next_step',
                data:{
                    type : 'none',
                },
                type: 'POST',
                dataType: 'JSON',
                beforeSend: function () {
                    form.find('')
                },
                success: function(r) {
                    $('#csrf').val(r.csrf);
                    if(r.answered){
                        $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                        $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>');
                    }else{
                        $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                        $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-danger pull-left" style="margin-top:5px;"></i>');
                    }

                    if(r.success){
                        if(r.mode === 'previous'){
                            $(this).get_questions({
                                question_id : r.previous_id,
                                load : true
                            });
                        }
                        if(r.mode === 'next'){
                            $(this).get_questions({
                                question_id : r.next_id,
                                load : true
                            });
                        }
                    }else{
                        $('.container-fluid').pgNotification({
                            style: 'flip',
                            message: 'Failed to save your answers',
                            position: 'top-right',
                            timeout: 2000,
                            type: 'danger'
                        }).show();
                    }
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
            });
            e.preventDefault();
        });

        $(this).on('click', '.saving-answers', function (e){
            $('.mode-saving').val($(this).val());
        });

        $(this).on('click', '.btn-finish-test', function (e){
            var conf = confirm('Are you sure ?');
            if(conf){
                var question_type_id = $('#form-save-answers input[name="question_type_id"]').val();
                if(question_type_id == 5 || question_type_id == 6){
                    $('#form-save-answers').ajaxSubmit({
                        url: site_url + 'mulai_test/next_step',
                        data:{
                            type : 'none'
                        },
                        type: 'POST',
                        dataType: 'JSON',
                        success: function(r) {
                            $('#csrf').val(r.csrf);
                            if(r.answered){
                                $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                                $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>');
                            }else{
                                $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                                $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-danger pull-left" style="margin-top:5px;"></i>');
                            }
                            if(r.success){
                                $(this).finish_tests({
                                    checking_mode : 'not_true'
                                });
                            }else{
                                alert('Failed to save your answers');
                            }
                        },
                        error: function (jqXHR, status, errorThrown) {
                            error_handle(jqXHR, status, errorThrown);
                        },
                    });
                }else{
                    $(this).finish_tests({
                        checking_mode : 'not_true'
                    });
                }
            }else{
                return false;
            }
        });

        $.fn.finish_tests = function(params) {
            var p = $.extend({
                checking_mode : true
            }, params);

            ajaxManager.addReq({
                url: site_url + 'mulai_test/finish_tests',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    test_id: reg_id,
                    checking_mode: p.checking_mode,
                    test_transaction_id: test_transaction_id
                },
                beforeSend: function () {
                   
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    if(p.checking_mode !== 'not_true'){
                        alert('Your time has expired, you will be redirected from this page');
                        // swal({
                        //     title: "Time Out",
                        //     text: "<span class='text-danger'>Your time has expired, you will be redirected from this page</span>",
                        //     html: true,
                        //     confirmButtonColor: '#eb7374',
                        //     confirmButtonText: "OK"
                        // });
                    }

                    if(r.success){
                        window.location.href = site_url;
                    }else{
                        alert(r.msg);
                    }
                }
            });
        }


        $(this).on('click', '.detail-soal', function (e){
            var id = $(this).data('id');
            var question_type_id = $('#form-save-answers input[name="question_type_id"]').val();
            if(question_type_id == 5 || question_type_id == 6){
                $('#form-save-answers').ajaxSubmit({
                    url: site_url + 'mulai_test/next_step',
                    data:{
                        type : 'none'
                    },
                    type: 'POST',
                    dataType: 'JSON',
                    success: function(r) {
                        $('#csrf').val(r.csrf);
                        if(r.answered){
                            $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                            $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-success pull-left" style="margin-top:5px;"></i>');
                        }else{
                            $('.detail-soal[data-id="' + r.question_id + '"]').find('i').remove();
                            $('.detail-soal[data-id="' + r.question_id + '"]').prepend('<i class="fa fa-circle text-danger pull-left" style="margin-top:5px;"></i>');
                        }
                        if(r.success){
                            $(this).get_questions({
                                question_id : id,
                                load : true
                            });
                        }else{
                            $('.container-fluid').pgNotification({
                                style: 'flip',
                                message: 'Failed to save your answers',
                                position: 'top-right',
                                timeout: 2000,
                                type: 'danger'
                            }).show();
                        }
                    },
                    error: function (jqXHR, status, errorThrown) {
                        error_handle(jqXHR, status, errorThrown);
                    },
                });
            }else{
                $(this).get_questions({
                    question_id : id,
                    load : true
                });
                e.preventDefault();
            }
        });

    });

</script>