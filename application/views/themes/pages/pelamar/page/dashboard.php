<div class="row" style="margin-top: 10px;">
    <div class="col-md-5">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-transparent" style="margin-bottom: 0;">
                    <div class="panel-heading">
                        <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-user"></i> Profil Anda</div>
                    </div>
                    <div class="panel-body">
                        <table class="table table-condensed">
                            <tbody>
                                <tr>
                                    <td width="170" class="bold">Nama</td>
                                    <td ><?php echo $pelamar['name'] ?></td>
                                </tr>
                                <tr>
                                    <td width="150" class="bold">Email</td>
                                    <td><?php echo $pelamar['email'] ?></td>
                                </tr>
                                <tr>
                                    <td width="150" class="bold">No. Handphone</td>
                                    <td><?php echo $pelamar['contact_number'] ?></td>
                                </tr>
                                <tr>
                                    <td width="190" class="bold">Tingkat Pendidikan</td>
                                    <td><?php echo $pelamar['education_degree'] ?></td>
                                </tr>
                                <tr>
                                    <td width="150" class="bold">Universitas/Sekolah</td>
                                    <td><?php echo $pelamar['university'] ?></td>
                                </tr>
                                <tr>
                                    <td width="150" class="bold">Bidang</td>
                                    <td><?php echo $pelamar['school_majors'] ?></td>
                                </tr>
                                <tr>
                                    <td width="150" class="bold">Birth Date</td>
                                    <td><?php echo $pelamar['birth_date'] ? date('d M Y', strtotime($pelamar['birth_date'])) : '' ?></td>
                                </tr>
                                <tr>
                                    <td width="150" class="bold">Lowongan</td>
                                    <td style=""><?php echo $pelamar['vacancy_division_name'] ?></td>
                                </tr>
                                <tr>
                                    <td width="150" class="bold" style="">Level</td>
                                    <td style=""><?php echo $pelamar['level'] == 1 ? 'Fresh Graduate' : 'Expert' ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-transparent" style="margin-bottom: 0">
                    <div class="panel-heading">
                        <div class="panel-title bold text-danger" style="font-size: 17px;"><i class="fa fa-edit"></i> Daftar Test</div>
                    </div>
                    <div class="panel-body">
                       	<div class="row">
                          	<div class="col-md-12">
                             	<button class="btn btn-success btn-block" data-target="#modal-instructions" data-toggle="modal"><i class="fa fa-info"></i> Instruksi Tes</button>
                          	</div>
                       	</div>
                       	<br>
                       	<?php
                   		if($test_list->num_rows() > 0){
                   			foreach ($test_list->result_array() as $key => $value) {
                   				$total_question = $this->db->where('test_type_id', $value['id'])->count_all_results('question');
                                if($value['rules'] != 'opened'){
                                    $check = $this->db->where('applicant_id', $vacancy['id'])
                                                          ->where('test_type_id', $value['rules'])
                                                          ->where('status', 2)
                                                          ->get('test_transaction')->num_rows();
                                }else{
                                    $check = 1;
                                }
                                echo '<div class="card share full-width">';
                                    if($value['status_register']){
                                        if($value['status_register'] == 1){
                                            if($check > 0){
                                                echo '<button class="btn btn-danger btn-sm pull-right apply-test" style="margin-top:13px;margin-right:10px;" data-id="'.$value['id'].'">Continue</button>';
                                            }
                                        }else{
                                            if($check > 0){
                                                echo '<label class="label label-success pull-right" style="font-size:20px;margin-top:15px;margin-right:10px;"><i class="fa fa-check"></i></label>';
                                            }
                                        }
                                    }else{
                                        if($check > 0){
                                            echo '<button class="btn btn-danger btn-sm pull-right apply-test" style="margin-top:13px;margin-right:10px;" data-id="'.$value['id'].'">Apply</button>';
                                        }
                                    }
                                    echo '<div class="card-header clearfix" style="cursor: default;">';
                                        echo '<h5>'.$value['name'].'</h5>';
                                        echo '<h6>'.$total_question.' pertanyaan dalam '.$value['time'].' menit</h6>';
                                    echo '</div>';
                                    echo '<div class="card-description">';
                                        echo ''.($value['description'] ? $value['description'] : '').'';
                                    echo '</div>';
                                echo '</div>';
               				}
               			}
                   		?>
                   </div>
               </div>
           </div>
       </div>
   </div>
</div>

<div class="modal fade fill-in" id="modal-instructions" tabindex="-1" role="dialog" aria-hidden="true">
  	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close"></i></button>
  	<div class="modal-dialog" style="">
      	<div class="modal-content">
            <div class="modal-header">
            	<h5 class="text-left"><span class="bold">Instruksi Tes</span></h5>
            </div>
            <div class="modal-body" style="padding:2px;">
               	<div class="panel panel-default">
                  	<div class="panel-body">
                     	<h5 class="font-arial" style="font-size: 16px;">
                        	<ul style="">
                        	<li style="padding-bottom: 10px;">Anda harus memastikan internet cukup baik dan tidak membuka banyak file yang berat yang dapat menghambat berjalannya tes</li>
                        	<li style="padding-bottom: 10px;">Anda dipersilahkan untuk menyiapkan kertas kosong dan alat tulis karena di salah satu tes memerlukannya 			untuk mencorat-coret</li>
                    		<li style="padding-bottom: 10px;">setiap tes memiliki waktu pengerjaan dan petunjuknya masing-masing</li>
                        	</ul>
                     	</h5>
                  	</div>
               	</div>
    	 	</div>
      	</div>
  	</div>
</div>

<script type="text/javascript">
	$(function () {
 	
 		'use restrict';

 		$(this).on('click', '.apply-test', function (e){
	   		var id = $(this).data('id');
      		var now = new Date();
      		var year    = now.getUTCFullYear();
	      	var month   = now.getUTCMonth() + 1; 
	      	var day     = now.getUTCDate();
	      	var hour    = now.getUTCHours();
     	 	var minute  = (now.getUTCMinutes()<10?'0':'') + now.getUTCMinutes();
     	 	var second  = now.getUTCSeconds(); 
	      	var time_now_js = year+'-'+month+'-'+day+' '+hour+':'+minute+':'+second;

      		ajaxManager.addReq({
         		url: site_url + 'main/registration_test',
         		type: 'POST',
         		dataType: 'JSON',
         		data: {
            		id: id,
            		time_now_js: time_now_js,
            		now: now
         		},
	         	error: function (jqXHR, status, errorThrown) {
	            	error_handle(jqXHR, status, errorThrown);
	         	},
	         	success: function(r) {
	            	if(r.success) {
            		 	window.location.href = site_url+'mulai_test?id='+id+'';
	            	}else{
	               		alert(r.msg);
	            	}
	         	}
      		});
      		e.preventDefault(); 		
 		});

	});
</script>