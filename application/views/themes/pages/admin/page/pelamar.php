<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <table id="dt-table">
                    <thead>
                        <th></th>
                        <th>Nama</th>
                        <th>Tingkat</th>
                        <th>Tgl Lahir</th>
                        <th>Status</th>
                        <th>Lowongan</th>
                        <th>CV</th>
                        <th>Tgl Daftar</th>
                        <th></th>
                    </thead>
                    <tbody class="data-pelamar">
                        <?php
                        foreach ($datapelamar as $k => $v) {
                            echo '<tr>';
                            echo '<td><img src="'.base_url().'files/pelamar_pasphoto/'.$v['pas_photo']['applicant_file'].'" width="50" height="50"></td>';
                            echo '<td>'.$v['name'].'<br><span style="font-size:10px">'.$v['email'].'</span></td>';
                            echo '<td>'.$v['education_degree'].'</td>';
                            echo '<td>'.date('d M Y', strtotime($v['birth_date'])).'<br><span style="font-size:10px">Umur : '.$v['age'].'</span></td>';
                            echo '<td>'.($v['active'] == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>').'</td>';
                            echo '<td>'.$v['vacancy_name'].'</td>';
                            echo '<td><a href="'.base_url().'files/cv/'.$v['cv']['applicant_file'].'" target="_blank"><i class="fa fa-file"></i></a></td>';
                            echo '<td>'.date('d M Y', strtotime($v['created_date'])).'</td>';
                            echo '<td>';
                                echo '<div class="btn-group">';
                                    if($v['active'] == 1){
                                        echo '<a class="btn btn-default btn-xs" href="'.site_url().'pelamar/status/'.$v['user_id'].'/0">Set Inactive</a>'; 
                                    }else{
                                        echo '<a class="btn btn-default btn-xs" href="'.site_url().'pelamar/status/'.$v['user_id'].'/1">Set Active</a>';
                                    }
                                    echo '<a class="btn btn-default btn-xs" href="'.site_url().'pelamar/hapus/'.$v['user_id'].'/'.$v['applicant_id'].'">Delete</a>';
                                echo '</div>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    

    $(function(){

        $('#dt-table').DataTable();

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>