<link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/fancybox/jquery.fancybox.min.css">
<style type="text/css">
    .radio label, .checkbox label{
        margin-right: 0;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
 	<div class="col-md-12">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <form method="post" action="<?php echo site_url()?>pertanyaan/update" enctype="multipart/form-data">
                    <input type="hidden" name="id" value="<?php echo $pertanyaan['id'] ?>">
                    <input type="hidden" name="test_type_id" value="<?php echo $test_type_id ?>">
                    <input type="hidden" name="question_type_id" value="<?php echo $pertanyaan['question_type_id'] ?>">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Pertanyaan untuk test <span class="text-danger">*</span></label>
                                <select class="form-control" name="type_test" />
                                    <?php
                                    if($test){
                                        foreach ($test as $v) {
                                            if($pertanyaan['test_type_id'] == $v['id']){
                                                echo '<option value="'.$v['id'].'" selected>'.$v['name'].'</option>';
                                            }else{
                                                echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                                            }
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tambahkan gambar untuk pertanyaan</label>
                                <input type="file" name="question_image" class="form-control">
                                <input type="hidden" name="image_before" value="<?php echo $pertanyaan['image'] ?>">
                                <input type="hidden" name="image_id" value="<?php echo $pertanyaan['image_id'] ?>">
                                <?php 
                                if($pertanyaan['image']){
                                    echo '<h6 class="font-arial">';
                                    echo '<a data-fancybox="gallery" href="'.base_url().'files/pertanyaan_gambar/'.$pertanyaan['image'].'">';
                                    echo '<i class="fa fa-image"></i> '.$pertanyaan['temp_name_image'].'';
                                    echo '</a>';
                                    echo '</h6>';
                                }
                                ?>
                            </div>
                            <div class="form-group">
                                <label>Tambahkan file untuk pertanyaan</label>
                                <input type="file" name="question_file" class="form-control">
                                <input type="hidden" name="file_before" value="<?php echo $pertanyaan['file'] ?>">
                                <input type="hidden" name="file_id" value="<?php echo $pertanyaan['file_id'] ?>">
                                <?php 
                                if($pertanyaan['file']){
                                    echo '<h6 class="font-arial">';
                                    echo '<a href="'.base_url().'files/pertanyaan_file/'.$pertanyaan['file'].'">';
                                    echo ''.$pertanyaan['temp_name_file'].'';
                                    echo '</a>';
                                    echo '</h6>';
                                }
                                ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Deskripsi Pertanyaan</label>
                                <textarea class="form-control" name="description"><?php echo $pertanyaan['question_text'] ?></textarea>
                            </div>
                        </div>
                    </div>
                    <?php 
                    if($pertanyaan['question_type_id'] == 1 || $pertanyaan['question_type_id'] == 4){
                        $jawaban = $this->db->where('question_id', $pertanyaan['id'])->get('question_answers')->result_array();
                        $total = count($jawaban);

                        if($pertanyaan['question_type_id'] == 1){
                            echo '<div id="section-multiple-choice">';
                                echo '<hr>';
                                echo '<div class="row">';
                                    echo '<div class="col-md-12">';
                                        echo '<a href="" class="add-answers-multiple-choice">';
                                            echo '<p><i class="fa fa-plus"></i> Tambah Jawaban Lainnya</p>';
                                        echo '</a>';
                                        echo '<div class="multiple-choice">';
                                            if($jawaban){
                                                $i = 0;
                                                foreach ($jawaban as $key => $value) {
                                                    echo '<div class="input-group" style="margin-bottom:10px;">';
                                                        echo '<div class="input-group-btn">';
                                                            echo '<label class="btn btn-default btn-sm" style="height:35px;">';
                                                                echo '<div class="radio radio-danger" style="margin:0;padding:0">';
                                                                    echo '<input name="correct" value="'.$key.'" type="radio" id="cr-'.$key.'" class="choose-correct-mchoice" '.($value['correct_answers'] == 1 ? 'checked' : '').'>';
                                                                    echo '<label for="cr-'.$key.'" style="margin:0;padding:0">Jawaban Benar</label>';
                                                                echo '</div>';
                                                            echo '</label>';
                                                        echo '</div>';
                                                        echo '<input type="text" class="form-control" name="ans_mchoice['.$key.'][result]" value="'.$value['question_answers'].'">';
                                                        echo '<input type="hidden" name="ans_mchoice['.$key.'][correct]" class="ans-mchoice" value="'.($value['correct_answers'] == 1 ? 1 : '').'">';
                                                        echo '<input type="hidden" name="ans_mchoice['.$key.'][updated]" value="'.$value['id'].'">';
                                                        if($i >= 2){
                                                            echo '<div class="input-group-btn">';
                                                                echo '<button type="button" class="btn btn-danger btn-remove-answers-db" data-id="'.$key.'" data-type="'.$pertanyaan['question_type_id'].'"><i class="fa fa-trash"></i></button>';
                                                            echo '</div>';
                                                        }
                                                    echo '</div>';
                                                    $i++;
                                                }
                                            }
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        }
                        if($pertanyaan['question_type_id'] == 4){
                            echo '<div id="section-multiple-answers">';
                                echo '<hr>';
                                echo '<div class="row">';
                                    echo '<div class="col-md-12">';
                                        echo '<a href="" class="add-answers-multiple-answers">';
                                            echo '<p><i class="fa fa-plus"></i> Tambah Jawaban Lainnya</p>';
                                        echo '</a>';
                                        echo '<div class="multiple-answers">';
                                            $jawaban = $this->db->where('question_id', $pertanyaan['id'])->get('question_answers')->result_array();
                                            if($jawaban){
                                                $i = 0;
                                                foreach ($jawaban as $key => $value) {
                                                    echo '<div class="input-group" style="margin-bottom:10px;">';
                                                        echo '<div class="input-group-btn">';
                                                            echo '<label class="btn btn-default btn-sm" style="height:35px;">';
                                                                echo '<input type="checkbox" name="correct[]" value="'.$key.'" class="choose-correct-manswers" '.($value['correct_answers'] == 1 ? 'checked' : '').'>&nbsp;'; 
                                                                echo '<label style="font-weight: normal;font-size:13px;">Jawaban Benar</label>';
                                                            echo '</label>';
                                                        echo '</div>';
                                                        echo '<input type="text" class="form-control" name="ans_manswers['.$key.'][result]" value="'.$value['question_answers'].'">';
                                                        echo '<input type="hidden" name="ans_manswers['.$key.'][correct]" class="ans-manswers" value="'.($value['correct_answers'] == 1 ? 1 : '').'">';
                                                        echo '<input type="hidden" name="ans_manswers['.$key.'][updated]" value="'.$value['id'].'">';

                                                        if($i >= 2){
                                                            echo '<div class="input-group-btn">';
                                                                echo '<button type="button" class="btn btn-danger btn-remove-answers-db" data-id="'.$key.'" data-type="'.$pertanyaan['question_type_id'].'"><i class="fa fa-trash"></i></button>';
                                                            echo '</div>';
                                                        }
                                                    echo '</div>';
                                                    $i++;
                                                }
                                            }
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        }
                    }else{
                        $total = 0;
                    }
                    ?>
                    <a href="<?php echo site_url('pertanyaan') ?>" class="btn btn-default btn-block">Kembali</a>
                    <button type="submit" class="btn btn-info btn-block">Simpan</button>
                </form>
      		</div>
   		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>
<script src="<?php echo base_url() ?>assets/plugins/fancybox/jquery.fancybox.min.js"></script>

<script type="text/javascript">
    total = '<?php echo $total ?>';
    $(function () {

        key_correct_answers = parseInt(total);

        tinymce.init({
            selector: 'textarea',
            menubar: false,
            height: '180px',
            style: "border:5px solid black;",
            mobile: { theme: 'mobile' }
        });

        $('.add-answers-multiple-choice').on('click', function (e) {
            var mode = $(this).data('mode');
            key_correct_answers += 1;
            ans = '';
            ans += '<div class="input-group" style="margin-bottom:10px;">';
                ans += '<div class="input-group-btn">';
                    ans += '<label class="btn btn-default btn-sm" style="height:35px;">';
                        ans += '<div class="radio radio-danger" style="margin:0;padding:0">'
                            ans += '<input name="correct" value="'+key_correct_answers+'" type="radio" class="choose-correct-mchoice">';
                            ans += '<label for="cr-'+key_correct_answers+'" style="margin:0;padding:0">Jawaban Benar</label>';
                        ans += '</div>';
                    ans += '</label>';
                ans += '</div>';
                ans += '<input type="text" class="form-control" name="ans_mchoice['+key_correct_answers+'][result]" placeholder="Jawaban...">';
                ans += '<input type="hidden" name="ans_mchoice['+key_correct_answers+'][correct]" class="ans-mchoice">';
                ans += '<div class="input-group-btn">';
                    ans += '<button type="button" class="btn btn-danger btn-remove-answers"><i class="fa fa-trash"></i></button>';
                ans += '</div>';
            ans += '</div>';
            $('.multiple-choice').append(ans);
            e.preventDefault();

            $('.btn-remove-answers').on('click', function (e) {
                $(this).parents('.input-group').remove();
                e.preventDefault();
            });

            $(this).on('change', '.choose-correct-mchoice', function (e) {
                var key = $(this).val();
                $('.ans-mchoice').val('');
                $('input[name="ans_mchoice['+key+'][correct]"]').val(1);
            });
        });

        $('.add-answers-multiple-answers').on('click', function (e) {
            var mode = $(this).data('mode');
            key_correct_answers += 1;
            ans = '';
            ans += '<div class="input-group" style="margin-bottom:10px;">';
                ans += '<div class="input-group-btn">';
                    ans += '<label class="btn btn-default btn-sm" style="height:35px;">';
                        ans += '<input type="checkbox" name="correct['+key_correct_answers+']" value="'+key_correct_answers+'" class="choose-correct-manswers">&nbsp;'; 
                        ans += '<label style="font-weight: normal;font-size:13px;">Jawaban Benar</label>';
                    ans += '</label>';
                ans += '</div>';
                ans += '<input type="text" class="form-control" name="ans_manswers['+key_correct_answers+'][result]" placeholder="Jawaban...">';
                ans += '<input type="hidden" name="ans_manswers['+key_correct_answers+'][correct]" class="ans-manswers">';
                ans += '<div class="input-group-btn">';
                    ans += '<button type="button" class="btn btn-danger btn-remove-answers"><i class="fa fa-trash"></i></button>';
                ans += '</div>';
            ans += '</div>';
            $('.multiple-answers').append(ans);
            e.preventDefault();

            $('.btn-remove-answers').on('click', function (e) {
                $(this).parents('.input-group').remove();
                e.preventDefault();
            });

            $(this).on('change', '.choose-correct-manswers', function (e) {
                var key = $(this).val();
                if(this.checked) {
                    $('input[name="ans_manswers['+key+'][correct]"]').val(1);
                }else{
                    $('input[name="ans_manswers['+key+'][correct]"]').val('');
                }
            });

        });

        $(this).on('change', '.choose-correct-mchoice', function (e) {
            var key = $(this).val();
            $('.ans-mchoice').val('');
            $('input[name="ans_mchoice['+key+'][correct]"]').val(1);
        });

        $(this).on('change', '.choose-correct-manswers', function (e) {
            var key = $(this).val();
            if(this.checked) {
                $('input[name="ans_manswers['+key+'][correct]"]').val(1);
            }else{
                $('input[name="ans_manswers['+key+'][correct]"]').val('');
            }
        });

        $('.btn-remove-answers-db').on('click', function (e) {
            e.preventDefault();
            var key = $(this).data('id');
            var type = $(this).data('type');
            if(type == 1){
                var id = $('input[name="ans_mchoice['+key+'][updated]"]').val();
            }
            if(type == 4){
                var id = $('input[name="ans_manswers['+key+'][updated]"]').val();
            }
            ajaxManager.addReq({
                url: site_url + 'pertanyaan/delete_jawaban',
                type: 'GET',
                dataType: 'JSON',
                data: {
                    id: id
                },
                error: function (jqXHR, status, errorThrown) {
                    error_handle(jqXHR, status, errorThrown);
                },
                success: function(r) {
                    if(r.success){
                        $('.btn-remove-answers-db[data-id="'+key+'"]').parents('.input-group').remove();
                    }else{
                        alert('gagal menghapus jawaban');
                    }
                }
            });
        });
    });
</script>