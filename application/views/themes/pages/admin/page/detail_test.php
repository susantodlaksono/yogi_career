<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
 	<div class="col-md-8">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
                 <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
         		<form method="post" action="<?php echo site_url()?>manage_test/update">
                    <input type="hidden" name="id" value="<?php echo $test['id'] ?>">
         			<div class="form-group">
                      	<label class="text-info">Nama Test <span class="text-danger">*</span></label>
                      	<input type="text" class="form-control input-sm" name="name" value="<?php echo $test['name'] ?>" />
                   </div>
                   <div class="form-group">
                      	<label>Jawaban Lengkap <span class="text-danger">*</span></label>
                      	<select class="form-control input-sm" name="mode_test" />
                         	<option value="1" <?php echo $test['mode_test'] == 1 ? 'selected=""' : ''?>>Ya</option>
                         	<option value="2" <?php echo $test['mode_test'] == 2 ? 'selected=""' : ''?>>Tidak</option>
                      	</select>
                   </div>
                   <div class="form-group">
                      	<label class="text-info">Total Pertanyaan <span class="text-danger">*</span></label>
                      	<input type="text" class="form-control input-sm" name="total_question" value="<?php echo $test['total_question'] ?>" />
                   </div>
                   <div class="form-group">
                  		<label>Waktu <span class="text-danger">*</span></label>
                  		<div class="input-group">
                  			<input type="number" class="form-control" name="times" value="<?php echo $test['time'] ?>">
                  			<span class="input-group-addon default">
                                Menit
                            </span>
              			</div>
                	</div>
                	<div class="form-group">
                      	<label>Status <span class="text-danger">*</span></label>
                      	<select class="form-control input-sm" name="status" />
                         	<option value="1" <?php echo $test['status'] == 1 ? 'selected=""' : ''?>>Active</option>
                         	<option value="0" <?php echo $test['status'] == NULL ? 'selected=""' : ''?>>Inactive</option>
                      	</select>
                   </div>
                	<div class="form-group">
						<label>Instruksi Test</label>
						<textarea class="form-control" name="guidetest"><?php echo $test['description'] ?></textarea>
					</div>
                   <a href="<?php echo site_url('manage_test') ?>" class="btn btn-default btn-block">Kembali</a>
                   <button type="submit" class="btn btn-info btn-block">Update</button>
         		</form>
      		</div>
   		</div>
	</div>
</div>

<script src="<?php echo base_url('assets/plugins/tinymce/tinymce.min.js') ?>"></script>

<script type="text/javascript">
	$(function () {
		tinymce.init({
	        selector: 'textarea',
	        menubar: false,
	        style: "border:5px solid black;",
	        mobile: { theme: 'mobile' }
	    });
	});
</script>