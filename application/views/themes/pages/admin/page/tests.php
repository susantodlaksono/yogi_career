<div class="row">
    <div class="col-md-10">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
    <div class="col-md-2">
        <div class="btn-group pull-right" style="margin-top:10px;">
            <a href="<?php echo site_url() ?>manage_test/tambah" class="btn btn-default btn-sm">
                <i class="fa fa-plus"></i> Tambah
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <table id="dt-table">
                    <thead>
                        <th>Nama</th>
                        <th>Jawaban Lengkap</th>
                        <th>Total Pertanyaan</th>
                        <th>Waktu</th>
                        <th>Status</th>
                        <th></th>
                    </thead>
                    <tbody class="data-pelamar">
                        <?php
                        foreach ($test as $k => $v) {
                            echo '<tr>';
                            echo '<td>'.$v['name'].'</td>';
                            echo '<td>'.($v['mode_test'] == 1 ? '<span class="label label-success">Ya</span>' : '<span class="label label-danger">Tidak</span>').'</td>';
                            echo '<td>'.$v['total_question'].' Soal</td>';
                            echo '<td>'.$v['time'].' Menit</td>';
                            echo '<td>'.($v['status'] == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>').'</td>';
                            echo '<td>';
                                echo '<div class="btn-group">';
                                    if($v['status'] == 1){
                                        echo '<a class="btn btn-default btn-xs" href="'.site_url().'manage_test/status/'.$v['id'].'/0">Set Inactive</a>'; 
                                    }else{
                                        echo '<a class="btn btn-default btn-xs" href="'.site_url().'manage_test/status/'.$v['id'].'/1">Set Active</a>';
                                    }
                                    echo '<a class="btn btn-default btn-xs" href="'.site_url().'manage_test/detail/'.$v['id'].'">Edit</a>';
                                    echo '<a class="btn btn-default btn-xs" href="'.site_url().'manage_test/hapus/'.$v['id'].'">Delete</a>';
                                echo '</div>';
                            echo '</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    

    $(function(){

        $('#dt-table').DataTable();

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>