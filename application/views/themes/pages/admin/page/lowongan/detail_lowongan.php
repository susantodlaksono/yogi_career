<div class="row">
    <div class="col-md-10">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
    <div class="col-md-2">
        <div class="btn-group pull-right" style="margin-top:10px;">
            <a href="<?php echo site_url() ?>lowongan" class="btn btn-default btn-sm">
                Kembali
            </a>
        </div>
    </div>
</div>
<?php
if($this->session->userdata('notif') != ''){
    echo '<div class="alert alert-info" role="alert">';
        echo '<button class="close" data-dismiss="alert"></button>';
        echo $this->session->userdata('notif');
    echo '</div>';
    $this->session->sess_destroy();
}
?>
<div class="row">
 	<div class="col-md-4">
     	<div class="panel panel-default">
     		<div class="panel-heading">
            	<div class="panel-title">Detail</div>
          	</div>
         	<div class="panel-body" style="margin-top: 10px;">
         		<form method="post" action="<?php echo site_url()?>lowongan/update">
                    <input type="hidden" name="id" value="<?php echo $lowongan['id'] ?>">
         			<div class="form-group">
                      	<label class="text-info">Nama Lowongan</label>
                      	<input type="text" class="form-control input-sm" name="name" value="<?php echo $lowongan['name'] ?>" />
                   </div>
                   <div class="form-group">
                      	<label>Status</label>
                      	<select class="form-control input-sm" name="status" />
                         	<option value="1" <?php echo $lowongan['status'] == 1 ? 'selected=""' : ''?>>Active</option>
                         	<option value="0" <?php echo $lowongan['status'] == 0 ? 'selected=""' : ''?>>Inactive</option>
                      	</select>
                   </div>
                   <button type="submit" class="btn btn-info btn-block">Update</button>
         		</form>
      		</div>
   		</div>
	</div>
	<div class="col-md-8">
     	<div class="panel panel-default">
     		<div class="panel-heading">
            	<div class="panel-title">Konfigurasi Test</div>
          	</div>
         	<div class="panel-body" style="margin-top: 10px;">
         		<ul class="nav nav-tabs nav-tabs-linetriangle">
         			<li class="active">
         				<a style="font-weight: bold;" data-toggle="tab" href="#basic">
         					<span>Daftar Test</span>
         				</a>
         			</li>
         			<li>
         				<a style="font-weight: bold;" data-toggle="tab" href="#config">
         					<span>Aturan Test</span>
         				</a>
         			</li>
     			</ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="basic">
                        <form method="post" action="<?php echo site_url()?>lowongan/tambah_test">
                        <input type="hidden" name="id" value="<?php echo $lowongan['id'] ?>">
                        <div class="row">
                            <div class="col-md-10">
                                <select class="form-control input-sm" name="test_id">
                                    <?php
                                    if($test_type_list){
                                        foreach ($test_type_list as $v) {
                                            echo '<option value="'.$v['id'].'">'.$v['name'].'</option>';
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-default btn-block">
                                    Tambah
                                </button>
                            </div>
                        </div>
                        </form>
                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <table id="table-basic">
                                    <thead>
                                        <th>Nama Test</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                        <?php 
                                        if($group_test){
                                            foreach ($group_test as $v) {
                                                echo '<tr>';
                                                echo '<td>'.$v['name'].'</td>';
                                                echo '<td>';
                                                echo '<div class="btn-group">';
                                                    echo '<button role="group" class="btn btn-danger btn-xs btn-remove-test" data-id="184" data-test-type-id="3" style="margin-top: -10px">';
                                                    echo '<i class="fa fa-trash"></i>';
                                                    echo '</button>';
                                                echo '</div>';
                                                echo '</td>';
                                                echo '</tr>';
                                            }
                                        }else{
                                            echo '<tr><td colspan="2" class="text-center">Data tidak ditemukan</td></tr>';
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="config">
                    </div>
                </div>
     		</div>
 		</div>
	</div>	
</div>

<script type="text/javascript">
    

    $(function(){

        $('#table-basic').DataTable();

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>