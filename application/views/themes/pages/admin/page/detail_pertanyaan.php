<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
	<div class="col-md-12">
     	<div class="panel panel-default">
         	<div class="panel-body" style="margin-top: 10px;">
         		<ul class="nav nav-tabs nav-tabs-linetriangle">
                    <?php
                    if($tipe_pertanyaan){
                        $i = 0;
                        foreach ($tipe_pertanyaan as $v) {
                            echo '<li class="'.($i == 0 ? 'active' : '').'">';
                                echo '<a style="font-weight: bold;" data-toggle="tab" href="#tab-'.$v['question_type_id'].'">';
                                    if($v['question_type_id'] == 1){
                                        echo '<span>Multiple Choice</span>';
                                    }
                                    if($v['question_type_id'] == 2){
                                        echo '<span>File Upload</span>';
                                    }
                                    if($v['question_type_id'] == 1){
                                        echo '<span>Jawaban Deskripsi</span>';
                                    }
                                    if($v['question_type_id'] == 4){
                                        echo '<span>Jawaban Ganda</span>';
                                    }   
                                    
                                echo '</a>';
                            echo '</li>';
                            
                            $i++;
                        }
                    }
                    ?>
         			
         			<li>
         				<a style="font-weight: bold;" data-toggle="tab" href="#config">
         					<span>Aturan Test</span>
         				</a>
         			</li>
     			</ul>
     		</div>
 		</div>
	</div>	
</div>