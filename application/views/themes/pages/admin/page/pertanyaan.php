<div class="row">
    <div class="col-md-10">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
    <div class="col-md-2">
        <div class="btn-group pull-right" style="margin-top:10px;">
            <a href="<?php echo site_url() ?>pertanyaan/tambah" class="btn btn-default btn-sm">
                <i class="fa fa-plus"></i> Tambah
            </a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body" style="margin-top: 10px;">
                <?php
                if($this->session->userdata('notif') != ''){
                    echo '<div class="alert alert-info" role="alert">';
                        echo '<button class="close" data-dismiss="alert"></button>';
                        echo $this->session->userdata('notif');
                    echo '</div>';
                    $this->session->sess_destroy();
                }
                ?>
                <table id="dt-table">
                    <thead>
                        <th>Nama Test</th>
                        <th>Jawaban Lengkap</th>
                        <th>Total Pertanyaan</th>
                        <th>Waktu</th>
                        <th></th>
                    </thead>
                    <tbody class="data-pelamar">
                        <?php
                        if($test){
                            foreach ($test as $k => $v) {
                                echo '<tr>';
                                echo '<td>'.$v['name'].'</td>';
                                echo '<td>'.($v['mode_test'] == 1 ? '<span class="label label-success">Ya</span>' : '<span class="label label-danger">Tidak</span>').'</td>';
                                echo '<td>'.$v['total_pertanyaan'].' Soal</td>';
                                echo '<td>'.$v['time'].' Menit</td>';
                                echo '<td>';
                                    echo '<div class="btn-group">';
                                        echo '<a class="btn btn-default btn-xs" href="'.site_url().'pertanyaan/detail/'.$v['id'].'">Detail</a>';
                                    echo '</div>';
                                echo '</td>';
                                echo '</tr>';
                            }
                        }else{
                            echo '<tr><td colspan="10">Tidak ditemukan</td></tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    

    $(function(){

        $('#dt-table').DataTable();

        $(".alert-info").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-info").slideUp(500);
        });

        // $(this).getting();
    });

</script>