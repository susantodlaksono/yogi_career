<div class="row">
    <div class="col-md-12">
        <h4 class="font-arial bold"><?php echo $title ?></h4>   
    </div>
</div>
<div class="row">
	<div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<div class="panel-title">Summary Pelamar</div>
          	</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 text-center" style="border-bottom:1px solid #ededed;">
                	 	<h1 class="bold no-margin total-applicant"><?php echo $pelamar ?></h1>
                        <h5 class="bold no-margin ">Total Pelamar</h5>
                    </div>
                    <div class="col-md-12 text-center" style="border-bottom:1px solid #ededed;">
                	 	<h1 class="bold text-success no-margin total-applicant"><?php echo $active ?></h1>
                        <h5 class="bold text-success no-margin ">Aktif</h5>
                    </div>
                    <div class="col-md-12 text-center">
                	 	<h1 class="bold text-danger no-margin total-applicant"><?php echo $inactive ?></h1>
                        <h5 class="bold text-danger no-margin ">Nonaktif</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-5">
        <div class="panel panel-default">
            <div class="panel-heading">
            	<div class="panel-title">Summary Berdasarkan Lowongan</div>
          	</div>
            <div class="panel-body" style="margin-top:10px">
                <div class="row table-responsive">
                	<div class="col-md-12">
	                	<table class="">
	                		<thead>
	                			<th>Lowongan</th>
	                			<th>Total</th>
	                		</thead>
	                		<tbody>
	                			<?php
	                			if($vacancy){
	                				foreach ($vacancy as $v) {
	                					echo '<tr>';
	                						echo '<td>'.$v['vacancy_name'].'</td>';
	                						echo '<td>'.$v['total'].'</td>';
	                					echo '</tr>';
	                				}
	                			}
	                			?>
	                		</tbody>
	                	</table>
                	</div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
    	<div class="row">
    		<div class="col-md-12">
    			<div class="panel panel-default">
		            <div class="panel-heading">
		            	<div class="panel-title">Summary Berdasarkan Tingkat Pendidikan</div>
		          	</div>
		            <div class="panel-body" style="margin-top:10px">
		                <div class="row table-responsive">
		                	<div class="col-md-12">
			                	<table class="">
			                		<thead>
			                			<th>Tingkat</th>
			                			<th>Total</th>
			                		</thead>
			                		<tbody>
			                			<?php
			                			if($tingkat){
			                				foreach ($tingkat as $v) {
			                					echo '<tr>';
			                						echo '<td>'.$v['education_degree'].'</td>';
			                						echo '<td>'.$v['total'].'</td>';
			                					echo '</tr>';
			                				}
			                			}
			                			?>
			                		</tbody>
			                	</table>
		                	</div>
		                </div>
		            </div>
		        </div>
			</div>
			<div class="col-md-12">
				<div class="panel panel-default">
		            <div class="panel-heading">
		            	<div class="panel-title">Summary Berdasarkan Kelamin</div>
		          	</div>
		            <div class="panel-body" style="margin-top:10px">
		                <div class="row table-responsive">
		                	<div class="col-md-12">
			                	<table class="">
			                		<thead>
			                			<th>Kelamin</th>
			                			<th>Total</th>
			                		</thead>
			                		<tbody>
			                			<?php
			                			if($kelamin){
			                				foreach ($kelamin as $v) {
			                					echo '<tr>';
			                						echo '<td>'.($v['gender'] == 'L' ? 'Laki-laki' : 'Perempuan').'</td>';
			                						echo '<td>'.$v['total'].'</td>';
			                					echo '</tr>';
			                				}
			                			}
			                			?>
			                		</tbody>
			                	</table>
		                	</div>
		                </div>
		            </div>
		        </div>
			</div>
		</div>
    </div>
</div>