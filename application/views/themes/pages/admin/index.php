<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="<?php echo config_item('app_desc');?>" />
        <meta name="author" content="<?php echo config_item('app_author')?>" />
        <link href="<?php echo base_url('logo.png'); ?>" rel="shortcut icon"/>
        <title>CARRIER</title>
        
        <link class="main-stylesheet" href="<?php echo base_url('assets/pages/css/pages.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/loading.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/pace/pace-theme-flash.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/bootstrapv3/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url('assets/plugins/switchery/css/switchery.min.css'); ?>" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?php echo base_url('assets/pages/css/pages-icons.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/plugins/simplepagination/simplePagination.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/plugins/daterangepicker/css/daterangepicker.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url('assets/plugins/select2/css/select2.min.css') ?>" rel="stylesheet" type="text/css" >
        <link href="<?php echo base_url('assets/plugins/font-awesome-5.0.1/css/fontawesome-all.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/snackbar/css/snackbar.min.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/snackbar/css/material.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url('assets/plugins/datatable/dataTables.min.css'); ?>" rel="stylesheet" type="text/css" />        
	    <script src="<?php echo base_url('assets/plugins/jquery/jquery-1.11.1.min.js') ?>" type="text/javascript"></script>        
        <style type="text/css">
            .btn-danger{
                background-color: #f11818;
            }
            .text-danger{
                color: #f11818 !important;
            }
            .bg-danger{
                background-color: #f11818;
            }
            table{
                width: 100%;
            }
            tr{
                border-bottom:1px solid #d2d2d2;
            }
            td{
                padding:5px;
            }
            .dataTables_wrapper .dataTables_info{
                padding: 0;
                padding-top: 10px;
            }
            .dataTables_wrapper .dataTables_paginate{
                padding: 0;
            }
            .dataTables_wrapper .dataTables_info, .dataTables_wrapper .dataTables_paginate{
                margin-top: 10px !important;
            }
            .dataTables_wrapper .dataTables_paginate .paginate_button{
                padding:1px;
            }
            table.dataTable thead th, table.dataTable thead td{
                padding: 10px 9px;
            }
        </style>
    </head>
	<body class="fixed-header windows menu-behind desktop pace-done horizontal-menu">
		<!-- <div class="loading"></div> -->
        <nav class="page-sidebar" data-pages="sidebar">
            <div class="sidebar-header">
                <img src="<?php echo base_url('logo.png') ?>" alt="logo" class="brand" data-src="<?php echo base_url('logo.png') ?>" width="78" height="22">
                <div class="sidebar-header-controls">
                  <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu">
                        <i class="fa fa-angle-down fs-16"></i>
                  </button>
                  <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar">
                        <i class="fa fs-12"></i>
                  </button>
                </div>
            </div>
            <div class="sidebar-menu">
                <ul class="menu-items">
                    <li class="m-t-30">
                        <a href="<?php echo site_url() ?>main">
                            <span class="title bold" style="width:90%;overflow:unset;font-size: 12px;text-transform:uppercase;">Dashboard</span>
                        </a>
                        <span class="icon-thumbnail bg-info-dark">
                            <i class="fa fa-home"></i>
                        </span>
                    </li>
                    <li>
                        <a href="<?php echo site_url() ?>pelamar">
                            <span class="title bold" style="width:90%;overflow:unset;font-size: 12px;text-transform:uppercase;">Pelamar</span>
                        </a>
                        <span class="icon-thumbnail bg-info-dark">
                            <i class="fa fa-user"></i>
                        </span>
                    </li>
                    <li>
                        <a href="<?php echo site_url() ?>manage_test">
                            <span class="title bold" style="width:90%;overflow:unset;font-size: 12px;text-transform:uppercase;">Daftar Test</span>
                        </a>
                        <span class="icon-thumbnail bg-info-dark">
                            <i class="fa fa-edit"></i>
                        </span>
                    </li>
                    <li>
                        <a href="<?php echo site_url() ?>lowongan">
                            <span class="title bold" style="width:90%;overflow:unset;font-size: 12px;text-transform:uppercase;">Lowongan</span>
                        </a>
                        <span class="icon-thumbnail bg-info-dark">
                            <i class="fa fa-tag"></i>
                        </span>
                    </li>
                    <li>
                        <a href="<?php echo site_url() ?>pertanyaan">
                            <span class="title bold" style="width:90%;overflow:unset;font-size: 12px;text-transform:uppercase;">Pertanyaan</span>
                        </a>
                        <span class="icon-thumbnail bg-info-dark">
                            <i class="fa fa-file"></i>
                        </span>
                    </li>
                </ul>
            </div>
        </nav>
		<div class="page-container">
			<div class="header" style="background-color:#f11818">
                <div class="container-fluid relative">
                    <div class="pull-left full-height visible-sm visible-xs">
                        <div class="header-inner">
                            <a href="#" class="btn-link toggle-sidebar visible-sm-inline-block visible-xs-inline-block padding-5" data-toggle="sidebar">
                                <span class="icon-set menu-hambuger"></span>
                            </a>
                        </div>
                    </div>
                    <div class="pull-center hidden-md hidden-lg">
                        <div class="header-inner">
                            <div class="brand inline">
                                <h5 class="bold text-complete" style="padding: 5px;margin-top: 5px;font-size: 20px;letter-spacing: 0.1em;margin-left:6px;">
                                    CAREER
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div class="pull-right full-height visible-sm visible-xs">
                        <div class="header-inner">
                            <a href="#" class="btn-link visible-xs-inline-block visible-sm-inline-block m-r-10" data-pages="horizontal-menu-toggle">
                                <span class="pg pg-arrow_minimize"></span>
                            </a>
                            <a href="#" class="btn-link visible-sm-inline-block visible-xs-inline-block" data-toggle="quickview" data-toggle-element="#quickview">
                                <span class="icon-set menu-hambuger-plus"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class=" pull-left sm-table hidden-xs hidden-sm" style="margin-left: 15px;">
                    <div class="header-inner">
                        <div class="brand inline" style="width: auto;text-align: left;">
                            <img src="<?php echo base_url('logo.png') ?>" width="35" height="35" style="margin-top: 8px;margin-right: 7px;">
                            <h5 class="bold" style="float:right;font-size: 20px;margin-top: 8px;color: #fff;">CAREER</h5>
                        </div>
                    </div>
                </div>
                <div class="pull-right">
                    <div class="header-inner">
                        <a href="<?php echo site_url('security/logout') ?>" style="color:#fff;font-size: 16px;font-weight: bold;"><i class="fa fa-sign-out-alt"></i> Logout</a>
                    </div>
                </div>
            </div>
			<div class="page-container">
        		<div class="page-content-wrapper">
        			<div class="content">
			            <div class="container-fluid container-fixed-lg" id="container-content">
                            <?php
                            if (!defined('BASEPATH')) 
                                exit('No direct script access allowed');
                                if ($content) {
                                    $this->load->view($content);
                                }  
                            ?>         
                        </div>
		          	</div>
                </div>
    		</div>
		</div>
        
        <script src="<?php echo base_url('assets/setup.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/widget.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/datatable/jquery.dataTables.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/pace/pace.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/modernizr.custom.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-ui/jquery-ui.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/bootstrapv3/js/bootstrap.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery/jquery-easy.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-unveil/jquery.unveil.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-bez/jquery.bez.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-ios-list/jquery.ioslist.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-actual/jquery.actual.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js') ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('assets/plugins/classie/classie.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/switchery/js/switchery.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/pages/js/pages.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/js/scripts.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/js/timeline.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/simplepagination/jquery.simplePagination.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/momentjs/momentjs.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/daterangepicker/js/daterangepicker.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/jquery.form.min.js') ?>" type="text/javascript"></script>
        <script src="<?php echo base_url('assets/plugins/select2/js/select2.full.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/plugins/snackbar/js/snackbar.min.js') ?>"></script>
        

        <script>
		    var site_url = '<?php echo site_url(); ?>';
            var base_url = '<?php echo base_url(); ?>';
		    var loading = '<h5 class="font-arial" style="font-size:15px;"></h5>';
		</script>

	</body>
</html>