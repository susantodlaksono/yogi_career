-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 25, 2021 at 11:35 AM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 7.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `career_yogi`
--

-- --------------------------------------------------------

--
-- Table structure for table `applicant`
--

CREATE TABLE `applicant` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `birth_date` date DEFAULT NULL,
  `age` tinyint(5) DEFAULT NULL,
  `vacancy_division_id` int(11) NOT NULL,
  `gender` enum('L','P') DEFAULT NULL,
  `education_degree` varchar(100) DEFAULT NULL,
  `school_majors` varchar(100) DEFAULT NULL,
  `university` varchar(100) DEFAULT NULL,
  `contact_number` varchar(20) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `sc_twitter` varchar(250) DEFAULT NULL,
  `sc_facebook` varchar(250) DEFAULT NULL,
  `sc_instagram` varchar(250) DEFAULT NULL,
  `sc_portofolio` varchar(250) DEFAULT NULL,
  `sc_portofolio_file` text DEFAULT NULL,
  `sc_linkedin` varchar(250) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `level` tinyint(3) DEFAULT NULL,
  `notes` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant`
--

INSERT INTO `applicant` (`id`, `user_id`, `name`, `birth_date`, `age`, `vacancy_division_id`, `gender`, `education_degree`, `school_majors`, `university`, `contact_number`, `email`, `sc_twitter`, `sc_facebook`, `sc_instagram`, `sc_portofolio`, `sc_portofolio_file`, `sc_linkedin`, `created_date`, `status`, `level`, `notes`) VALUES
(2, 4, 'Susanto Dwi', '1986-11-01', 35, 1, 'L', 'S1', 'TI', 'Widyatama', '0869898922', 'susantodlaksono@gmail.com', 'santo.tw', 'santo.fb', 'santo.ig', '', NULL, 'santo.lk', '2021-11-10 19:56:26', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `applicant_file`
--

CREATE TABLE `applicant_file` (
  `id` int(11) NOT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `applicant_file` text DEFAULT NULL,
  `applicant_temp_file` text DEFAULT NULL,
  `applicant_type_file` tinyint(4) NOT NULL COMMENT '1 : CV | 2 : Pas Photo'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `applicant_file`
--

INSERT INTO `applicant_file` (`id`, `applicant_id`, `applicant_file`, `applicant_temp_file`, `applicant_type_file`) VALUES
(3, 2, '075625000000618bc179bff16.docx', 'Daftar clipers imm terbaru polda banten.docx', 1),
(4, 2, '075625000000618bc179c2043.jpg', 'CLEAN (4).jpg', 2);

-- --------------------------------------------------------

--
-- Table structure for table `evaluator_tests`
--

CREATE TABLE `evaluator_tests` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `test_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`, `date`) VALUES
(1, 'Admin', 'Administrator', '2016-05-22 21:48:44'),
(2, 'User', 'User/member', '2016-09-16 22:15:46'),
(3, 'Evaluator', 'Penilai test teknis', '2021-11-10 19:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(11) NOT NULL,
  `question_text` text DEFAULT NULL,
  `correct_answers_id` int(11) DEFAULT NULL,
  `correct_answers_multiple` text DEFAULT NULL,
  `question_description` text DEFAULT NULL,
  `test_type_id` int(11) NOT NULL,
  `question_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question_text`, `correct_answers_id`, `correct_answers_multiple`, `question_description`, `test_type_id`, `question_type_id`) VALUES
(30, '<p>Pertanyaan 2 2?</p>', NULL, NULL, NULL, 1, 4),
(35, '<p>Bacalah paragraf berikut dengan saksama!</p>\r\n<p>Sifat manusia ibarat padi yang terhampar di sawah yang luas. Ketika manusia itu meraih kepandaian, kebesaran, dan kekayaan, sifatnya akan menjadi rendah hati dan dermawan.</p>\r\n<p>Bergitu pula dengan padi yang semakin berisi, ia akan semakin merenduk. Apabila padi itu kosong, ia akan berdiri tegak. Demikian pula dengan manusia yang tak berilmu dan tak berperasaan, ia akan semakin sombong dan garang. Oleh karena itu kita sebagai manusia apabila diberi kepandaian dan kelebihan, bersikaplah seperti padi yang selalu menunduk.</p>\r\n<p>Hal yang dianalogikan pada paragraf di atas adalah &hellip;.</p>', NULL, NULL, NULL, 8, 1),
(36, '<p>Deret kata barisan tak bergenderang-berpalu pada barisan kedua bait kedua bermakna &hellip;.</p>', NULL, NULL, NULL, 8, 1),
(37, '<p>Membaca dengan mata bergerak cepat untuk melihat dan memperhatikan bahan tertulis untuk mencari serta mendapatkan informasi. Uraian tersebut merupakan pengertian membaca&hellip;.</p>', NULL, NULL, NULL, 8, 1),
(38, '<p style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; min-height: 24px; font-size: 18px; line-height: 24px; font-family: ProximaNova, Helvetica, Arial, sans-serif;\">1) Gerakan suatu benda dimana setiap titik pada benda tersebut mempunyai jarak yang tetap terhadap suatu sumbu tertentu, merupakan pengertian dari:</p>', NULL, NULL, NULL, 9, 1),
(39, '<p style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; min-height: 24px; font-size: 18px; line-height: 24px; font-family: ProximaNova, Helvetica, Arial, sans-serif;\">2) &nbsp;Sebuah balok bermassa 1,5 kg didorong ke atas oleh gaya konstan F = 15 N pada bidang miring seperti gambar. Anggap percepatan gravitasi (g) 10 ms-2 dan gesekan antara balok dan bidang miring nol. Usaha total yang dilakukan pada balok adalah ...</p>', NULL, NULL, NULL, 9, 1),
(40, '<p style=\"box-sizing: border-box; margin: 0px 0px 8px; padding: 0px; min-height: 24px; font-size: 18px; line-height: 24px; font-family: ProximaNova, Helvetica, Arial, sans-serif;\">3) Sebanyak 3 liter gas Argon bersuhu 270C pada tekanan 1 atm (1 atm = 105 Pa) berada di dalam tabung. Jika konstanta gas umum R = 8,314 J m-1K-1 dan banyaknya partikel dalam 1 mol gas 6,02 x 1023 partikel, maka banyak partikel gas Argon dalam tabung tersebut adalah ...</p>\r\n<p>&nbsp;</p>', NULL, NULL, NULL, 9, 1),
(41, '<p class=\"MsoListParagraphCxSpFirst\" style=\"text-indent: -18.0pt; mso-list: l0 level1 lfo1;\"><span lang=\"IN\"><span>1.<span style=\"font: 7.0pt \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span><span lang=\"IN\">Tampilkan angka dari 1 sampai 100, tetapi untuk setiap kelipatan 3 diganti dengan menuliskan kata &ldquo;snip&rdquo;, setiap kelipatan 5 diganti menuliskan &ldquo;snap&rdquo;, dan setiap kelipatan 3 dan 5 menuliskan &ldquo;snip-snap&rdquo;. Contoh output:</span></p>', NULL, NULL, NULL, 10, 2),
(42, '<p class=\"MsoListParagraphCxSpFirst\" style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 20px; font-family: Arial, sans-serif; text-indent: -18pt;\"><span lang=\"IN\" style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \'Times New Roman\';\">&nbsp; &nbsp; &nbsp;&nbsp;</span></span></span><span lang=\"IN\" style=\"box-sizing: border-box;\">Diketahui sebuah array terdiri dari pasangan bilangan, kecuali satu yang tidak berpasangan. Buatlah&nbsp;tampilan yang menentukan posisi bilangan tersebut dalam array dengan animasi pemunculannya.</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 20px; font-family: Arial, sans-serif;\"><span lang=\"IN\" style=\"box-sizing: border-box;\">&nbsp;</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 20px; font-family: Arial, sans-serif;\"><span lang=\"IN\" style=\"box-sizing: border-box;\">contoh array:&nbsp;</span><span lang=\"IN\" style=\"box-sizing: border-box; font-size: 10pt; line-height: 15.3333px;\">[102,32,99,32,45,102,45,67,67,100,100]</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 20px; font-family: Arial, sans-serif;\"><span lang=\"IN\" style=\"box-sizing: border-box;\">&nbsp;</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 20px; font-family: Arial, sans-serif;\"><span lang=\"IN\" style=\"box-sizing: border-box;\">maka bilangan yang tidak berpasangan adalah 99 pada posisi ke-3.</span></p>', NULL, NULL, NULL, 10, 2),
(43, '<p class=\"MsoListParagraphCxSpFirst\" style=\"box-sizing: border-box; margin: 0px 0px 10px; line-height: 20px; font-family: Arial, sans-serif; text-indent: -18pt;\"><span lang=\"IN\" style=\"box-sizing: border-box;\">&nbsp; &nbsp; &nbsp;Buatlah&nbsp;tampilan untuk mengkonversi mata uang ke USD dari mata uang lain atau sebaliknya. Nilai kurs dapat berubah sewaktu-waktu, yang dituliskan adalah nilai USD pada saat itu. Untuk simulasi bisa digunakan file atau resourse.xml untuk menggambarkan aliran data KURS. Requirement:</span></p>\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"box-sizing: border-box; margin: 0px 0px 10px 54pt; line-height: 20px; font-family: Arial, sans-serif; text-indent: -18pt;\"><span lang=\"IN\" style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box;\">a.<span style=\"box-sizing: border-box; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \'Times New Roman\';\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></span></span><span lang=\"IN\" style=\"box-sizing: border-box;\">Model pojo yang menampung mata uang dan nilai kurs</span></p>\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"box-sizing: border-box; margin: 0px 0px 10px 54pt; line-height: 20px; font-family: Arial, sans-serif; text-indent: -18pt;\"><span lang=\"IN\" style=\"box-sizing: border-box;\"><span style=\"box-sizing: border-box;\">b.<span style=\"box-sizing: border-box; font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: \'Times New Roman\';\">&nbsp; &nbsp; &nbsp;</span></span></span>&nbsp;Satu Button untuk pperintah konversi mata uang dan satu Button lagi untuk switch konversi</p>\r\n<table class=\"MsoNormalTable\" style=\"border-spacing: 0px; border-collapse: collapse; font-family: Arial, sans-serif; letter-spacing: 0.13px; margin-left: 36pt; border: none;\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody style=\"box-sizing: border-box;\">\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0cm 5.4pt; width: 231.05pt; border: 1pt solid windowtext;\" valign=\"top\" width=\"308\">\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-family: Calibri, sans-serif;\">Input</span></p>\r\n</td>\r\n<td style=\"box-sizing: border-box; padding: 0cm 5.4pt; width: 231.05pt; border-top: 1pt solid windowtext; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-image: initial; border-left: none;\" valign=\"top\" width=\"308\">\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-family: Calibri, sans-serif;\">Output yang diharapkan</span></p>\r\n</td>\r\n</tr>\r\n<tr style=\"box-sizing: border-box;\">\r\n<td style=\"box-sizing: border-box; padding: 0cm 5.4pt; width: 231.05pt; border-right: 1pt solid windowtext; border-bottom: 1pt solid windowtext; border-left: 1pt solid windowtext; border-image: initial; border-top: none;\" valign=\"top\" width=\"308\">\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">KURS IDR 9000</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">KURS CAD 400</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">KURS JPY 300</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">CONV JPY 40000</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">CONV IDR 80000</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">KURS IDR 9800</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">CONV IDR 80000</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">CONV MYR 20000</span></p>\r\n</td>\r\n<td style=\"box-sizing: border-box; padding: 0cm 5.4pt; width: 231.05pt; border-top: none; border-left: none; border-bottom: 1pt solid windowtext; border-right: 1pt solid windowtext;\" valign=\"top\" width=\"308\">\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">&nbsp;</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">&nbsp;</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">&nbsp;</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">USD 133.33 -&gt;(40000/300)</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">USD 8.88<span style=\"box-sizing: border-box;\">&nbsp;&nbsp;&nbsp;</span>-&gt;(80000/9000)</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">&nbsp;</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">USD 8.16<span style=\"box-sizing: border-box;\">&nbsp;&nbsp;&nbsp;</span>-&gt;(80000/9800)</span></p>\r\n<p class=\"MsoNormal\" style=\"box-sizing: border-box; margin: 0px 0px 10px; font-size: 15px; letter-spacing: normal; line-height: 20px;\"><span lang=\"EN-US\" style=\"box-sizing: border-box; font-size: 10pt; font-family: Calibri, sans-serif;\">MYR 20000<span style=\"box-sizing: border-box;\">&nbsp;&nbsp;</span>-&gt;unknown</span></p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', NULL, NULL, NULL, 10, 2),
(44, '<p>siapa yang ganteng ?</p>', NULL, NULL, NULL, 8, 4);

-- --------------------------------------------------------

--
-- Table structure for table `question_answers`
--

CREATE TABLE `question_answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) DEFAULT NULL,
  `question_answers` varchar(250) NOT NULL,
  `question_answers_alias` char(5) DEFAULT NULL,
  `question_type` enum('text','image') NOT NULL DEFAULT 'text',
  `correct_answers` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_answers`
--

INSERT INTO `question_answers` (`id`, `question_id`, `question_answers`, `question_answers_alias`, `question_type`, `correct_answers`) VALUES
(47, 30, 'a2', NULL, 'text', 1),
(48, 30, 'b2', NULL, 'text', NULL),
(49, 30, 'c2', NULL, 'text', NULL),
(53, 30, 'd2', NULL, 'text', NULL),
(54, 30, 'e2', NULL, 'text', NULL),
(68, 35, 'a. sifat manusia dan sifat padi', NULL, 'text', 1),
(69, 35, 'b. keberhasilan manusia dan padi', NULL, 'text', NULL),
(70, 35, 'c. manusia dan padi', NULL, 'text', NULL),
(71, 36, 'a. berperang membawa genderang', NULL, 'text', NULL),
(72, 36, 'b. rasa kemanusiaan yang tinggi', NULL, 'text', NULL),
(73, 36, 'c. semangat perjuangan tinggi', NULL, 'text', 1),
(74, 37, 'a. survei', NULL, 'text', NULL),
(75, 37, 'b. sekilas', NULL, 'text', 1),
(76, 37, 'c. dangkal', NULL, 'text', NULL),
(77, 38, 'A) Gerak Translasi', NULL, 'text', NULL),
(78, 38, 'B) Gerak Rotasi', NULL, 'text', 1),
(79, 38, 'C) Perpindahan', NULL, 'text', NULL),
(80, 39, 'A.    15 J', NULL, 'text', NULL),
(81, 39, 'B.    30 J', NULL, 'text', NULL),
(82, 39, 'C.    35 J', NULL, 'text', 1),
(83, 40, 'A.    0,83 x 1023 partikel ', NULL, 'text', NULL),
(84, 40, 'B.     0,72 x 1023 partikel', NULL, 'text', 1),
(85, 40, 'C.     0,42 x 1023 partikel', NULL, 'text', NULL),
(86, 44, 'afgan', NULL, 'text', 1),
(87, 44, 'ariel', NULL, 'text', 1),
(88, 44, 'kiwil', NULL, 'text', NULL),
(89, 44, 'sule', NULL, 'text', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `question_file`
--

CREATE TABLE `question_file` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `file` varchar(150) DEFAULT NULL,
  `temp_name` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_image`
--

CREATE TABLE `question_image` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `image` varchar(150) DEFAULT NULL,
  `temp_name` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question_type`
--

CREATE TABLE `question_type` (
  `id` int(11) NOT NULL,
  `name` varchar(75) DEFAULT NULL,
  `type` varchar(50) NOT NULL,
  `status` tinyint(5) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_type`
--

INSERT INTO `question_type` (`id`, `name`, `type`, `status`) VALUES
(1, 'Multiple Choice', 'Basic', 1),
(2, 'File Upload', 'Basic', 1),
(3, 'Textarea', 'Basic', 1),
(4, 'Multiple Answers', 'Basic', 1),
(5, 'Statement & Holder', 'Special', 1),
(6, 'Add Person, Organization and Location', 'Special', 1),
(7, 'Psikotest', 'Special', 1),
(8, 'Papi Kostick', 'Special', 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_answers`
--

CREATE TABLE `test_answers` (
  `id` int(11) NOT NULL,
  `question_id` int(11) NOT NULL,
  `test_transaction_id` int(11) NOT NULL,
  `answers` text DEFAULT NULL,
  `answers_temp_file` text DEFAULT NULL,
  `result` int(15) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_answers`
--

INSERT INTO `test_answers` (`id`, `question_id`, `test_transaction_id`, `answers`, `answers_temp_file`, `result`) VALUES
(30, 44, 17, '[\"86\",\"87\"]', NULL, 1),
(31, 37, 17, '74', NULL, NULL),
(32, 36, 17, '71', NULL, NULL),
(33, 35, 17, '68', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_assessment`
--

CREATE TABLE `test_assessment` (
  `id` int(11) NOT NULL,
  `test_transaction_id` int(11) NOT NULL,
  `question_type_id` int(11) NOT NULL,
  `assessment` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_assessment`
--

INSERT INTO `test_assessment` (`id`, `test_transaction_id`, `question_type_id`, `assessment`) VALUES
(7, 17, 1, '25');

-- --------------------------------------------------------

--
-- Table structure for table `test_groups`
--

CREATE TABLE `test_groups` (
  `id` int(11) NOT NULL,
  `vacancy_division_id` int(11) NOT NULL,
  `test_type_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `rules` varchar(30) NOT NULL DEFAULT 'opened',
  `random_test` tinyint(4) DEFAULT NULL,
  `sort` tinyint(3) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_groups`
--

INSERT INTO `test_groups` (`id`, `vacancy_division_id`, `test_type_id`, `status`, `rules`, `random_test`, `sort`) VALUES
(2, 1, 8, 1, 'opened', NULL, 1),
(3, 1, 9, 1, 'opened', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `test_transaction`
--

CREATE TABLE `test_transaction` (
  `id` int(11) NOT NULL,
  `time_start` datetime DEFAULT NULL,
  `time_end` datetime DEFAULT NULL,
  `applicant_id` int(11) DEFAULT NULL,
  `test_type_id` int(11) NOT NULL,
  `status` tinyint(4) DEFAULT 0 COMMENT '0 : Register | 1 : Start | 2 : Finish',
  `checked_transaction` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_transaction`
--

INSERT INTO `test_transaction` (`id`, `time_start`, `time_end`, `applicant_id`, `test_type_id`, `status`, `checked_transaction`) VALUES
(17, '2021-11-25 17:19:26', '2021-11-25 17:39:26', 2, 8, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `test_type`
--

CREATE TABLE `test_type` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `mode_test` tinyint(3) NOT NULL,
  `total_question` int(11) NOT NULL,
  `time` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  `type` enum('Fix','Custom') NOT NULL DEFAULT 'Custom'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test_type`
--

INSERT INTO `test_type` (`id`, `name`, `description`, `mode_test`, `total_question`, `time`, `status`, `sort`, `type`) VALUES
(1, 'Psikotes', NULL, 1, 60, 20, 1, 5, 'Custom'),
(8, 'TEST 2 (BAHASA INDONESIA)', NULL, 1, 10, 20, 1, NULL, 'Custom'),
(9, 'TEST 3 (FISIKA)', NULL, 1, 10, 20, 1, NULL, 'Custom'),
(10, 'TEST BACKEND DEVELOPER', NULL, 1, 10, 20, 1, NULL, 'Custom'),
(11, 'BE Programmers', NULL, 0, 0, NULL, 1, NULL, 'Custom');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `password_show` varchar(225) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `avatar` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `password_show`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`, `avatar`) VALUES
(1, '::1', 'admin', '$2a$08$d6dNzNvDgCVaKZQatI0HD.KJQJHJ0q/SvYiqnm8.7uatu0ikrf1Bu', NULL, NULL, 'administrator@bdg.ebdesk.com', NULL, NULL, NULL, 'cU7uVf7ybM6w0vagvsqgdu', 1460088952, 1637835517, 1, 'Administrator', '', NULL, '-', NULL),
(2, '::1', 'evaluator', '$2a$08$d6dNzNvDgCVaKZQatI0HD.KJQJHJ0q/SvYiqnm8.7uatu0ikrf1Bu', NULL, NULL, 'evaluator@bdg.ebdesk.com', NULL, NULL, NULL, '.PVIFJG/7kCfnhUTDCLsNe', 1460088952, 1630919777, 1, 'Evaluator', '', NULL, '-', NULL),
(4, '::1', 'susantodlaksono@gmail.com', '$2y$08$AqJ9KlpZk4a2XebqQNSipe0Wz2y7Vh9z.MHKyLp9dEsaH6pvq3TMG', '0869898922618bc179c230e', NULL, 'susantodlaksono@gmail.com', NULL, NULL, NULL, '4aKJBNgpeJ4fu1U2AQAcN.', 1636548985, 1637830355, 1, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 3),
(4, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `vacancy_division`
--

CREATE TABLE `vacancy_division` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '0 ',
  `created_date` datetime DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vacancy_division`
--

INSERT INTO `vacancy_division` (`id`, `name`, `status`, `created_date`, `updated_date`) VALUES
(1, 'BE Programmer', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(2, 'FE Programmer', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(3, 'Web Developer', 0, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(4, 'Quality Assurance', 0, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(5, 'Network Engineer', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(7, 'Data Scientist', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(8, 'AI Engineer', 0, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(9, 'Media Analyst', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(10, 'HR Officer', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(11, 'Finance', 0, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(12, 'Secretary', 1, '2017-10-19 00:00:00', '2017-10-19 00:00:00'),
(14, 'Account Executive', 0, '2018-08-06 14:45:35', NULL),
(16, 'Mechanical Engineer', 1, '2018-09-17 18:01:47', NULL),
(17, 'Sales & Marketing', 0, '2018-09-18 09:24:33', NULL),
(19, 'NLP Research', 1, '2018-10-29 21:50:36', NULL),
(20, 'Design Graphic', 1, '2018-10-31 19:53:59', NULL),
(21, 'Social Media Officer', 0, '2018-11-19 16:52:37', NULL),
(26, 'Development Tools', 0, '2019-01-25 17:20:21', NULL),
(27, 'Research and Development', 0, '2019-02-15 17:58:26', NULL),
(28, 'Social Data Analyst', 0, '2019-06-19 14:07:28', NULL),
(31, 'Support Officer', 1, '2020-03-20 11:14:49', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `applicant`
--
ALTER TABLE `applicant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vacancy_division_id` (`vacancy_division_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `applicant_file`
--
ALTER TABLE `applicant_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicant_id` (`applicant_id`);

--
-- Indexes for table `evaluator_tests`
--
ALTER TABLE `evaluator_tests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `test_type_id` (`test_type_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_type_id` (`test_type_id`),
  ADD KEY `question_type_id` (`question_type_id`),
  ADD KEY `correct_answers_id` (`correct_answers_id`);

--
-- Indexes for table `question_answers`
--
ALTER TABLE `question_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `correct_answers` (`correct_answers`);

--
-- Indexes for table `question_file`
--
ALTER TABLE `question_file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `question_image`
--
ALTER TABLE `question_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`);

--
-- Indexes for table `question_type`
--
ALTER TABLE `question_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_answers`
--
ALTER TABLE `test_answers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `test_transaction_id` (`test_transaction_id`);

--
-- Indexes for table `test_assessment`
--
ALTER TABLE `test_assessment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `test_transaction_id` (`test_transaction_id`),
  ADD KEY `question_type_id` (`question_type_id`);

--
-- Indexes for table `test_groups`
--
ALTER TABLE `test_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vacancy_division_id` (`vacancy_division_id`),
  ADD KEY `test_type_id` (`test_type_id`);

--
-- Indexes for table `test_transaction`
--
ALTER TABLE `test_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `applicant_id` (`applicant_id`),
  ADD KEY `test_type_id` (`test_type_id`);

--
-- Indexes for table `test_type`
--
ALTER TABLE `test_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `group_id` (`group_id`);

--
-- Indexes for table `vacancy_division`
--
ALTER TABLE `vacancy_division`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `applicant`
--
ALTER TABLE `applicant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `applicant_file`
--
ALTER TABLE `applicant_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `evaluator_tests`
--
ALTER TABLE `evaluator_tests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `question_answers`
--
ALTER TABLE `question_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT for table `question_file`
--
ALTER TABLE `question_file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `question_image`
--
ALTER TABLE `question_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `question_type`
--
ALTER TABLE `question_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `test_answers`
--
ALTER TABLE `test_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `test_assessment`
--
ALTER TABLE `test_assessment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `test_groups`
--
ALTER TABLE `test_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `test_transaction`
--
ALTER TABLE `test_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `test_type`
--
ALTER TABLE `test_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `vacancy_division`
--
ALTER TABLE `vacancy_division`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `applicant`
--
ALTER TABLE `applicant`
  ADD CONSTRAINT `applicant_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `applicant_ibfk_2` FOREIGN KEY (`vacancy_division_id`) REFERENCES `vacancy_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `applicant_file`
--
ALTER TABLE `applicant_file`
  ADD CONSTRAINT `applicant_file_ibfk_1` FOREIGN KEY (`applicant_id`) REFERENCES `applicant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `evaluator_tests`
--
ALTER TABLE `evaluator_tests`
  ADD CONSTRAINT `evaluator_tests_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `evaluator_tests_ibfk_2` FOREIGN KEY (`test_type_id`) REFERENCES `test_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `question_ibfk_1` FOREIGN KEY (`test_type_id`) REFERENCES `test_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `question_ibfk_2` FOREIGN KEY (`correct_answers_id`) REFERENCES `question_answers` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `question_answers`
--
ALTER TABLE `question_answers`
  ADD CONSTRAINT `question_answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question_file`
--
ALTER TABLE `question_file`
  ADD CONSTRAINT `question_file_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `question_image`
--
ALTER TABLE `question_image`
  ADD CONSTRAINT `question_image_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_answers`
--
ALTER TABLE `test_answers`
  ADD CONSTRAINT `test_answers_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_answers_ibfk_2` FOREIGN KEY (`test_transaction_id`) REFERENCES `test_transaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_assessment`
--
ALTER TABLE `test_assessment`
  ADD CONSTRAINT `test_assessment_ibfk_1` FOREIGN KEY (`question_type_id`) REFERENCES `question_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_assessment_ibfk_2` FOREIGN KEY (`test_transaction_id`) REFERENCES `test_transaction` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_groups`
--
ALTER TABLE `test_groups`
  ADD CONSTRAINT `test_groups_ibfk_1` FOREIGN KEY (`test_type_id`) REFERENCES `test_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_groups_ibfk_2` FOREIGN KEY (`vacancy_division_id`) REFERENCES `vacancy_division` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test_transaction`
--
ALTER TABLE `test_transaction`
  ADD CONSTRAINT `test_transaction_ibfk_1` FOREIGN KEY (`test_type_id`) REFERENCES `test_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `test_transaction_ibfk_2` FOREIGN KEY (`applicant_id`) REFERENCES `applicant` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
